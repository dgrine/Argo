from invoke import task, Collection
import pathlib
if __package__ is None or __package__ == '':
    from docker import tasks as docker
    from docs import tasks as docs
    from examples import tasks as examples
    from library import tasks as library
else:
    from .docker import tasks as docker
    from .docs import tasks as docs
    from .examples import tasks as examples
    from .library import tasks as library

THIS_DIR = pathlib.Path(__file__).parent.resolve()
LIBRARY_DIR = pathlib.Path(THIS_DIR, 'library')
SINGLE_INCLUDE_DIR = pathlib.Path(THIS_DIR, 'single_include')
RELEASES_DIR = pathlib.Path(THIS_DIR, 'releases')

@task
def version(context, format = '{major}.{minor}.{patch}', verbose = True, set = "", bump = False):
    """
    Prints the version number in the given format. Optionally sets a new version (--set).
    """
    import re
    fn_cmakelists = pathlib.Path(LIBRARY_DIR, 'CMakeLists.txt')
    numbers = []
    lines = []
    with open(str(fn_cmakelists), 'r') as fin:
        for line in fin.readlines():
            if 'project(' in line:
                regex = "(\d+)\.(\d+)\.(\d+)"
                if bump:
                    matches = re.search(regex, line)
                    set = "{}.{}.{}".format(matches[1], matches[2], int(matches[3]) + 1)
                if set: line = re.sub(regex, set, line)
                matches = re.search(regex, line)
                if not matches: raise RuntimeError("Missing from line '{}' from {}".format(line, fn_cmakelists))
                numbers = [matches.groups()[idx] for idx in range(0, 3)]
            lines.append(line)
    if set or bump:
        with open(fn_cmakelists, 'w') as fout: fout.writelines(lines)
        context.run('inv library.config library.publish')
    if verbose or set or bump: print(format.format(major = numbers[0], minor = numbers[1], patch = numbers[2]))
    return numbers

@task(pre = [library.publish])
def qc(context, docker = True):
    """
    Quality check: unit-tests, examples and Docker platform integration.
    """
    print("Configuring build: release")
    context.run('invoke library.config --release')
    print("Running unit-tests")
    context.run('invoke library.test/release')
    print("Compiling examples")
    context.run('invoke examples.build')
    if docker:
        print("Running Docker platform integration tests")
        context.run('invoke docker.test')
    else:
        print("Skipping Docker platform integration tests")
    print("Done.")

@task(pre = [library.publish])
def package(context):
    """
    Packages the library as a tgz file.
    """
    print("Packaging...")
    if not RELEASES_DIR.exists():
        print("Release directory {} does not exist. Cloning from GitLab...".format(RELEASES_DIR))
        context.run("git clone https://gitlab.com/dgrine/Argo-release.git releases")
    import tarfile
    version_nr = '.'.join(version(context, verbose = False))
    fn_name = 'Argo-{}.tgz'.format(version_nr)
    fn_package = pathlib.Path(RELEASES_DIR, 'version', fn_name)
    fn_header = pathlib.Path(SINGLE_INCLUDE_DIR, 'argo', 'Argo.hpp')
    if fn_package.exists(): print("Overwriting {}".format(fn_package))
    with tarfile.open(fn_package, 'w:gz') as ftar:
        ftar.add(fn_header, arcname = 'argo/Argo.hpp')
    print("Created package {}".format(fn_package))
    fn_package_latest = pathlib.Path(RELEASES_DIR, 'version', 'Argo-latest.tgz')
    context.run("cp {} {}".format(fn_package, fn_package_latest))
    print("Created package {}".format(fn_package_latest))
    print("Done.")

def git_branch():
    import subprocess
    branch = subprocess.run('git rev-parse --abbrev-ref HEAD', shell = True, stdout = subprocess.PIPE).stdout.decode('utf8')
    return branch.strip()

@task(pre = [package])
def deploy(context):
    """
    Deploys the library to Git.
    """
    ver = '.'.join(version(context, verbose = False))
    print("Deploying Argo v{ver} [{branch}]".format(ver = ver, branch = git_branch()))
    def push(update_command):
        cmd = 'git add .; git status'
        context.run(cmd, pty = True)
        _ = input("Press Enter to continue...")
        cmd = 'git commit -m \'Released version {}.\' && {} && git push'.format(ver, update_command)
        print(cmd)
        context.run(cmd)
    with context.cd(str(THIS_DIR)):
        print("Pushing code...")
        push('git pull --rebase')
    with context.cd('releases'):
        print("Pushing releases...")
        push('git merge -s ours')
    print("Done.")

@task(pre = [qc, deploy])
def dia(context):
    """
    Does it all: testing, documentation update and deployment.
    """
    pass

ns = Collection(version, qc, package, deploy, dia)
ns.add_collection(Collection.from_module(docker, 'docker'))
ns.add_collection(Collection.from_module(docs, 'docs'))
ns.add_collection(Collection.from_module(examples, 'examples'))
ns.add_collection(Collection.from_module(library, 'library'))

