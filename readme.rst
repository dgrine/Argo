|logo|

.. |logo| image:: data/argo.png

.. image:: https://readthedocs.org/projects/argo/badge/?version=latest

Argo is a single header C++11 library for argument parsing like a boss. It is light-weight, flexible, easy-to-use, and has out-of-the-box support for:

* Long-hand (``--foo``) and short-hand notation (``-f``)
* Options, toggles, flags, and positional arguments
* Automatic short-hand generation and guessing of undefined short-hands
* Automatic generation of ``--help``, ``--version`` options
* Automatic dashing of names (``foo_bar`` to ``foo-bar``)
* Automatic type conversions for basic types, and easily extensible for custom types
* Automatic storage for basic types, including STL containers
* Support for ``std::optional`` (C++17, when setting ``-DARGO_ENABLE_STD_OPTIONAL=1``)
* Advanced parsing:

  * Support for various flavours, such as ``--foo 1 2`` and ``--foo=1,2`` and ``--foo=1 --foo=2`` and ``--foo 1 --foo 2`` or any combination thereof
  * Understanding that ``--debug false`` and ``--no-debug`` are equivalent
* Grouping of options, including inclusive and exclusive groups
* Required and optional arguments
* Cardinality: fixed number, one or more, zero or more
* Response files: configuration files that use the same syntax as the command line
* Helpful error messages
* Beautiful and responsive output

.. code-block:: cpp

    #include <argo/Argo.hpp>
    #include <iostream>

    using namespace argo;

    void count(unsigned int max)
    {
        for (unsigned int idx = 1u; idx <= max; ++idx) std::cout << idx << " ";
        std::cout << std::endl;
    }

    int main(int argc, char **argv)
    {
        unsigned int max;
        
        Arguments args;
        handler::Option option{"--number", max};
        option.help("Numbers to count").required();
        args.add(option);
        const auto result = args.parse(argc, argv);
        switch (result.status)
        {
            case ReturnCode::Error: std::cerr << "Error: " << result.message << std::endl; return 1;
            case ReturnCode::SuccessAndAbort: return 0;
            default: break;
        }
        count(max);
        return 0;
    }

|demo|

.. |demo| image:: data/demo.gif

Check out the `documentation <https://argo.readthedocs.io>`_ to get started.

