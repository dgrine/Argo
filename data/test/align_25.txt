input:
abcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcdef
page size:   40
indentation: 0
result:
1   5    10   15   20   25   30   35   40   45   50   55   60   65   70   75   80
abcdefghijklmnopqrstuvwxyzbcdefghijklmno
pqrstuvwxyzbcdefghijklmnopqrstuvwxyzbcde
fghijklmnopqrstuvwxyzbcdefghijklmnopqrst
uvwxyzbcdefghijklmnopqrstuvwxyzbcdefghij
klmnopqrstuvwxyzbcdefghijklmnopqrstuvwxy
zbcdefghijklmnopqrstuvwxyzbcdefghijklmno
pqrstuvwxyzbcdef
