input:
This is an example of a line that is longer than expected and should definitely be wrapped so that it presents nicely on the user's screen. An extra sentence is added so that we are sure to have two lines.
page size:   40
indentation: 16
result:
1   5    10   15   20   25   30   35   40   45   50   55   60   65   70   75   80
                This is an example of a
                line that is longer
                than expected and
                should definitely be
                wrapped so that it
                presents nicely on the
                user's screen. An extra
                sentence is added so
                that we are sure to
                have two lines.
