from invoke import task, Collection, call, exceptions
import os
import pathlib
import re
import shutil
import sys

THIS_DIR = pathlib.Path(__file__).parent.resolve()
ROOT_DIR = pathlib.Path(THIS_DIR).parent.resolve()
BUILD_DIR = pathlib.Path(ROOT_DIR, 'build')
BUILD_DIR.mkdir(exist_ok = True)
SINGLE_INCLUDE_DIR = pathlib.Path(ROOT_DIR, 'single_include')

class CompilerConfig(object):
    def __init__(self, debug, rtc, debug_info):
        super(CompilerConfig, self).__init__()
        self.debug = debug
        self.rtc = rtc
        self._debug_info = debug_info

    @classmethod
    def from_vars(cls, debug, rtc, debug_info):
        return CompilerConfig(debug, rtc, debug_info)
    @classmethod
    def from_string(cls, string):
        debug = None
        rtc = None
        debug_info = None
        parts = string.split('-')
        if 'reldeb' in parts:
            debug = False
            debug_info = True
        elif 'release' in parts:
            debug = False
            debug_info = False
        elif 'debug' in parts:
            debug = True
            debug_info = True
        if 'rtc' in parts: rtc = True
        else: rtc = False
        assert(debug is not None)
        assert(rtc is not None)
        assert(debug_info is not None)
        return cls.from_vars(debug, rtc, debug_info)

    @property
    def release(self): return not self.debug
    @property
    def debug_info(self):
        return True if self.debug else self._debug_info
    @property
    def cmake_build_type(self):
        if self.release: return 'RelWithDebInfo' if self.debug_info else 'Release'
        else: return "Debug"
    @property
    def build_dir_name(self):
        parts = []
        if self.release and self.debug_info: parts.append('reldeb')
        elif self.release: parts.append('release')
        elif self.debug: parts.append('debug')
        if self.rtc: parts.append('rtc')
        return '-'.join(parts)
    @property
    def build_dir(self):
        path = pathlib.Path(BUILD_DIR, self.build_dir_name)
        path.mkdir(parents = True, exist_ok = True)
        return path
    def __str__(self): return self.build_dir_name

def replace_if_different(fn_tmp, force):
    assert('.tmp' in str(fn_tmp))
    assert(fn_tmp.exists())
    fn_target = pathlib.Path(str(fn_tmp).replace('.tmp', ''))
    content_tmp = None
    content_target = None
    with open(fn_tmp, 'r') as fin: content_tmp = fin.read()
    if fn_target.exists():
        with open(fn_target, 'r') as fin: content_target = fin.read()
    else:
        content_target = ""
    if force or content_tmp != content_target:
        with open(fn_target, 'w') as fout: fout.write(content_tmp)
        os.remove(fn_tmp)
        return True
    else:
        os.remove(fn_tmp)
        return False

@task
def config(context, debug = True, rtc = True, debug_info = True, release = False):
    """
    Configures a build with the given configuration. This will create a builds/<config> folder.
    """
    if release:
        debug = False
        rtc = False
        debug_info = False
    cfg = CompilerConfig.from_vars(debug, rtc, debug_info)
    print("Compiler configuration: {} ".format(cfg))
    print("Build directory: {}".format(cfg.build_dir))
    with context.cd(str(cfg.build_dir)):
        options = []
        options.append('-DCMAKE_BUILD_TYPE={}'.format(cfg.cmake_build_type))
        if cfg.rtc: options.append('-DENABLE_SANITIZERS=\'address;undefined\'')
        if cfg.debug: options.append('-DARGO_EXPANDED_LIBRARY=TRUE')
        context.run('cmake {} {}'.format(THIS_DIR, ' '.join(options)))

@task
def publish(context, force = False, verbose = False):
    """
    Publishes all deliverables: single include, forwarding header.
    """
    #Single include
    fn_si = pathlib.Path(SINGLE_INCLUDE_DIR, 'argo', 'Argo.hpp.tmp')
    def add_license():
        with open(fn_si, 'a') as fout:
            fn_license = pathlib.Path(ROOT_DIR, 'license.txt')
            with open(fn_license, 'r') as fin:
                fout.write('/*\n')
                fout.write(fin.read())
                fout.write('*/\n\n')
    def open_include_guard():
        with open(fn_si, 'a') as fout:
            fout.write("#ifndef HEADER_argo_Argo_hpp_INCLUDE_GUARD\n")
            fout.write("#define HEADER_argo_Argo_hpp_INCLUDE_GUARD\n\n")
    def close_include_guard():
        with open(fn_si, 'a') as fout:
            fout.write("#endif")
    rx_include = r"#include \<(argo/[a-zA-Z\/_\.]+)\>"
    rx_include_sysroot = r"#include <([a-zA-Z\/_\.]+)>"
    rx_s = re.compile(r'\sS\(')
    rx_l = re.compile(r'L\(')
    rx_c = re.compile(r'C\(')
    rx_mss = re.compile(r'MSS')
    rx_range = re.compile(r'RANGE\(')
    level = 0
    def log(msg):
        if not verbose: return
        log_idx = 0
        while log_idx < level:
            sys.stdout.write('  ')
            log_idx += 1
        sys.stdout.write(msg)
        sys.stdout.write('\n')
    fn_processed = set()
    includes_sys = set()
    def process(fn):
        nonlocal level
        log("Processing {}".format(fn))
        if fn in fn_processed:
            log("Skipping: already processed {}".format(fn))
            return []
        fn_processed.add(fn)
        lines_out = []
        with open(fn, 'r') as fin:
            lines_in = fin.readlines()
            for n, line in enumerate(lines_in):
                #Include expressions
                matches = re.match(rx_include, line)
                if matches:
                    fn_sub = pathlib.Path(THIS_DIR, 'inc', matches.group(1))
                    if not fn_sub.exists(): raise RuntimeError("Missing file {} included from {}".format(fn_sub, fn))
                    level += 1
                    lines_out.extend(process(fn_sub))
                    level -= 1
                    continue
                matches = re.match(rx_include_sysroot, line)
                if matches:
                    inc = matches.group(1)
                    if inc in includes_sys: continue
                    includes_sys.add(matches.group(1))
                for expr, replacement in zip([rx_s, rx_l, rx_c, rx_mss, rx_range], [' ARGO_S(', 'ARGO_L(', 'ARGO_C(', 'ARGO_MSS', 'ARGO_RANGE(']):
                    if re.search(expr, line): line = re.sub(expr, replacement, line)
                lines_out.append(line)
        return lines_out
    def add(fns):
        idx = 0
        while idx < len(fns):
            fn = fns[idx]
            idx += 1
            lines = process(fn)
            with open(fn_si, 'a') as fout: fout.writelines(lines)

    add_license()
    open_include_guard()
    fn_headers = [fn for fn in pathlib.Path(THIS_DIR, 'inc', 'argo').glob('**/*.hpp')]
    add(fn_headers)
    close_include_guard()
    if replace_if_different(fn_si, force):
        print("Published {} files to the single include header".format(len(fn_processed)))

    #Forwarding header
    fn_fwd = pathlib.Path(THIS_DIR, 'inc', 'argo', 'Argo.hpp.tmp')
    with open(fn_fwd, 'w') as fout:
        def blacklisted(fn):
            # return False
            fn = str(fn)
            return 'core' in fn or 'Argo.hpp' in fn
        prefix = str(pathlib.Path(THIS_DIR, 'inc')) + '/'
        includes = [str(fn).replace(prefix, '') for fn in fn_headers if not blacklisted(fn)]
        fout.writelines(['#include <{}>\n'.format(include) for include in includes])
    if replace_if_different(fn_fwd, force):
        print("Published {} includes to the forwarding header".format(len(includes)))

@task(pre = [publish])
def transform(context, namespace, output):
    """
    Generates a transformed single include header for deep integration into an external code base.
    """
    fn_si = pathlib.Path(SINGLE_INCLUDE_DIR, 'argo', 'Argo.hpp')
    include_guard = ''.join(namespace.replace('::', '_'))
    argo_ns_begin = ' '.join(['namespace {} {{'.format(ns) for ns in namespace.split('::')])
    argo_ns_end = ' '.join(['}' for ns in namespace.split('::')])
    rx_include_guard = re.compile(r'HEADER_argo')
    rx_define_argo_ns_begin_or_end = re.compile(r'#define ARGO_NS_(BEGIN|END)')
    rx_argo_ns_begin = re.compile(r'ARGO_NS_BEGIN')
    rx_argo_ns_end = re.compile(r'ARGO_NS_END')
    rx_argo_ns = re.compile(r'#define ARGO_NS\(x\) argo::x')
    rxs = [
            (rx_include_guard, lambda expr, line: re.sub(expr, 'HEADER_' + include_guard, line)),
            (rx_define_argo_ns_begin_or_end, lambda expr, line: '\n'),
            (rx_argo_ns_begin, lambda expr, line: re.sub(expr, argo_ns_begin, line)),
            (rx_argo_ns_end, lambda expr, line: re.sub(expr, argo_ns_end, line)),
            (rx_argo_ns, lambda expr, line: re.sub(expr, '#define ARGO_NS(x) {}::x'.format(namespace), line)),
    ]
    def process(line):
        for expr, fnc in rxs:
            if re.search(expr, line):
                line = fnc(expr, line)
        return line

    with open(fn_si, 'r') as fin: lines = [process(line) for line in fin.readlines()]
    with open(output, 'w') as fout: fout.writelines(lines)

def glob_build_tasks():
    configs = [str(dir.stem) for dir in BUILD_DIR.glob('*') if dir.is_dir()]
    for config in configs:
        template = """
@task(name = 'build/{config}', pre = [publish])
def build_{func_name}(context, target = 'all'):
    \"\"\"
    Builds the {config} configuration.
    \"\"\"
    with context.cd({build_dir}):
        cmd = 'make -j8 ' + target
        cmd += ' VERBOSE=1'
        print("Building in", context.cwd, ":", cmd)
        context.run(cmd, pty = True)
ns.add_task(build_{func_name})

@task(name = 'clean/{config}')
def clean_{func_name}(context, verbose = 0):
    \"\"\"
    Cleans the {config} configuration.
    \"\"\"
    with context.cd({build_dir}):
        context.run('make clean')
ns.add_task(clean_{func_name})

@task(name = 'test/{config}', pre = [build_{func_name}])
def test_{func_name}(context, tag = '', verbose = False, run = True):
    \"\"\"
    Runs the unit-tests of the {config} configuration.
    \"\"\"
    exes = [exe for exe in pathlib.Path({build_dir}).glob('**/*-tests')]
    if not run:
        print("There are " + str(len(exes)) + " test executables:")
        for exe in exes: print("- " + str(exe))
        return
    nr_failed = 0
    catch_tag = "".join(['[' + t + ']' for t in tag.split(':')])
    for exe in exes:
        cmd = str(exe)
        if 0 != len(tag): cmd += " " + catch_tag
        print("Running test executable:\\n" + cmd)
        try: context.run(cmd, pty = True)
        except exceptions.UnexpectedExit as err: nr_failed += 1
    if 0 != nr_failed:
        print("Error: " + str(nr_failed) + " out of " + str(len(exes)) + " test executables contain failed test cases.")
        exit(1)

ns.add_task(test_{func_name})

@task(name = 'remove/{config}')
def remove_{func_name}(context, verbose = 0):
    \"\"\"
    Removes the {config} configuration from the builds directory. This will disable any related tasks.
    \"\"\"
    shutil.rmtree({build_dir})
ns.add_task(remove_{func_name})
        """
        func_name = config.replace('-', '_')
        build_dir = pathlib.Path(BUILD_DIR, config)
        code = template.format(config = config, func_name = func_name, build_dir = "\"{}\"".format(build_dir))
        exec(code)

ns = Collection(config, publish, transform)
glob_build_tasks()

