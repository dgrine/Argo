#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Positional", "[argo][handler][positional][parsing]")
{
    bool foo = false;
    std::vector<std::string> input_files;
    Arguments args;
    {
        handler::Toggle toggle{ "--foo", foo };
        toggle.required();
        REQUIRE(args.add(toggle));
    }
    {
        handler::Positional positional{ "input-files" };
        positional.required().action(action::store(input_files)).nargs("+");
        REQUIRE(args.add(positional));
    }
    test::RawArguments raw_args;
    SECTION("Named options in front")
    {
        raw_args = { "--foo", "one", "two", "three" };
    }
    const auto result = args.parse(raw_args);
    REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    REQUIRE(std::vector<std::string>{ "one", "two", "three" } == input_files);
}

