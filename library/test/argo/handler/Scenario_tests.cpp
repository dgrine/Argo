#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Scenario test: Option with positionals", "[argo][handler][foo]")
{
    struct Options
    {
        std::string fn_output;
        std::vector<std::string> fn_input;
    } options;
    Arguments args{};
    REQUIRE(args.add(argo::handler::Option("output", "o", options.fn_output)));
    REQUIRE(args.add(argo::handler::Positional("input-files", options.fn_input)));
    SECTION("Option Positional")
    {
        test::RawArguments raw_args = {"-o", "foo.dom", "foo.wow"};
        const auto result = args.parse(raw_args);
        std::cout << result.message << std::endl;
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        REQUIRE("foo.dom" == options.fn_output);
        REQUIRE(std::vector<std::string>{"foo.wow"} == options.fn_input);
    }
    SECTION("Option Positional Positional")
    {
        test::RawArguments raw_args = {"-o", "foo.dom", "foo.wow", "bar.wow"};
        const auto result = args.parse(raw_args);
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        REQUIRE("foo.dom" == options.fn_output);
        REQUIRE(std::vector<std::string>{"foo.wow", "bar.wow"} == options.fn_input);
    }
}

