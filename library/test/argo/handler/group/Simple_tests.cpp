#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Simple optional group with two optionals", "[argo][handler][group][simple][01]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Simple group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }));
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: []
})";
    }
    SECTION("One argument")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1"]
})";
    }
    SECTION("Multiple arguments")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Simple required group with two optionals", "[argo][handler][group][simple][02]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Simple group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }));
    group.required();
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "At least one argument is required for group 'my group'",
  arguments: []
})";
    }
    SECTION("One argument")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1"]
})";
    }
    SECTION("Multiple arguments")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Simple optional group with a required and optional", "[argo][handler][group][simple][03]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Simple group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }.required()));
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' is required",
  arguments: []
})";
    }
    SECTION("One argument: the optional one")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' is required",
  arguments: ["-f", "1"]
})";
    }
    SECTION("One argument: the required one")
    {
        raw_args = { "-b", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-b", "1"]
})";
    }
    SECTION("Multiple arguments")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Simple required group with a required and optional", "[argo][handler][group][simple][04]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Simple group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }.required()));
    group.required();
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' is required",
  arguments: []
})";
    }
    SECTION("One argument: the optional one")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' is required",
  arguments: ["-f", "1"]
})";
    }
    SECTION("One argument: the required one")
    {
        raw_args = { "-b", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-b", "1"]
})";
    }
    SECTION("Multiple arguments")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}
