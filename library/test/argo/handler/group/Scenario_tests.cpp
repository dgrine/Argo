#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Group scenario test: optional inclusive group containing two optional exclusive groups", "[argo][handler][group][scenario][01]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, barbar;
    handler::group::Inclusive group{ "group-inc" };
    {
        handler::group::Exclusive subgroup{ "group-exc-1" };
        REQUIRE(subgroup.add(handler::Option{ "--foo", foo }));
        REQUIRE(subgroup.add(handler::Flag{ "--bar" }));
        REQUIRE(group.add(subgroup));
    }
    {
        handler::group::Exclusive subgroup{ "group-exc-2" };
        REQUIRE(subgroup.add(handler::Flag{ "--foofoo" }));
        REQUIRE(subgroup.add(handler::Option{ "--barbar", barbar }));
        REQUIRE(group.add(subgroup));
    }
    REQUIRE(args.add(group));
    SECTION("One from each")
    {
        raw_args = { "--foo", "1", "--barbar", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["--foo", "1", "--barbar", "2"]
})";
    }
    SECTION("Only from one")
    {
        raw_args = { "--foo", "1" };
        expected = R"({
  status: "Error",
  message: "Missing argument 'group-exc-2' for inclusive group 'group-inc'",
  arguments: ["--foo", "1"]
})";
    }
    SECTION("No arguments")
    {
        raw_args = {};
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: []
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Group scenario test: required inclusive group containing two optional exclusive groups", "[argo][handler][group][scenario][02]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, barbar;
    handler::group::Inclusive group{ "group-inc" };
    group.required();
    {
        handler::group::Exclusive subgroup{ "group-exc-1" };
        REQUIRE(subgroup.add(handler::Option{ "--foo", foo }));
        REQUIRE(subgroup.add(handler::Flag{ "--bar" }));
        REQUIRE(group.add(subgroup));
    }
    {
        handler::group::Exclusive subgroup{ "group-exc-2" };
        REQUIRE(subgroup.add(handler::Flag{ "--foofoo" }));
        REQUIRE(subgroup.add(handler::Option{ "--barbar", barbar }));
        REQUIRE(group.add(subgroup));
    }
    REQUIRE(args.add(group));
    SECTION("One from each")
    {
        raw_args = { "--foo", "1", "--barbar", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["--foo", "1", "--barbar", "2"]
})";
    }
    SECTION("Only from one")
    {
        raw_args = { "--foo", "1" };
        expected = R"({
  status: "Error",
  message: "Missing argument 'group-exc-2' for inclusive group 'group-inc'",
  arguments: ["--foo", "1"]
})";
    }
    SECTION("No arguments")
    {
        raw_args = {};
        expected = R"({
  status: "Error",
  message: "Missing arguments for required inclusive group 'group-inc'",
  arguments: []
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Group scenario test: exclusive group containing two exclusive groups", "[argo][handler][group][scenario][03]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    handler::group::Exclusive group{ "group-exc-root" };
    {
        handler::group::Exclusive subgroup{ "group-exc-1" };
        REQUIRE(subgroup.add(handler::Flag{ "--foo" }));
        REQUIRE(subgroup.add(handler::Flag{ "--bar" }));
        REQUIRE(group.add(subgroup));
    }
    {
        handler::group::Exclusive subgroup{ "group-exc-2" };
        REQUIRE(subgroup.add(handler::Flag{ "--foofoo" }));
        REQUIRE(subgroup.add(handler::Flag{ "--barbar" }));
        REQUIRE(group.add(subgroup));
    }
    REQUIRE(args.add(group));
    SECTION("Option from subgroup 1")
    {
        raw_args = { "--foo" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["--foo"]
})";
    }
    SECTION("Option from subgroup 2")
    {
        raw_args = { "--barbar" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["--barbar"]
})";
    }
    SECTION("Options from subgroup 1")
    {
        raw_args = { "--foo", "--bar" };
        expected = R"({
  status: "Error",
  message: "Flag '--bar' belongs to exclusive group 'group-exc-1' and cannot be used in combination with options from the same group",
  arguments: ["--foo", "--bar"]
})";
    }
    SECTION("Options from subgroup 2")
    {
        raw_args = { "--foofoo", "--barbar" };
        expected = R"({
  status: "Error",
  message: "Flag '--barbar' belongs to exclusive group 'group-exc-2' and cannot be used in combination with options from the same group",
  arguments: ["--foofoo", "--barbar"]
})";
    }
    SECTION("Option from subgroup 1 and from subgroup 2")
    {
        raw_args = { "--foo", "--foofoo" };
        expected = R"({
  status: "Error",
  message: "Exclusive group 'group-exc-2' belongs to exclusive group 'group-exc-root' and cannot be used in combination with options from the same group",
  arguments: ["--foo", "--foofoo"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Group copying and parent check test", "[argo][handler][group][scenario][parent]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    handler::group::Exclusive group{"group-exc-root"};
    {
        handler::group::Exclusive tmp{ "group-exc-root" };
        {
            handler::group::Exclusive subgroup{ "group-exc-1" };
            REQUIRE(subgroup.add(handler::Flag{ "--foo" }));
            REQUIRE(subgroup.add(handler::Flag{ "--bar" }));
            auto other = subgroup;
            REQUIRE(tmp.add(other));
        }
        group = tmp;
    }
    {
        handler::group::Exclusive subgroup{ "group-exc-2" };
        REQUIRE(subgroup.add(handler::Flag{ "--foofoo" }));
        REQUIRE(subgroup.add(handler::Flag{ "--barbar" }));
        auto other = subgroup;
        REQUIRE(group.add(other));
    }
    //SECTION("Hierarchy")
    //{
        //{
            //std::ostringstream os;
            //for (const auto &handler : group) os << handler << "/";
            //REQUIRE("group-exc-1/group-exc-2/" == os.str());
        //}
        //{
            //std::ostringstream os;
            //const core::handler::IHandler *pfirst = nullptr;
            //auto idx = 0u;
            //for (const auto &handler : group) if (idx++ == 0) pfirst = &handler;
            //REQUIRE(core::handler::is_type<handler::group::Interface>(*pfirst));
            //REQUIRE(!!pfirst);
            //for (const auto &handler: static_cast<const handler::group::Exclusive &>(*pfirst)) os << handler << "/";
            //REQUIRE("--foo/--bar/" == os.str());
        //}
        //{
            //std::ostringstream os;
            //const core::handler::IHandler *pfirst = nullptr;
            //auto idx = 0u;
            //for (const auto &handler : group) if (idx++ == 1) pfirst = &handler;
            //REQUIRE(!!pfirst);
            //REQUIRE(core::handler::is_type<handler::group::Interface>(*pfirst));
            //for (const auto &handler: static_cast<const handler::group::Exclusive &>(*pfirst)) os << handler << "/";
            //REQUIRE("--foofoo/--barbar/" == os.str());
        //}
    //}
    REQUIRE(args.add(group));
    SECTION("Option from subgroup 1")
    {
        raw_args = { "--foo" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["--foo"]
})";
    }
    SECTION("Option from subgroup 2")
    {
        raw_args = { "--barbar" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["--barbar"]
})";
    }
    SECTION("Options from subgroup 1")
    {
        raw_args = { "--foo", "--bar" };
        expected = R"({
  status: "Error",
  message: "Flag '--bar' belongs to exclusive group 'group-exc-1' and cannot be used in combination with options from the same group",
  arguments: ["--foo", "--bar"]
})";
    }
    SECTION("Options from subgroup 2")
    {
        raw_args = { "--foofoo", "--barbar" };
        expected = R"({
  status: "Error",
  message: "Flag '--barbar' belongs to exclusive group 'group-exc-2' and cannot be used in combination with options from the same group",
  arguments: ["--foofoo", "--barbar"]
})";
    }
    SECTION("Option from subgroup 1 and from subgroup 2")
    {
        raw_args = { "--foo", "--foofoo" };
        expected = R"({
  status: "Error",
  message: "Exclusive group 'group-exc-2' belongs to exclusive group 'group-exc-root' and cannot be used in combination with options from the same group",
  arguments: ["--foo", "--foofoo"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

//#TODO: Implement feature
#if 0
TEST_CASE("Same name options in exclusive groups", "[argo][handler][group][scenario][same_name_in_exclusives]")
{
    std::size_t one_foo = 0;
    std::size_t two_foo = 0;
    Arguments args{};
    {
        handler::group::Exclusive group{"one"};
        group.add(handler::Flag{"--one"});
        group.add(handler::Option{"--foo", one_foo});
        args.add(group);
    }
    {
        handler::group::Exclusive group{"two"};
        group.add(handler::Flag{"--two"});
        group.add(handler::Option{"--foo", two_foo});
        args.add(group);
    }
    test::RawArguments raw_args = {"--two", "--foo", "123"};
    const auto result = args.parse(raw_args);
    std::cout << result.message << std::endl;
    REQUIRE(ReturnCode::SuccessAndContinue == result.status);
}
#endif
