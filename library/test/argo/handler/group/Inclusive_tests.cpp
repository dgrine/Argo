#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Inclusive optional group with two optionals", "[argo][handler][group][inclusive][01]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Inclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }));
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: []
})";
    }
    SECTION("One argument")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "Error",
  message: "Missing argument '--bar (-b)' for inclusive group 'my group'",
  arguments: ["-f", "1"]
})";
    }
    SECTION("All arguments")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Inclusive required group with two optionals", "[argo][handler][group][inclusive][02]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Inclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }));
    group.required();
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "Missing arguments for required inclusive group 'my group'",
  arguments: []
})";
    }
    SECTION("One argument")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "Error",
  message: "Missing argument '--bar (-b)' for inclusive group 'my group'",
  arguments: ["-f", "1"]
})";
    }
    SECTION("All arguments")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Inclusive optional group with a required and two optionals", "[argo][handler][group][inclusive][03]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar, car;
    handler::group::Inclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }.required()));
    REQUIRE(group.add(handler::Option{ "-c", "--car", car }));
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: []
})";
    }
    SECTION("One argument: an optional one")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "Error",
  message: "Missing argument '--bar (-b)' for inclusive group 'my group'",
  arguments: ["-f", "1"]
})";
    }
    SECTION("One argument: the required one")
    {
        raw_args = { "-b", "1" };
        expected = R"({
  status: "Error",
  message: "Argument '--bar (-b)' belongs to inclusive group 'my group', which expects other arguments",
  arguments: ["-b", "1"]
})";
    }
    SECTION("All arguments")
    {
        raw_args = { "-f", "1", "-b", "2", "-c", "3" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2", "-c", "3"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Inclusive required group with a required and two optionals", "[argo][handler][group][inclusive][04]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar, car;
    handler::group::Inclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }.required()));
    REQUIRE(group.add(handler::Option{ "-c", "--car", car }));
    group.required();
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "Missing arguments for required inclusive group 'my group'",
  arguments: []
})";
    }
    SECTION("One argument: an optional one")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "Error",
  message: "Missing argument '--bar (-b)' for inclusive group 'my group'",
  arguments: ["-f", "1"]
})";
    }
    SECTION("One argument: the required one")
    {
        raw_args = { "-b", "1" };
        expected = R"({
  status: "Error",
  message: "Argument '--bar (-b)' belongs to inclusive group 'my group', which expects other arguments",
  arguments: ["-b", "1"]
})";
    }
    SECTION("All arguments")
    {
        raw_args = { "-f", "1", "-b", "2", "-c", "3" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1", "-b", "2", "-c", "3"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Inclusive optional group", "[argo][handler][group][inclusive][05]")
{
    bool info_mode = false;
    bool process_mode = false;
    bool fail_on_clipping = false;
    std::string wav_input;
    std::string wav_output;
    std::string fn_clipping_info;
    Arguments args{};
    {
        handler::group::Simple group{ "Processing mode" };
        group.required();
        group.add(handler::Toggle{ "--info", info_mode }.help("Prints the plugin information."));
        group.add(handler::Toggle{ "--process", process_mode }.help("Process the plugin."));
        args.add(group);
    }
    {
        handler::group::Inclusive group{ "Processing options" };
        group.required([&process_mode]() { return process_mode; });
        group.add(handler::Option{ "--input", wav_input }.help("The wav input"));
        group.add(handler::Option{ "--output", wav_output }.help("The wav output"));
        {
            handler::group::Simple clipping_group{ "Clipping options" };
            clipping_group.optional();
            clipping_group.add(handler::Toggle{ "--fail-on-clipping", fail_on_clipping }.help("Fail the executable when clipping is detected, after processing the whole content."));
            clipping_group.add(handler::Option{ "--clipping-info", fn_clipping_info }.help("Filename for the naft output of the clipping detector."));
            group.add(clipping_group);
        }
        args.add(group);
    }
    test::RawArguments raw_args;
    SECTION("Info mode")
    {
        raw_args = { "--info" };
        const auto result = args.parse(raw_args);
        REQUIRE(!!result);
        REQUIRE((info_mode && !process_mode));
    }
    SECTION("Processing mode")
    {
        SECTION("Forgot processing mode, but using processing options")
        {
            raw_args = { "--input", "in.wav", "--output", "out.wav" };
            const auto result = args.parse(raw_args);
            REQUIRE(!result);
        }
        SECTION("Without processing options")
        {
            raw_args = { "--process" };
            const auto result = args.parse(raw_args);
            REQUIRE(!result);
            REQUIRE((!info_mode && process_mode));
        }
        SECTION("With processing options")
        {
            SECTION("Without clipping options")
            {
                raw_args = { "--process", "--input", "in.wav", "--output", "out.wav" };
                const auto result = args.parse(raw_args);
                REQUIRE(!!result);
                REQUIRE((!info_mode && process_mode));
                REQUIRE("in.wav" == wav_input);
                REQUIRE("out.wav" == wav_output);
            }
            SECTION("With clipping options")
            {
                raw_args = { "--process", "--input", "in.wav", "--output", "out.wav", "--fail-on-clipping" };
                const auto result = args.parse(raw_args);
                REQUIRE(!!result);
                REQUIRE((!info_mode && process_mode));
                REQUIRE("in.wav" == wav_input);
                REQUIRE("out.wav" == wav_output);
                REQUIRE(fail_on_clipping);
            }
        }
    }
}
