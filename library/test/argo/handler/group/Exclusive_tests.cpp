#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Exclusive optional group with two optionals", "[argo][handler][group][exclusive][01]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Exclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }));
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: []
})";
    }
    SECTION("One argument")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1"]
})";
    }
    SECTION("More than one argument")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' belongs to exclusive group 'my group' and cannot be used in combination with options from the same group",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Exclusive required group with two optionals", "[argo][handler][group][exclusive][02]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar;
    handler::group::Exclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo }));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar }));
    group.required();
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "Missing argument for exclusive group 'my group'",
  arguments: []
})";
    }
    SECTION("One argument")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1"]
})";
    }
    SECTION("More than one argument")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' belongs to exclusive group 'my group' and cannot be used in combination with options from the same group",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Exclusive optional group with a required and two optionals", "[argo][handler][group][exclusive][03]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar, car;
    handler::group::Exclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo}));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar}.required()));
    REQUIRE(group.add(handler::Option{ "-c", "--car", car}));
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: []
})";
    }
    SECTION("One argument: an optional one")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1"]
})";
    }
    SECTION("One argument: the required one")
    {
        raw_args = { "-b", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-b", "1"]
})";
    }
    SECTION("More than one argument")
    {
        raw_args = { "-f", "1", "-b", "2" };
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' belongs to exclusive group 'my group' and cannot be used in combination with options from the same group",
  arguments: ["-f", "1", "-b", "2"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Exclusive required group with a required and two optionals", "[argo][handler][group][exclusive][04]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo, bar, car;
    handler::group::Exclusive group{ "my group" };
    REQUIRE(group.add(handler::Option{ "-f", "--foo", foo}));
    REQUIRE(group.add(handler::Option{ "-b", "--bar", bar}.required()));
    REQUIRE(group.add(handler::Option{ "-c", "--car", car}));
    group.required();
    REQUIRE(args.add(group));
    SECTION("No arguments")
    {
        expected = R"({
  status: "Error",
  message: "Missing argument for exclusive group 'my group'",
  arguments: []
})";
    }
    SECTION("One argument: an optional one")
    {
        raw_args = { "-f", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "1"]
})";
    }
    SECTION("One argument: the required one")
    {
        raw_args = { "-b", "1" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-b", "1"]
})";
    }
    SECTION("More than argument")
    {
        raw_args = { "-f", "1", "-b", "2", "-c", "3" };
        expected = R"({
  status: "Error",
  message: "Option '--bar (-b)' belongs to exclusive group 'my group' and cannot be used in combination with options from the same group",
  arguments: ["-f", "1", "-b", "2", "-c", "3"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

