#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>
#include <string>
#include <vector>

using namespace argo;

TEST_CASE("Option: Required", "[argo][handler][option][required]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string foo;
    handler::Option option{ "-f", "--foo", foo };
    option.required();
    REQUIRE(args.add(option));
    const auto result = args.parse(raw_args);
    REQUIRE_FALSE(ReturnCode::SuccessAndContinue == result.status);
    args.print_help(std::cout);
}

TEST_CASE("Option: Cadinality", "[argo][handler][option][nargs]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string dummy;
    handler::Option option{ "-f", "--foo", dummy };

    SECTION("Fixed number")
    {
        option.nargs(2);
        REQUIRE(args.add(option));
        SECTION("Correct")
        {
            raw_args = { "-f", "a", "b" };
            expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "a", "b"]
})";
        }
        SECTION("Incorrect")
        {
            raw_args = { "-f", "a", "b", "1c" };
            expected = R"({
  status: "Error",
  message: "Unexpected value '1c' for option '--foo (-f)': expected 2 arguments",
  arguments: ["-f", "a", "b", "1c"]
})";
        }
    }
    SECTION("One or more")
    {
        option.nargs("+");
        REQUIRE(args.add(option));
        raw_args = { "-f", "a", "b" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "a", "b"]
})";
    }
    SECTION("Zero or more")
    {
        option.nargs("*");
        REQUIRE(args.add(option));
        raw_args = { "-f", "a", "b" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "a", "b"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Option: Cadinality with explicit values", "[argo][handler][option][implicit_values]")
{
    Configuration config;
    config.parser.implicit_values = false;
    Arguments args{ config };
    test::RawArguments raw_args;
    std::string expected;
    std::string dummy;
    handler::Option option{ "-f", "--foo", dummy };

    SECTION("Fixed number")
    {
        option.nargs(2);
        REQUIRE(args.add(option));
        SECTION("Correct")
        {
            raw_args = { "-f", "a", "-f", "b" };
            expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "a", "-f", "b"]
})";
        }
        SECTION("Incorrect: implicit values")
        {
            raw_args = { "-f", "a", "b", "1c" };
            expected = R"({
  status: "Error",
  message: "Unexpected positional argument 'b'",
  arguments: ["-f", "a", "b", "1c"]
})";
        }
        SECTION("Incorrect: nargs")
        {
            raw_args = { "-f", "a", "-f", "b", "-f", "1c" };
            expected = R"({
  status: "Error",
  message: "Unexpected value '1c' for option '--foo (-f)': expected 2 arguments",
  arguments: ["-f", "a", "-f", "b", "-f", "1c"]
})";
        }
    }
    SECTION("One or more")
    {
        option.nargs("+");
        REQUIRE(args.add(option));
        raw_args = { "-f", "a", "-f", "b" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "a", "-f", "b"]
})";
    }
    SECTION("Zero or more")
    {
        option.nargs("*");
        REQUIRE(args.add(option));
        raw_args = { "-f", "a", "-f", "b" };
        expected = R"({
  status: "SuccessAndContinue",
  message: "",
  arguments: ["-f", "a", "-f", "b"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Option: Storage", "[argo][handler][option][storage]")
{
    std::ostringstream actual;
    Arguments args;
    SECTION("Scalar")
    {
        float value = 0;
        handler::Option option{ "--value", value };
        args.add(option);
        SECTION("Correct")
        {
            const auto result = args.parse({ "--value", "1.2" });
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(1.2f == value);
        }
        SECTION("Incorrect: too many values")
        {
            const auto result = args.parse({ "--value=1.2,3.4" });
            actual << result;
            const auto expected = R"({
  status: "Error",
  message: "Unexpected value '3.4' for option '--value': expected 1 argument",
  arguments: ["--value", "1.2", "3.4"]
})";
            REQUIRE(expected == actual.str());
        }
    }
    SECTION("Vector")
    {
        std::vector<float> values;
        handler::Option option{ "--values", values };
        REQUIRE(args.add(option));
        SECTION("Correct")
        {
            const auto result = args.parse({ "--values", "1.2", "3.4", "5.6" });
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(std::vector<float>{ 1.2, 3.4, 5.6 } == values);
        }
    }
}

TEST_CASE("Option: Variable binding", "[argo][handler][option][binding]")
{
    SECTION("Binding to a non-const variable indicates storage")
    {
        std::string value = "-f";
        handler::Option option{ "--foo", value };
        Arguments args;
        REQUIRE(args.add(option));
        const auto result = args.parse({ "--foo", "bar" });
        REQUIRE("bar" == value);
    }
    SECTION("Const variables can never indicate storage")
    {
        SECTION("Without a custom action, this should fail")
        {
            const std::string value = "-f";
            handler::Option option{ "--foo", value };
            Arguments args;
            REQUIRE(!args.add(option));
        }
        SECTION("With a custom action, this is acceptable")
        {
            const std::string shorthand = "-f";
            handler::Option option{ "--foo", shorthand };
            Arguments args;
            std::string value;
            option.action(action::store(value));
            REQUIRE(args.add(option));
            const auto result = args.parse({ "--foo", "bar" });
            REQUIRE("bar" == value);
        }
    }
}

TEST_CASE("Option: Auto dashing", "[argo][handler][option][dashing]")
{
    Configuration cfg;
    SECTION("Convert underscores to dashes")
    {
        cfg.parser.dash_names = true;
    }
    SECTION("Do not convert underscores to dashes")
    {
        cfg.parser.dash_names = false;
    }
    std::size_t foo_bar = 0;
    Arguments args{ cfg };
    handler::Option option{ "--foo_bar", foo_bar };
    REQUIRE(args.add(option));
    test::RawArguments raw_args = { "--foo_bar", "123" };
    const auto result = args.parse(raw_args);
    if (cfg.parser.dash_names)
    {
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        REQUIRE(123 == foo_bar);
    }
    else
    {
        REQUIRE(ReturnCode::Error == result.status);
    }
}

TEST_CASE("Option: Prefixing of named arguments", "[argo][handler][option][prefixing]")
{
    Arguments args;
    bool must_pass = false;
    std::string foo;
    SECTION("Standard use case")
    {
        must_pass = true;
        std::string first, second;
        SECTION("First longhand, then shorthand")
        {
            first = "--foo";
            second = "-f";
        }
        SECTION("First shorthand, then longhand")
        {
            first = "-f";
            second = "--foo";
        }
        handler::Option option{ first, second, foo };
        REQUIRE(args.add(option));
    }
    SECTION("No prefixes at all")
    {
        must_pass = true;
        std::string first, second;
        SECTION("First longhand, then shorthand")
        {
            first = "foo";
            second = "f";
        }
        SECTION("First shorthand, then longhand")
        {
            first = "f";
            second = "foo";
        }
        handler::Option option{ first, second, foo };
        REQUIRE(args.add(option));
    }
    SECTION("Explicit longhand, implicit shorthand")
    {
        must_pass = true;
        std::string first, second;
        SECTION("First longhand, then shorthand")
        {
            first = "--foo";
            second = "f";
        }
        SECTION("First shorthand, then longhand")
        {
            first = "f";
            second = "--foo";
        }
        handler::Option option{ first, second, foo };
        REQUIRE(args.add(option));
    }
    SECTION("Explicit shorthand, implicit longhand")
    {
        must_pass = true;
        std::string first, second;
        SECTION("First longhand, then shorthand")
        {
            first = "foo";
            second = "-f";
        }
        SECTION("First shorthand, then longhand")
        {
            first = "-f";
            second = "foo";
        }
        handler::Option option{ first, second, foo };
        REQUIRE(args.add(option));
    }
    SECTION("Implicit shorthand")
    {
        must_pass = true;
        handler::Option option{ "f", foo };
        REQUIRE(args.add(option));
    }
    SECTION("Implicit longhand")
    {
        must_pass = true;
        handler::Option option{ "foo", foo };
        REQUIRE(args.add(option));
    }
    SECTION("Failure: same size implicit shorthand and longhand")
    {
        must_pass = false;
        handler::Option option{ "foo", "fff", foo };
        REQUIRE(!args.add(option));
    }
    if (must_pass)
    {
        const std::string value = "test";
        const auto result = args.parse({ "-f", value });
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    }
}

TEST_CASE("Option: Require any_of", "[argo][handler][option][require][any_of]")
{
    Arguments args;
    SECTION("String values")
    {
        std::string actual;
        handler::Option option{ "--foo", actual };
        const auto allowed_values = { "one", "two", "three" };
        option.require(require::any_of(allowed_values));
        REQUIRE(args.add(option));
        SECTION("Allowed values")
        {
            for (const auto &allowed_value : allowed_values)
            {
                test::RawArguments raw_args = { "--foo", allowed_value };
                const auto result = args.parse(raw_args);
                REQUIRE(ReturnCode::SuccessAndContinue == result.status);
                REQUIRE(allowed_value == actual);
            }
        }
        SECTION("Disallowed values")
        {
            test::RawArguments raw_args = { "--foo", "some-none-allowed-value" };
            const auto result = args.parse(raw_args);
            std::ostringstream actual;
            actual << result;
            const auto expected = R"({
  status: "Error",
  message: "Unexpected value 'some-none-allowed-value' for option '--foo'. Expected 'one', 'two' or 'three'",
  arguments: ["--foo", "some-none-allowed-value"]
})";
            REQUIRE(expected == actual.str());
        }
    }
    SECTION("Non-string values")
    {
        unsigned actual;
        handler::Option option{ "--foo", actual };
        const auto allowed_values = { 1, 2, 3 };
        option.require(require::any_of(allowed_values));
        REQUIRE(args.add(option));
        SECTION("Allowed values")
        {
            for (const auto &allowed_value : allowed_values)
            {
                test::RawArguments raw_args = { "--foo", std::to_string(allowed_value) };
                const auto result = args.parse(raw_args);
                REQUIRE(ReturnCode::SuccessAndContinue == result.status);
                REQUIRE(allowed_value == actual);
            }
        }
        SECTION("Disallowed values")
        {
            test::RawArguments raw_args = { "--foo", "4" };
            const auto result = args.parse(raw_args);
            std::ostringstream actual;
            actual << result;
            const auto expected = R"({
  status: "Error",
  message: "Unexpected value '4' for option '--foo'. Expected '1', '2' or '3'",
  arguments: ["--foo", "4"]
})";
            REQUIRE(expected == actual.str());
        }
    }
}

TEST_CASE("Option: Require none_of", "[argo][handler][option][require][none_of]")
{
    Arguments args;
    SECTION("String values")
    {
        std::string actual;
        handler::Option option{ "--foo", actual };
        const auto disallowed_values = { "one", "two", "three" };
        option.require(require::none_of(disallowed_values));
        REQUIRE(args.add(option));
        SECTION("Disallowed values")
        {
            for (const auto &disallowed_value : disallowed_values)
            {
                test::RawArguments raw_args = { "--foo", disallowed_value };
                const auto result = args.parse(raw_args);
                REQUIRE(ReturnCode::Error == result.status);
            }
        }
        SECTION("Allowed values")
        {
            test::RawArguments raw_args = { "--foo", "some-allowed-value" };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        }
    }
}

TEST_CASE("Option: Require range::open_open", "[argo][handler][option][require][range][open_open]")
{
    Arguments args;
    int actual;
    int first = -2;
    int last = 4;
    handler::Option option{ "--foo", actual };
    option.require(require::range::open_open(first, last));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = first + 1; value < last; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (const auto value : { -3, -2, 4, 5 })
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require range::open_closed", "[argo][handler][option][require][range][open_closed]")
{
    Arguments args;
    int actual;
    int first = -2;
    int last = 4;
    handler::Option option{ "--foo", actual };
    option.require(require::range::open_closed(first, last));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = first + 1; value <= last; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (const auto value : { -3, -2, 5 })
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require range::closed_closed", "[argo][handler][option][require][range][closed_closed]")
{
    Arguments args;
    int actual;
    int first = -2;
    int last = 4;
    handler::Option option{ "--foo", actual };
    option.require(require::range::closed_closed(first, last));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = first; value <= last; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (const auto value : { -3, 5 })
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require range::closed_open", "[argo][handler][option][require][range][closed_open]")
{
    Arguments args;
    int actual;
    int first = -2;
    int last = 4;
    handler::Option option{ "--foo", actual };
    option.require(require::range::closed_open(first, last));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = first; value < last; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (const auto value : { -3, 4, 5 })
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require greater_than", "[argo][handler][option][require][greater_than]")
{
    Arguments args;
    int actual;
    int cutoff = 5;
    handler::Option option{ "--foo", actual };
    option.require(require::greater_than(cutoff));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = cutoff + 1; value < cutoff + 3; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (auto value = cutoff - 3; value <= cutoff; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require greater_than_or_equal", "[argo][handler][option][require][greater_than_or_equal]")
{
    Arguments args;
    int actual;
    int cutoff = 5;
    handler::Option option{ "--foo", actual };
    option.require(require::greater_than_or_equal(cutoff));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = cutoff + 1; value <= cutoff + 3; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (auto value = cutoff - 3; value < cutoff; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require lesser_than", "[argo][handler][option][require][lesser_than]")
{
    Arguments args;
    int actual;
    int cutoff = 5;
    handler::Option option{ "--foo", actual };
    option.require(require::lesser_than(cutoff));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = cutoff - 3; value < cutoff; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (auto value = cutoff; value < cutoff + 3; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Require lesser_than_or_equal", "[argo][handler][option][require][lesser_than_or_equal]")
{
    Arguments args;
    int actual;
    int cutoff = 5;
    handler::Option option{ "--foo", actual };
    option.require(require::lesser_than_or_equal(cutoff));
    REQUIRE(args.add(option));
    SECTION("Allowed values")
    {
        for (auto value = cutoff - 3; value <= cutoff; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::SuccessAndContinue == result.status);
            REQUIRE(value == actual);
        }
    }
    SECTION("Disallowed values")
    {
        for (auto value = cutoff + 1; value < cutoff + 3; ++value)
        {
            test::RawArguments raw_args = { "--foo", std::to_string(value) };
            const auto result = args.parse(raw_args);
            REQUIRE(ReturnCode::Error == result.status);
        }
    }
}

TEST_CASE("Option: Argument-like values", "[argo][handler][option][argument_values]")
{
    std::unique_ptr<Arguments> pargs;
    ReturnCode rc;
    SECTION("Argument-like values not allowed (default behavior)")
    {
        rc = ReturnCode::Error;
        pargs = core::make_unique<Arguments>();
    }
    SECTION("Argument-like values allowed")
    {
        rc = ReturnCode::SuccessAndContinue;
        Configuration config;
        config.parser.argument_values = true;
        pargs = core::make_unique<Arguments>(config);
    }
    REQUIRE(!!pargs);
    std::vector<std::string> input;
    REQUIRE(pargs->add(handler::Option{ "--input", input }));
    std::string output;
    REQUIRE(pargs->add(handler::Option{ "--output", output }));
    test::RawArguments raw_args = { "-i", "one", "two", "--three", "-o", "out" };
    const auto result = pargs->parse(raw_args);
    REQUIRE(rc == result.status);
}

TEST_CASE("Option: empty values", "[argo][handler][option][empty_values]")
{
    std::unique_ptr<Arguments> pargs;
    ReturnCode rc;
    SECTION("Empty values not allowed (default behavior)")
    {
        rc = ReturnCode::Error;
        pargs = core::make_unique<Arguments>();
    }
    SECTION("Empty values allowed")
    {
        rc = ReturnCode::SuccessAndContinue;
        Configuration config;
        config.parser.empty_values = true;
        pargs = core::make_unique<Arguments>(config);
    }
    REQUIRE(!!pargs);
    std::vector<std::string> input;
    REQUIRE(pargs->add(handler::Option{ "--input", input }));
    test::RawArguments raw_args = { "-i", "" };
    const auto result = pargs->parse(raw_args);
    REQUIRE(rc == result.status);
}
