#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Toggle: Parsing", "[argo][handler][toggle][parsing]")
{
    Arguments args;
    test::RawArguments raw_args = { "--foo" };
    bool foo = false;
    bool expected = false;
    SECTION("Using setter")
    {
        auto set_foo = [&foo](const bool value) { foo = value; };
        handler::Toggle toggle{ "--foo", set_foo };
        toggle.required();
        REQUIRE(args.add(toggle));
        SECTION("Truthy: true")
        {
            expected = true;
            SECTION("no value") {}
            SECTION("true") { raw_args.push_back("true"); }
            SECTION("1") { raw_args.push_back("1"); }
            SECTION("yes") { raw_args.push_back("yes"); }
            SECTION("y") { raw_args.push_back("y"); }
            SECTION("True") { raw_args.push_back("True"); }
            SECTION("YES") { raw_args.push_back("YES"); }
            SECTION("Y") { raw_args.push_back("Y"); }
        }
        SECTION("Truthy: false")
        {
            expected = false;
            SECTION("false") { raw_args.push_back("false"); }
            SECTION("0") { raw_args.push_back("0"); }
            SECTION("no") { raw_args.push_back("no"); }
            SECTION("n") { raw_args.push_back("n"); }
            SECTION("False") { raw_args.push_back("False"); }
            SECTION("NO") { raw_args.push_back("NO"); }
            SECTION("N") { raw_args.push_back("N"); }
        }
    }
    SECTION("Using variable")
    {
        handler::Toggle toggle{ "--foo", foo };
        toggle.required();
        REQUIRE(args.add(toggle));
        SECTION("Truthy: true")
        {
            expected = true;
            SECTION("no value") {}
            SECTION("true") { raw_args.push_back("true"); }
            SECTION("1") { raw_args.push_back("1"); }
            SECTION("yes") { raw_args.push_back("yes"); }
            SECTION("y") { raw_args.push_back("y"); }
            SECTION("True") { raw_args.push_back("True"); }
            SECTION("YES") { raw_args.push_back("YES"); }
            SECTION("Y") { raw_args.push_back("Y"); }
        }
        SECTION("Truthy: false")
        {
            expected = false;
            SECTION("false") { raw_args.push_back("false"); }
            SECTION("0") { raw_args.push_back("0"); }
            SECTION("no") { raw_args.push_back("no"); }
            SECTION("n") { raw_args.push_back("n"); }
            SECTION("False") { raw_args.push_back("False"); }
            SECTION("NO") { raw_args.push_back("NO"); }
            SECTION("N") { raw_args.push_back("N"); }
        }
    }
    const auto result = args.parse(raw_args);
    REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    REQUIRE(expected == foo);
}

TEST_CASE("Toggle: All cases", "[argo][handler][toggle][cases]")
{
    struct Scenario {
        test::RawArguments raw_args;
        bool value;
        bool expected;
    };
    auto scenarios = {
        Scenario{{"--foo"}, true, true},
        Scenario{{"--foo", "true"}, true, true},
        Scenario{{"--foo", "false"}, true, false},
        Scenario{{"--no-foo"}, true, false},
        Scenario{{"--no-foo", "true"}, true, false},
        Scenario{{"--no-foo", "false"}, true, true},
        Scenario{{"--foo"}, false, true},
        Scenario{{"--foo", "true"}, false, true},
        Scenario{{"--foo", "false"}, false, false},
        Scenario{{"--no-foo"}, false, false},
        Scenario{{"--no-foo", "true"}, false, false},
        Scenario{{"--no-foo", "false"}, false, true}
    };
    for (auto scenario: scenarios)
    {
        Arguments args;
        args.add(handler::Toggle{"--foo", scenario.value});
        const auto result = args.parse(scenario.raw_args);
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        REQUIRE(scenario.expected == scenario.value);
    }
}


