#include <argo/Argo.hpp>
#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>
#include <ostream>

using namespace argo;

TEST_CASE("Response file: parsing", "[argo][responsefile][parsing]")
{
    const auto fn = test::data_file("responsefile_parsing", ".rsp", false);
    ResponseFile rsp;
    REQUIRE(rsp.parse(fn));
    const auto argc = rsp.argc();
    REQUIRE(5 == argc);
    const auto argv = rsp.argv();
    std::ostringstream os;
    for (auto idx = 0; idx < argc; ++idx) os << "(idx=" << idx << ", " << argv[idx] << ")" << std::endl;
}

TEST_CASE("Response file: integration", "[argo][responsefile][integration]")
{
    Arguments args{};
    test::RawArguments raw_args;
    bool foo = false;
    {
        const auto fn = test::data_file("responsefile_integration", ".rsp");
        raw_args.push_back("@" + fn);
    }
    {
        handler::Toggle toggle{ "--foo", foo };
        REQUIRE(args.add(toggle));
    }
    const auto result = args.parse(raw_args);
    REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    REQUIRE(!!foo);
}

