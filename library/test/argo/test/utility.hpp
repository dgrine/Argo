#ifndef HEADER_argo_test_utility_hpp_INCLUDE_GUARD
#define HEADER_argo_test_utility_hpp_INCLUDE_GUARD

#include <argo/Argo.hpp>
#include <argo/test/FileComparison.hpp>
#include <argo/test/reflection.hpp>
#include <iostream>
#include <list>
#include <set>
#include <string>
#include <vector>

namespace argo { namespace test {

using RawArguments = std::vector<std::string>;

inline std::string data_file(const std::string &name, const std::string &ext, const bool platform_dependent = false)
{
    std::string fn(argo::test::data_dir);
    fn += "/test/" + name;
    if (platform_dependent)
    {
#if ARGO_TOOLSET_PLATFORM_POSIX
        fn += "_posix";
#elif ARGO_TOOLSET_PLATFORM_WINDOWS
        fn += "_windows";
#endif
    }
    fn += ext;
    return fn;
}

template <typename T>
struct POD
{
    T value;
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const POD<T> &pod)
{
    os << "pod.value: " << pod.value << std::endl;
    return os;
}
template <typename T>
std::ostream &operator<<(std::ostream &os, const POD<core::optional<T>> &pod)
{
    os << "pod.value: ";
    if (!!pod.value) os << *pod.value;
    else os << "null";
    os << std::endl;
    return os;
}
#if ARGO_TOOLSET_CPP_STD_17
template <typename T>
std::ostream &operator<<(std::ostream &os, const POD<core::optional<T>> &pod)
{
    os << "pod.value: ";
    if (!!pod.value) os << *pod.value;
    else os << "null";
    os << std::endl;
    return os;
}
#endif
template <typename T>
std::ostream &operator<<(std::ostream &os, const POD<std::vector<T>> &pod)
{
    os << "pod.value: vector( ";
    std::copy(std::begin(pod.value), std::end(pod.value), std::ostream_iterator<T>(std::cout, " "));
    os << ")" << std::endl;
    return os;
}
template <typename T>
std::ostream &operator<<(std::ostream &os, const POD<std::set<T>> &pod)
{
    os << "pod.value: set( ";
    std::copy(std::begin(pod.value), std::end(pod.value), std::ostream_iterator<T>(std::cout, " "));
    os << ")" << std::endl;
    return os;
}
template <typename T>
std::ostream &operator<<(std::ostream &os, const POD<std::list<T>> &pod)
{
    os << "pod.value: list( ";
    std::copy(std::begin(pod.value), std::end(pod.value), std::ostream_iterator<T>(std::cout, " "));
    os << ")" << std::endl;
    return os;
}

struct VerifyPOD
{
    Arguments &args;
    handler::Option &option;
    const RawArguments &raw_args;

    explicit VerifyPOD(Arguments &args, handler::Option &option, const RawArguments &raw_args)
        : args(args)
        , option(option)
        , raw_args(raw_args)
    {
    }

    template <typename POD>
    bool operator()(const POD &pod)
    {
        if (!args.add(option))
        {
            std::cerr << "Couldn't add option\n";
            return false;
        }
        const auto &result = args.parse(raw_args);
        if (ReturnCode::SuccessAndContinue != result.status)
        {
            std::cerr << pod << std::endl;
            std::cerr << result.message << std::endl;
            return false;
        }
        return true;
    }
};

}} // namespace argo::test

#endif
