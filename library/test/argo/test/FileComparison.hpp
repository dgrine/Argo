#ifndef HEADER_argo_test_FileComparison_hpp_ALREADY_INCLUDED
#define HEADER_argo_test_FileComparison_hpp_ALREADY_INCLUDED

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace argo { namespace test {

    inline bool streams_are_equal(std::istream &ifs_ref, std::istream &ifs_test)
    {
        assert(!!ifs_ref);
        assert(!!ifs_test);
        ifs_ref >> std::noskipws;
        ifs_test >> std::noskipws;
        std::istreambuf_iterator<char> begin_ref{ifs_ref};
        std::istreambuf_iterator<char> begin_test{ifs_test};
        std::istreambuf_iterator<char> end{};

        auto current_ref = begin_ref;
        auto current_test = begin_test;
        while (current_ref != end && current_test != end)
        {
            if (*current_ref != *current_test) return false;
            ++current_ref; ++current_test;
        }
        return (current_ref == end) && (current_test == end);
    }
    inline bool files_are_equal(const std::string &reference, const std::string &test)
    {
        std::ifstream ifs_ref(reference, std::ios::in);
        std::ifstream ifs_test(test, std::ios::in);
        return streams_are_equal(ifs_ref, ifs_test);
    }
    inline bool file_and_stream_are_equal(const std::string &reference, std::istream &test)
    {
        std::ifstream ifs(reference, std::ios::in);
        return streams_are_equal(ifs, test);
    }
    
} }

#endif
