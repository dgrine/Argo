#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Arguments: Copying", "[argo][arguments][copying]")
{
    SECTION("Copy constructor")
    {
        Arguments one{};
        Arguments two{ one };
    }
    SECTION("Copy assignment")
    {
        Arguments one{};
        Arguments two{};
        one = two;
    }
    SECTION("Move constructor")
    {
        Arguments one{};
        Arguments two{std::move(one)};
    }
    SECTION("Move assignment")
    {
        Arguments one{};
        Arguments two{};
        one = std::move(two);
    }
}

TEST_CASE("Arguments: Help", "[argo][arguments][help]")
{
    Configuration config;
    config.program.name.brief = "awesome-app";
    config.program.name.extended = "My Awesome App";
    config.program.description.brief = "Brief description of this app.";
    config.program.description.extended = "Every app can have both a short and long description. Whereas the short description should give the most basic information, this section can go into more detail. In fact, I'm just rambling here to get some line wrapping action going.";
    config.program.version.major = 1;
    config.program.version.githash = "sd45f7";
    Arguments args{ config };
    args.formatter().set_width(80);
    std::string fn_expected;
    std::string var_foo, var_output_file, var_bar, var_input, var_output, var_foo_param, var_bar_param;
    bool var_debug = false;
    SECTION("Basic")
    {
        {
            handler::Option option{ "--foo", var_foo };
            option.help("This is a long description for the option foo which really only serves the purpose to have enough content so that we need multiple lines in the help output. I guess this is enough now.");
            option.required();
            REQUIRE(args.add(option));
        }
        {
            handler::Option option{ "--output-file", var_output_file };
            option.help("Output file");
            option.required();
            REQUIRE(args.add(option));
        }
        {
            handler::Option option{ "--bar", var_bar };
            option.help("A relatively short description. Especially compared to --foo");
            REQUIRE(args.add(option));
        }
        fn_expected = test::data_file("help_basic", ".txt", true);
    }
    SECTION("Advanced")
    {
        {
            handler::Option option{ "--input", var_input };
            option.nargs("+");
            option.required();
            REQUIRE(args.add(option));
        }
        {
            handler::Option option{ "--output", var_output };
            option.required();
            REQUIRE(args.add(option));
        }
        {
            handler::Toggle toggle{"--debug", var_debug};
            toggle.help("Enables debug mode.");
            REQUIRE(args.add(toggle));
        }
        {
            handler::group::Inclusive group{ "Algorithm" };
            {
                handler::group::Exclusive subgroup{ "Foo Algorithm" };
                {
                    handler::Flag flag{ "--foo" };
                    REQUIRE(subgroup.add(flag));
                }
                {
                    handler::Option option{ "--foo-param", var_foo_param };
                    REQUIRE(subgroup.add(option));
                }
                REQUIRE(group.add(subgroup));
            }
            {
                handler::group::Exclusive subgroup{ "Bar Algorithm" };
                {
                    handler::Flag flag{ "--bar" };
                    REQUIRE(subgroup.add(flag));
                }
                {
                    handler::Option option{ "--bar-param", var_bar_param };
                    REQUIRE(subgroup.add(option));
                }
                REQUIRE(group.add(subgroup));
            }
            REQUIRE(args.add(group));
        }
        fn_expected = test::data_file("help_advanced", ".txt", true);
    }
    //Via parsing
    {
        test::RawArguments raw_args = { "-h" };
        const auto result = args.parse(raw_args);
        REQUIRE(ReturnCode::SuccessAndAbort == result.status);
    }
    //Via printing
    {
        std::stringstream actual;
        args.print_help(actual);
#if 1
        REQUIRE(!!test::file_and_stream_are_equal(fn_expected, actual));
#else
        //#NOTE: use the following code to generate the reference files
        std::ofstream of(fn_expected, std::ios::out);
        of << actual.str();
#endif
    }
}

TEST_CASE("Arguments: Unknown options", "[argo][arguments][unknown]")
{
    Arguments args{};
    std::string foo;
    handler::Option option{ "--foo", foo };
    REQUIRE(args.add(option));
    test::RawArguments raw_args;
    std::string expected;
    SECTION("Misspelled existing option")
    {
        raw_args = { "-foo", "1" };
        expected = R"({
  status: "Error",
  message: "Unknown option '-foo'",
  arguments: ["-foo", "1"]
})";
    }
    const auto result = args.parse(raw_args);
    std::ostringstream os;
    os << result;
    REQUIRE(expected == os.str());
}

TEST_CASE("Arguments: Guessing", "[argo][arguments][guessing]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    std::string actual;
    auto set_actual = [&actual](const std::string &origin, const std::string &name) { actual = origin; return true; };
    auto add_option = [&args, &set_actual](const std::string &name) {
        handler::Flag flag{ name };
        //Must use an intermediate std::function with MSVC
        std::function<bool(const std::string &)> set_actual_f = std::bind(set_actual, name, std::placeholders::_1);
        flag.action(action::run(set_actual_f));
        return args.add(flag);
    };
    SECTION("Unambiguous")
    {
        REQUIRE(add_option("--foo"));
        REQUIRE(add_option("-f"));
        SECTION("--foo")
        {
            raw_args = { "--foo" };
        }
        SECTION("-f")
        {
            raw_args = { "-f" };
        }
        expected = raw_args[0];
    }
    SECTION("Ambiguous")
    {
        REQUIRE(add_option("--foo"));
        REQUIRE(add_option("-f"));
        raw_args = { "-f" };
        expected = raw_args[0];
    }
    SECTION("Guessing")
    {
        REQUIRE(add_option("--foo"));
        raw_args = { "-f" };
        expected = "--foo";
    }
    const auto result = args.parse(raw_args);
    REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    REQUIRE(expected == actual);
}

TEST_CASE("Arguments: Merging", "[argo][arguments][merge]")
{
    Arguments one{};
    Arguments two{};
    test::RawArguments raw_args;
    std::string expected;
    std::string actual;
    auto add_option = [&actual](Arguments &args, const std::string &name, const std::string &output) {
        handler::Flag flag{ name };
        flag.action(action::run([&actual, output](const std::string &value) { actual += output; return true; }));
        return args.add(flag);
    };
    SECTION("Disjunct")
    {
        REQUIRE(add_option(one, "--foo", "[foo]"));
        REQUIRE(add_option(two, "--bar", "[bar]"));
        REQUIRE(two.merge(one));
        raw_args = { "--foo", "--bar" };
        expected = "[foo][bar]";
    }
    SECTION("Overlap")
    {
        REQUIRE(add_option(one, "--foo", "[one:foo]"));
        REQUIRE(add_option(two, "--foo", "[two:foo]"));
        REQUIRE(add_option(two, "--bar", "[bar]"));
        bool replace;
        SECTION("Replace")
        {
            replace = true;
            expected = "[one:foo][bar]";
        }
        SECTION("Don't replace")
        {
            replace = false;
            expected = "[two:foo][bar]";
        }
        REQUIRE(two.merge(one, replace));
        raw_args = { "--foo", "--bar" };
    }
    const auto result = two.parse(raw_args);
    REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    REQUIRE(expected == actual);
}

TEST_CASE("Arguments: Switching parser", "[argo][arguments][switch]")
{
    struct
    {
        std::string command;
    } primary_options;
    Arguments primary_parser;
    struct
    {
        std::string profile;
    } secondary_options;
    Arguments secondary_parser;
    //Configure secondary parser
    {
        secondary_parser.add(handler::Option{ "--profile", secondary_options.profile }.required());
    }
    //Configure primary parser
    {
        handler::Positional cmd{ "command", primary_options.command };
        cmd.required();
        cmd.action(action::run([&secondary_parser](core::Context &context) {
            context.switch_parser(secondary_parser);
            return true;
        }));
        primary_parser.add(cmd);
    }

    SECTION("Enough arguments")
    {
        test::RawArguments raw_args = { "my_command", "--profile", "my_profile" };
        const auto &result = primary_parser.parse(raw_args);
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
    }
    SECTION("Not enough arguments")
    {
        test::RawArguments raw_args = { "my_command" };
        const auto &result = primary_parser.parse(raw_args);
        REQUIRE(ReturnCode::Error == result.status);
    }
}

TEST_CASE("Arguments: Multiple parses", "[argo][arguments][multiple][parses]")
{
    Arguments args{};
    std::uint32_t actual;
    std::uint32_t expected = 1234567890;
    handler::Option option{ "--foo", actual };
    REQUIRE(args.add(option));
    test::RawArguments raw_args = { "--foo", std::to_string(expected) };
    for (auto i = 0u; i < 2; ++i)
    {
        const auto result = args.parse(raw_args);
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        REQUIRE(expected == actual);
    }
}
