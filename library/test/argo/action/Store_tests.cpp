#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Action: Store with type conversions", "[argo][action][store][conversion][types]")
{
    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    handler::Option option{ "--foo", "-f" };

    SECTION("unsigned short")
    {
        using type = unsigned short;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
    SECTION("unsigned int")
    {
        using type = unsigned int;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "50k" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 50000 }) == pod.value);
        }
    }
    SECTION("unsigned long")
    {
        using type = unsigned long;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
    SECTION("unsigned long long")
    {
        using type = unsigned long long;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
    SECTION("short")
    {
        using type = short;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "-2", "3", "-4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, -2, 3, -4, 5 }) == pod.value);
        }
    }
    SECTION("int")
    {
        using type = int;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "-2", "3", "-4", "50k" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, -2, 3, -4, 50000 }) == pod.value);
        }
    }
    SECTION("long")
    {
        using type = long;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "-2", "3", "-4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, -2, 3, -4, 5 }) == pod.value);
        }
    }
    SECTION("long long")
    {
        using type = long long;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "-2", "3", "-4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, -2, 3, -4, 5 }) == pod.value);
        }
    }
    SECTION("std::string")
    {
        using type = std::string;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::run([&pod](const type &value) { pod.value = value; return true; }));
            raw_args = { "-f", "test" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE("test" == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::run([&pod](const type &value) { pod.value.push_back(value); return true; }));
            option.nargs("+");
            raw_args = { "-f", "one", "two", "three", "four", "five" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ "one", "two", "three", "four", "five" }) == pod.value);
        }
    }
    SECTION("float")
    {
        using type = float;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1.2" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1.2f == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("*");
            raw_args = { "-f", "1.2", "2.3", "-13.4", "4.5", "5.6" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1.2f, 2.3f, -13.4f, 4.5f, 5.6f }) == pod.value);
        }
    }
    SECTION("double")
    {
        using type = double;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1.2" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1.2 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("*");
            raw_args = { "-f", "1.2", "2.3", "-13.4", "4.5", "5.6" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1.2, 2.3, -13.4, 4.5, 5.6 }) == pod.value);
        }
    }
    SECTION("std::size_t")
    {
        using type = std::size_t;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
    SECTION("std::uint8_t")
    {
        using type = std::uint8_t;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
    SECTION("std::uint16_t")
    {
        using type = std::uint16_t;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
    SECTION("std::uint32_t")
    {
        using type = std::uint32_t;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
#if ARGO_TOOLSET_ARCH_BITS_64
    SECTION("std::uint64_t")
    {
        using type = std::uint64_t;

        SECTION("Single element")
        {
            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "1" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(1 == pod.value);
        }
        SECTION("Vector")
        {
            test::POD<std::vector<type>> pod;
            option.action(action::store(pod.value));
            option.nargs("+");
            raw_args = { "-f", "1", "2", "3", "4", "5" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(std::vector<type>({ 1, 2, 3, 4, 5 }) == pod.value);
        }
    }
#endif
    SECTION("core::optional<int>")
    {
        SECTION("Single element")
        {
            using type = core::optional<int>;

            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "2" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(!!pod.value);
            REQUIRE(2 == *pod.value);
        }
    }
#if ARGO_TOOLSET_CPP_STD_17
    SECTION("std::optional<int>")
    {
        SECTION("Single element")
        {
            using type = std::optional<int>;

            test::POD<type> pod;
            option.action(action::store(pod.value));
            raw_args = { "-f", "2" };
            test::VerifyPOD verify{ args, option, raw_args };
            REQUIRE(verify(pod));
            REQUIRE(!!pod.value);
            REQUIRE(2 == *pod.value);
        }
    }
#endif
}

TEST_CASE("Action: Store scalars and collections", "[argo][action][store][collections]")
{
    using type = int;

    Arguments args{};
    test::RawArguments raw_args;
    std::string expected;
    handler::Option option{ "--foo", "-f" };

    SECTION("Single element")
    {
        test::POD<type> pod;
        option.action(action::store(pod.value));
        raw_args = { "-f", "-123" };
        test::VerifyPOD verify{ args, option, raw_args };
        REQUIRE(verify(pod));
        REQUIRE(-123 == pod.value);
    }
    SECTION("Vector")
    {
        test::POD<std::vector<type>> pod;
        option.action(action::store(pod.value));
        option.nargs("*");
        raw_args = { "-f", "-1", "2", "3", "-4", "-15" };
        test::VerifyPOD verify{ args, option, raw_args };
        REQUIRE(verify(pod));
        REQUIRE(std::vector<type>({ -1, 2, 3, -4, -15 }) == pod.value);
    }
    SECTION("Set")
    {
        test::POD<std::set<type>> pod;
        option.action(action::store(pod.value));
        option.nargs("*");
        raw_args = { "-f", "-1", "2", "3", "-4", "-15", "3" };
        test::VerifyPOD verify{ args, option, raw_args };
        REQUIRE(verify(pod));
        REQUIRE(std::set<type>({ -1, 2, 3, -4, -15 }) == pod.value);
    }
    SECTION("List")
    {
        test::POD<std::list<type>> pod;
        option.action(action::store(pod.value));
        option.nargs("*");
        raw_args = { "-f", "-1", "2", "3", "-4", "-15", "3" };
        test::VerifyPOD verify{ args, option, raw_args };
        REQUIRE(verify(pod));
        REQUIRE(std::list<type>({ -1, 2, 3, -4, -15, 3 }) == pod.value);
    }
}

TEST_CASE("Action: Store with conversion of metric symbols", "[argo][action][store][conversion][metric]")
{
    const std::vector<std::string> symbols = { "D", "h", "k", "K", "M", "G", "T", "P", "E" };
    const std::vector<float> values = { 1e1, 1e2, 1e3, 1e3, 1e6, 1e9, 1e12, 1e15, 1e18 };
    assert(symbols.size() == values.size());
    for (auto idx = 0u; idx < values.size(); ++idx)
    {
        unsigned long long value = 0;
        Arguments args;
        handler::Option option{ "--foo", value };
        REQUIRE(args.add(option));
        test::RawArguments raw_args = { "-f", std::string("1") + symbols[idx] };
        const auto &result = args.parse(raw_args);
        REQUIRE(ReturnCode::SuccessAndContinue == result.status);
        REQUIRE(values[idx] == value);
    }
}

