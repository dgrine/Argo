#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("Action: Run", "[argo][action][run]")
{
    unsigned int foo = 0;
    Arguments args;
    handler::Option option{ "--foo" };
    struct SomeObject
    {
    } obj;
    auto set_foo = [&foo](SomeObject &object, core::Context &, const unsigned int value) {
        foo = value;
        return true;
    };
    option.action(action::run<unsigned int>([&foo](unsigned int value) { foo = value; return true; }));
    option.action(action::run<unsigned int>(std::bind(set_foo, obj, std::placeholders::_1, std::placeholders::_2)));
}

