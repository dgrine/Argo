#include <argo/Argo.hpp>
#include <argo/test/FileComparison.hpp>
#include <argo/test/utility.hpp>
#include <catch2/catch.hpp>

using namespace argo;

TEST_CASE("string::align", "[argo][core][string][align]")
{
    static unsigned case_id = 0u;
    std::string input;
    SECTION("An empty line")
    {
        input = "";
    }
    SECTION("One line")
    {
        input = "This is a short line.";
    }
    SECTION("One line and a half")
    {
        input = "This is an example of a line that is longer than expected "
        "and should definitely be wrapped so that it presents nicely "
        "on the user's screen.";
    }
    SECTION("Two lines")
    {
        input = "This is an example of a line that is longer than expected "
        "and should definitely be wrapped so that it presents nicely "
        "on the user's screen. An extra sentence is added so that we are "
        "sure to have two lines.";
    }
    SECTION("Only spaces")
    {
        SECTION("Less than the page size")
        {
            input.resize(120, ' ');
        }
        SECTION("More than the page size")
        {
            input.resize(20, ' ');
        }
    }
    SECTION("No spaces")
    {
        input.resize(256);
        char ch = 'a';
        for (auto &chin: input)
        {
            chin = ch;
            if ('z' == ch) ch = 'a';
            ++ch;
        }
    }
    SECTION("Lines with carriage returns")
    {
        input = "This is an example of a line that is longer than expected "
        "and should definitely be wrapped so that it presents nicely "
        "on the user's screen. An extra sentence is added so that we are "
        "sure to have two lines.\n"
        "Also note that we are adding line breaks so that we can test how "
        "the formatting reacts to this.\n"
        "\n"
        "Let's hope everything goes well and stays well!";
    }
    SECTION("Lines with color codes")
    {
        input = 
        "\033[1mawesome-app\033[0m --foo (--help) (--version) (--bar)\n"
        "\n"
        "\033[1mNote:\033[0m\n"
        "Groups are indicated with <>\n"
        "Optional groups and options are indicated with ()\n";
    }
    //std::vector<unsigned> page_sizes = {40};
    //std::vector<unsigned> indentations = {8};
    std::vector<unsigned> page_sizes = {40, 80};
    std::vector<unsigned> indentations = {0, 8, 16, 32};
    auto name = [&]()
    {
        std::stringstream ss;
        ss << "align_" << case_id;
        return ss.str();
    };
    for (const auto page_size: page_sizes)
    {
        for (const auto indentation: indentations)
        {
            if (page_size <= indentation) continue;
            const auto result = core::string::align(input, page_size, indentation);
            {
                auto tmp = input;
                core::string::trim(tmp);
                if (tmp.empty()) continue;
            }
            ++case_id;
            std::string fn_expected = test::data_file(name(), ".txt");
            std::stringstream actual;
            actual << "input:\n" << input
                   << "\npage size:   " << page_size
                   << "\nindentation: " << indentation
                   << "\nresult:\n";
            actual << "1   5    10   15   20   25   30   35   40   45   50   55   60   65   70   75   80" << std::endl;
            actual << result << std::endl;
#if 1
            const auto is_same = test::file_and_stream_are_equal(fn_expected, actual);
            if (!is_same)
            {
                std::cerr << "Diff in " << case_id << std::endl;
                std::cerr << actual.str() << std::endl;
            }
            REQUIRE(is_same);
#else
            //#NOTE: use the following code to generate the reference files
            std::ofstream of(fn_expected, std::ios::out);
            of << actual.str();
#endif
        }
    }
}

