#ifndef HEADER_argo_core_Context_hpp_INCLUDE_GUARD
#define HEADER_argo_core_Context_hpp_INCLUDE_GUARD

#include <argo/Configuration.hpp>
#include <argo/Result.hpp>
#include <argo/core/AlwaysTrueOrFalse.hpp>
#include <argo/core/Args.hpp>
#include <argo/core/Error.hpp>
#include <argo/core/Phase.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/Info.hpp>
#include <argo/core/memory.hpp>
#include <map>

ARGO_NS_BEGIN

class Arguments;

namespace core {

class Handlers;

namespace handler {

class IHandler;
}

//!Class that allows access and modification of the parsing state.
class Context
{
public:
    using ParserCouldContinueFunction = std::function<bool(Context &context)>;

    Context(Arguments &parser,
            const Configuration &config,
            const Handlers &handlers,
            Phase &phase,
            ParserCouldContinueFunction parser_could_continue = AlwaysFalse())
        : parser_could_continue(parser_could_continue)
        , pparser_(&parser)
        , config_(config)
        , phandlers_(&handlers)
        , phase_(phase)
    {
    }

    void set_args(Args &args) { pargs_ = &args; }
    const Args &args() const
    {
        assert(!!pargs_);
        return *pargs_;
    }
    Args &args()
    {
        assert(!!pargs_);
        return *pargs_;
    }
    void set_handler(const ARGO_NS(core::handler::IHandler) & handler) { phandler_ = &handler; }
    const ARGO_NS(core::handler::IHandler) & handler() const
    {
        assert(!!phandler_);
        return *phandler_;
    }
    ParserCouldContinueFunction parser_could_continue;
    const Result &result() const { return result_; }
    Result &result() { return result_; }
    //!Returns an Error object for usage within the caller's scope. The result's status will be set to `ReturnCode::Error`.
    Error error() { return Error{ result_ }; }
    //!Signals the parsing to process to abort. The result's status will be set to `ReturnCode::SuccessAndAbort`.
    void abort() { result_.status = ReturnCode::SuccessAndAbort; }
    bool aborted() const { return ReturnCode::SuccessAndAbort == result_.status; }
    //!Switches to another parser which will continue the processing using the current context.
    void switch_parser(Arguments &parser) { pparser_ = &parser; }
    //!Indicates if the given parser is the parser processing the current context.
    bool is_current_parser(const Arguments &parser) { return pparser_ == &parser; }
    const Arguments &parser() const
    {
        assert(!!pparser_);
        return *pparser_;
    }
    Arguments &parser()
    {
        assert(!!pparser_);
        return *pparser_;
    }
    //!Returns the configuration of the active parser.
    const Configuration &config() const { return config_; }
    const Handlers &handlers() const
    {
        assert(!!phandlers_);
        return *phandlers_;
    }
    Phase phase() const { return phase_; }
    void next_phase()
    {
        phase_ = static_cast<Phase>(static_cast<unsigned>(phase_) + 1);
        assert(Phase::Nr_ != phase_); 
    }
    const handler::Info &info(const std::string &name) const { return info_[name]; }
    handler::Info &info(const std::string &name) { return info_[name]; }
    const handler::Info &info(const core::handler::IHandler &handler) const { return info(handler.name()); }
    handler::Info &info(const core::handler::IHandler &handler) { return info(handler.name()); }

private:
    Arguments *pparser_ = nullptr;
    const Configuration config_;
    Args *pargs_ = nullptr;
    const ARGO_NS(core::handler::IHandler) *phandler_ = nullptr;
    const Handlers *phandlers_ = nullptr;
    Phase &phase_;
    Result result_;
    mutable std::map<std::string, handler::Info> info_;
};

} // namespace core

ARGO_NS_END

#endif
