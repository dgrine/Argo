#ifndef HEADER_argo_core_Args_hpp_INCLUDE_GUARD
#define HEADER_argo_core_Args_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/log.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/range.hpp>
#include <iterator>
#include <string>
#include <vector>

ARGO_NS_BEGIN

namespace core {

class Args
{
public:
    using iterator = std::vector<std::string>::iterator;

    explicit Args(std::vector<std::string> &arguments)
        : data_(arguments)
        , begin_(std::begin(data_))
        , end_(std::end(data_))
        , current_(begin_)
    {
    }

    iterator begin() const { return begin_; }
    iterator end() const { return end_; }
    iterator current() const { return current_; }
    bool reached_end() const { return end_ == current_; }
    bool next()
    {
        MSS_BEGIN(bool);
        MSS(end_ != current_);
        ++current_;
        MSS_END();
    }
    void replace(const std::vector<std::string> &arguments)
    {
        S(ARGO_DEBUG);
        auto idx = std::distance(begin(), current());
        std::copy(RANGE(arguments), std::inserter(data_, current() + 1));
        auto it = std::begin(data_) + idx;
        data_.erase(it);
        begin_ = std::begin(data_);
        end_ = std::end(data_);
        current_ = begin_ + idx;
        L("Replacing current argument with new arguments in-place:");
        for (const auto &arg : data_) L(C(arg));
    }

private:
    std::vector<std::string> &data_; //The underlying data must exist for the entire duration of the parsing
    iterator begin_;
    iterator end_;
    iterator current_;
};

} // namespace core

ARGO_NS_END

#endif
