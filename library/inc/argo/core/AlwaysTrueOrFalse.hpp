#ifndef HEADER_argo_core_AlwaysTrue_hpp_INCLUDE_GUARD
#define HEADER_argo_core_AlwaysTrue_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>

ARGO_NS_BEGIN

namespace core {

template <bool value>
struct AlwaysTrueOrFalse
{
    template <typename... Args>
    bool operator()(Args &&... args) const
    {
        return value;
    }
};

using AlwaysTrue = AlwaysTrueOrFalse<true>;
using AlwaysFalse = AlwaysTrueOrFalse<false>;

} // namespace core

ARGO_NS_END

#endif

