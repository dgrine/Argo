#ifndef HEADER_argo_core_action_IAction_hpp_INCLUDE_GUARD
#define HEADER_argo_core_action_IAction_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/memory.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core {

class Context;

namespace action {

class IAction;
using Ptr = std::unique_ptr<IAction>;

class IAction
{
public:
    virtual ~IAction() = default;

    virtual Ptr clone() const = 0;
    virtual bool apply(core::Context &) = 0;
};

} // namespace action

} // namespace core

ARGO_NS_END

#endif

