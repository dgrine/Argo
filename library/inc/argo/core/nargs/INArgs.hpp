#ifndef HEADER_argo_core_nargs_INArgs_hpp_INCLUDE_GUARD
#define HEADER_argo_core_nargs_INArgs_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/memory.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace nargs {

class INArgs;
using Ptr = std::unique_ptr<INArgs>;

class INArgs
{
public:
    virtual ~INArgs() = default;

    virtual std::string description() const = 0;
    virtual std::string symbol() const = 0;

    virtual Ptr clone() const = 0;
    virtual bool must_have_next() const = 0;
    virtual bool may_have_next() const = 0;
    virtual void next() = 0;
    virtual void reset() = 0;
};

}} // namespace core::nargs

ARGO_NS_END

#endif

