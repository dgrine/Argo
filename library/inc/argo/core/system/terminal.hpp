#ifndef HEADER_argo_core_system_utility_hpp_INCLUDE_GUARD
#define HEADER_argo_core_system_utility_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/config/toolset.hpp>
#if ARGO_TOOLSET_PLATFORM_POSIX
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#elif ARGO_TOOLSET_PLATFORM_WINDOWS
#include <windows.h>
#endif

ARGO_NS_BEGIN

namespace core { namespace system { namespace terminal {

inline bool is_tty()
{
#if ARGO_TOOLSET_PLATFORM_POSIX
    return isatty(fileno(stdout));
#else
    return false;
#endif
}
inline bool is_pty() { return !is_tty(); }

inline unsigned int width()
{
    const auto DEFAULT = 80u;
    if (is_pty()) return DEFAULT;
#if ARGO_TOOLSET_PLATFORM_POSIX
    struct winsize w;
    if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w)) return DEFAULT;
    return w.ws_col == 0 ? 80 : w.ws_col;
#else
    return DEFAULT;
#endif
}
inline unsigned int height()
{
    const auto DEFAULT = 32;
    if (is_pty()) return DEFAULT;
#if ARGO_TOOLSET_PLATFORM_POSIX
    struct winsize w;
    if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w)) return DEFAULT;
    return w.ws_row;
#else
    return DEFAULT;
#endif
}

}}} // namespace core::system::terminal

ARGO_NS_END

#endif
