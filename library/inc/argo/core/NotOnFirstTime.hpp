#ifndef HEADER_argo_core_NotOnFirstTime_hpp_INCLUDE_GUARD
#define HEADER_argo_core_NotOnFirstTime_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>

ARGO_NS_BEGIN

namespace core {

class NotOnFirstTime
{
public:
    template <typename Functor>
    void operator()(Functor functor) const
    {
        if (!first_time_)
            functor();
        else
            first_time_ = false;
    }
    void reset() { first_time_ = true; }

private:
    mutable bool first_time_ = true;
};

} // namespace core

ARGO_NS_END

#endif

