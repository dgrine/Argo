#ifndef HEADER_argo_core_Error_hpp_INCLUDE_GUARD
#define HEADER_argo_core_Error_hpp_INCLUDE_GUARD

#include <argo/Result.hpp>
#include <argo/core/config/build.hpp>
#include <sstream>

ARGO_NS_BEGIN

namespace core {

class Context;

//!Class that sets the current result's status to `ReturnCode::Error` and assigns the given message.
class Error
{
public:
    Error(const Error &other)
        : result_(other.result_)
    {
        os_ << other.os_.str();
    }
    ~Error()
    {
        auto message = os_.str();
        result_.message = message.empty() ? "Parsing failed" : message;
        result_.status = ReturnCode::Error;
    }
    //!Appends the given message and returns the object.
    template <typename T>
    Error &operator<<(T &&value)
    {
        os_ << value;
        return *this;
    }

private:
    friend class Context;

    explicit Error(Result &result)
        : result_(result)
    {
    }

    Result &result_;
    std::ostringstream os_;
};

} // namespace core

ARGO_NS_END

#endif
