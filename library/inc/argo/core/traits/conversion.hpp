#ifndef HEADER_argo_core_traits_conversion_hpp_INCLUDE_GUARD
#define HEADER_argo_core_traits_conversion_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/config/toolset.hpp>
#include <argo/core/convert.hpp>
#include <argo/core/optional.hpp>
#include <argo/core/range.hpp>
#include <argo/core/string/utility.hpp>
#include <cstdint>
#include <string>
#include <type_traits>
#include <limits>

ARGO_NS_BEGIN

namespace core { namespace traits {

template <typename T, typename = void>
struct conversion;

#define REPLACE_METRIC_SYMBOLS                    \
    if (value.size() - 1 == value.find("D"))      \
        string::replace_all(value, "D", "e1");    \
    else if (value.size() - 1 == value.find("h"))      \
        string::replace_all(value, "h", "e2");    \
    else if (value.size() - 1 == value.find("k"))      \
        string::replace_all(value, "k", "e3");    \
    else if (value.size() - 1 == value.find("K")) \
        string::replace_all(value, "K", "e3");    \
    else if (value.size() - 1 == value.find("M")) \
        string::replace_all(value, "M", "e6");    \
    else if (value.size() - 1 == value.find("G")) \
        string::replace_all(value, "G", "e9");    \
    else if (value.size() - 1 == value.find("T")) \
        string::replace_all(value, "T", "e12");   \
    else if (value.size() - 1 == value.find("P")) \
        string::replace_all(value, "P", "e15");   \
    else if (value.size() - 1 == value.find("E")) \
        string::replace_all(value, "E", "e18");
//Upper value limit

#define DEFINE_INTEGRAL_CONVERSION(T, D)                                              \
    template <>                                                                       \
    struct conversion<T>                                                              \
    {                                                                                 \
        using result_type = T;                                                        \
        static constexpr const char *description = D;                                 \
        static optional<result_type> run(std::string value)                           \
        {                                                                             \
            if (std::string::npos != value.find(".")) return nullopt;                 \
            REPLACE_METRIC_SYMBOLS                                                    \
            double dvalue;                                                            \
            if (!convert(dvalue, value)) return nullopt;                              \
            if (dvalue > std::numeric_limits<result_type>::max()) return nullopt;     \
            return static_cast<result_type>(dvalue);                                  \
        }                                                                             \
    };
DEFINE_INTEGRAL_CONVERSION(unsigned short, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(unsigned int, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(unsigned long, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(unsigned long long, "an unsigned integer");
DEFINE_INTEGRAL_CONVERSION(short, "an integer");
DEFINE_INTEGRAL_CONVERSION(int, "an integer");
DEFINE_INTEGRAL_CONVERSION(long, "an integer");
DEFINE_INTEGRAL_CONVERSION(long long, "an integer");

template <>
struct conversion<std::string>
{
    using result_type = std::string;
    static constexpr const char *description = "a string";
    static optional<std::string> run(const std::string &value) { return value; }
};
template <>
struct conversion<const char *>: conversion<std::string> {};
template <>
struct conversion<float>
{
    using result_type = float;
    static constexpr const char *description = "a float";
    static optional<result_type> run(std::string value)
    {
        REPLACE_METRIC_SYMBOLS;
        float fvalue;
        if (!convert(fvalue, value)) return nullopt;
        return fvalue;
    }
};
template <>
struct conversion<double>
{
    using result_type = double;
    static constexpr const char *description = "a double";
    static optional<result_type> run(std::string value)
    {
        REPLACE_METRIC_SYMBOLS;
        double dvalue;
        if (!convert(dvalue, value)) return nullopt;
        return dvalue;
    }
};
template <>
struct conversion<bool>
{
    using result_type = bool;
    static constexpr const char *description = "a boolean";
    static optional<result_type> run(const std::string &value)
    {
        const auto lval = core::string::tolower(value);
        if ("true" == lval || "yes" == lval || "y" == lval || "1" == lval)
            return true;
        else if ("false" == lval || "no" == lval || "n" == lval || "0" == lval)
            return false;
        else
            return nullopt;
    }
};
#if ARGO_TOOLSET_CPP_STD_17 || ARGO_ENABLE_STD_OPTIONAL
template <typename T>
struct conversion<std::optional<T>>
{
    using result_type = std::optional<T>;
    static constexpr const char *description = conversion<T>::description;
    static optional<result_type> run(const std::string &value)
    {
        const auto result = conversion<T>::run(value);
        if (!!result)
            return result_type{ *result };
        else return nullopt;
    }
};
#endif
template <typename T>
struct conversion<optional<T>>
{
    using result_type = optional<T>;
    static constexpr const char *description = conversion<T>::description;
    static optional<result_type> run(const std::string &value)
    {
        const auto result = conversion<T>::run(value);
        if (!!result)
            return result_type{ *result };
        else return nullopt;
    }
};
template <typename T>
struct conversion<T, typename std::enable_if<std::is_enum<T>::value>::type>
{
    using result_type = T;
    static constexpr const char *description = "an unsigned integer mapped to an enumeration";
    static optional<result_type> run(const std::string &value)
    {
        const auto converted = conversion<unsigned>::run(value);
        if (!converted) return nullopt;
        const auto number = *converted;
        if (number >= static_cast<unsigned>(T::Nr_)) return nullopt;
        return static_cast<T>(number);
    }
};
template <typename T>
struct conversion<T, typename std::enable_if<std::is_integral<T>::value>::type>
{
    using result_type = T;
    static constexpr const char *description = "an integer";
    static optional<result_type> run(const std::string &value)
    {
        const auto result = conversion<int>::run(value);
        if (!!result)
            return *result;
        else
            return nullopt;
    }
};

}} // namespace core::traits

ARGO_NS_END

#endif

