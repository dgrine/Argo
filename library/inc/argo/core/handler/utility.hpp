#ifndef HEADER_argo_core_handler_utility_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_utility_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/string/utility.hpp>

ARGO_NS_BEGIN

namespace core { namespace handler {

inline std::string name_of(const IHandler &handler, const bool uppercase = false)
{
    auto type = handler.type();
    if (!uppercase)
    {
        assert(!type.empty());
        string::tolower(std::begin(type), std::begin(type) + 1);
    }
    std::ostringstream os;
    os << type << " " << string::surround(handler.name(), "'");
    return os.str();
}

inline void dash(std::string &name) { core::string::replace_all(name, "_", "-"); }

inline void prefix(std::string &name)
{
    if (name.empty() || core::string::starts_with(name, "--") || core::string::starts_with(name, "-")) return;
    if (name.size() > 2) name = std::string("--") + name;
    else name = std::string("-") + name;
}

inline void prefix(std::string &first, std::string &second)
{
    if (first.empty() || second.empty()) return;
    if (first.size() == second.size()) return;

    if (core::string::starts_with(first, "--"))
    {
        if (core::string::starts_with(second, "-")) return;
        else second = std::string("-") + second;
    }
    else if (core::string::starts_with(second, "--"))
    {
        if (core::string::starts_with(first, "-")) return;
        else first = std::string("-") + first;
    }
    else if (core::string::starts_with(first, "-"))
    {
        assert(!core::string::starts_with(first, "--"));
        assert(!core::string::starts_with(second, "--"));
        second = std::string("--") + second;
    }
    else if (core::string::starts_with(second, "-"))
    {
        assert(!core::string::starts_with(first, "--"));
        assert(!core::string::starts_with(second, "--"));
        first = std::string("--") + first;
    }
    else
    {
        assert(!core::string::starts_with(first, "-"));
        assert(!core::string::starts_with(first, "--"));
        assert(!core::string::starts_with(second, "-"));
        assert(!core::string::starts_with(second, "--"));
        if (first.size() > second.size())
        {
            first = std::string("--") + first;
            second = std::string("-") + second;
        }
        else
        {
            assert(first.size() < second.size());
            first = std::string("-") + first;
            second = std::string("--") + second;
        }
    }
}

}} // namespace core::handler

ARGO_NS_END

#endif
