#ifndef HEADER_argo_core_handler_IHandler_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_IHandler_hpp_INCLUDE_GUARD

#include <argo/core/Context_fwd.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <argo/core/log.hpp>
#include <argo/core/memory.hpp>
#include <argo/core/mss.hpp>
#include <functional>
#include <ostream>
#include <string>
#include <vector>

ARGO_NS_BEGIN

class Arguments;

namespace core { namespace handler {

class IHandler;
using Ptr = std::unique_ptr<IHandler>;

class IHandler
{
public:
    virtual ~IHandler() = default;

    //!Indicates if the handler is correctly configured.
    virtual bool is_valid() const = 0;
    //!Returns the handler's name.
    virtual std::string name() const = 0;
    //!Returns the handler's type description.
    virtual std::string type() const = 0;
    //!Accepts a constant visitor. Returns a boolean indicating success.
    virtual bool accept(visitor::IConstVisitor &visitor) const = 0;
    //!Accepts a mutating visitor. Returns a boolean indicating success.
    virtual bool accept(visitor::IMutatingVisitor &visitor) = 0;
    //!Returns a cloned version of the handler.
    virtual Ptr clone() const = 0;
    //!Takes over the parsing process in the current context. Returns a boolean indicating success.
    virtual bool parse(Context &context) = 0;
    //!Indicates if the handler is satisfied were the parsing to stop at this point.
    virtual bool is_satisfied(Context &context) const = 0;

    const IHandler *parent() const { return pparent_; }
    void set_parent(const IHandler &handler) const { assert(&handler != this); pparent_ = &handler; }

protected:
    mutable const IHandler *pparent_ = nullptr;

private:
    friend class ARGO_NS(Arguments);

    virtual void reset_() {}
};

inline std::ostream &operator<<(std::ostream &os, const IHandler &handler) { return os << handler.name(); }

}} // namespace core::handler

ARGO_NS_END

#endif
