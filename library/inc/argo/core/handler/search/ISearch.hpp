#ifndef HEADER_argo_core_handler_search_ISearch_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_search_ISearch_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <argo/core/memory.hpp>
#include <argo/handler/group/Interface.hpp>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace search {

class ISearch : public visitor::IConstVisitor
{
public:
    ISearch(Context &context)
        : context_(context)
    {
    }

    IHandler *result() const { return const_cast<IHandler *>(presult_); }

    virtual bool visit(const ARGO_NS(handler::group::Interface) & group)
    {
        MSS_BEGIN(bool);
        for (const auto &handler : group) MSS(handler.accept(*this));
        MSS_END();
    }

protected:
    Context &context_;
    const IHandler *presult_ = nullptr;

};

using Ptr = std::unique_ptr<ISearch>;

}}} // namespace core::handler::search

ARGO_NS_END

#endif
