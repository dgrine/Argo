#ifndef HEADER_argo_core_handler_search_Option_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_search_Option_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/search/ISearch.hpp>
#include <argo/handler/Flag.hpp>
#include <argo/handler/Option.hpp>
#include <argo/handler/Toggle.hpp>
#include <argo/handler/group/Interface.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace search {

class Option : public ISearch
{
public:
    using ISearch::visit;

    explicit Option(Context &context)
        : ISearch(context)
    {
    }

    virtual bool visit(const ARGO_NS(handler::Flag) & flag) { return handle_(flag); }
    virtual bool visit(const ARGO_NS(handler::Interface) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Option) & option) { return handle_(option); }
    virtual bool visit(const ARGO_NS(handler::Positional) & positional) { return true; }
    virtual bool visit(const ARGO_NS(handler::Toggle) & toggle)
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        MSS(handle_(toggle));
        assert(context_.args().current() != context_.args().end());
        auto name = *context_.args().current();
        if (context_.config().parser.dash_names) dash(name);
        const auto negation_longhand = toggle.negation_longhand();
        L("Checking toggle negation: " << C(negation_longhand));
        if (negation_longhand == name)
        {
            L("Found perfect toggle negation handler");
            presult_ = &toggle;
            return false;
        }
        return handle_(toggle);
        MSS_END();
    }

private:
    template <typename Handler>
    bool handle_(const Handler &handler)
    {
        S(ARGO_DEBUG);
        assert(context_.args().current() != context_.args().end());
        auto name = *context_.args().current();
        if (context_.config().parser.dash_names) dash(name);
        const auto &named = static_cast<const property::Named &>(handler);
        if (named.has_longhand() && name == named.longhand())
        {
            L("Found perfect longhand match, stopping here");
            presult_ = &handler;
            return false; //stop
        }
        else if (named.has_shorthand() && name == named.shorthand())
        {
            L("Found perfect shorthand match, stopping here");
            presult_ = &handler;
            return false; //stop
        }
        else if (named.has_longhand() && !named.has_shorthand() && name == name::guess_shorthand(named.longhand()))
        {
            L("Found shorthand match after guessing, continuing in case there's a better match");
            presult_ = &handler;
        }
        return true; //continue
    }
};

}}} // namespace core::handler::search

ARGO_NS_END

#endif
