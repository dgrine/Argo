#ifndef HEADER_argo_core_handler_search_Positional_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_search_Positional_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/search/ISearch.hpp>
#include <argo/handler/Positional.hpp>
#include <argo/handler/group/Interface.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace search {

class Positional : public ISearch
{
public:
    using ISearch::visit;

    explicit Positional(Context &context)
        : ISearch(context)
    {
    }

    virtual bool visit(const ARGO_NS(handler::Interface) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Flag) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Option) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Positional) & positional)
    {
        assert(context_.args().current() != context_.args().end());
        const auto name = *context_.args().current();
        if (name::is_option(name)) return true;
        if (!context_.info(positional).nr_parses)
        {
            presult_ = &positional;
            return false; //stop
        }
        return true; //continue
    }
    virtual bool visit(const ARGO_NS(handler::Toggle) &) { return true; }
};

}}} // namespace core::handler::search

ARGO_NS_END

#endif
