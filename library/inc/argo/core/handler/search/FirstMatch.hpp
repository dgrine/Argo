#ifndef HEADER_argo_core_handler_search_FirstMatch_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_search_FirstMatch_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/search/ISearch.hpp>
#include <argo/handler/Flag.hpp>
#include <argo/handler/Interface.hpp>
#include <argo/handler/Option.hpp>
#include <argo/handler/Positional.hpp>
#include <argo/handler/Toggle.hpp>
#include <argo/handler/group/Interface.hpp>
#include <vector>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace search {

class FirstMatch : public ISearch
{
public:
    using ISearch::visit;

    explicit FirstMatch(Context &context)
        : ISearch(context)
    {
    }

    template <typename Searcher, typename... Args>
    void add(Args &&... args)
    {
        searchers_.push_back(make_unique<Searcher>(context_, std::forward<Args>(args)...));
    }
    void clear()
    {
        searchers_.clear();
        presult_ = nullptr;
    }

    virtual bool visit(const ARGO_NS(handler::Flag) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Interface) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Option) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Positional) & handler) { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Toggle) & handler) { return visit_(handler); }

private:
    bool visit_(const core::handler::IHandler &handler)
    {
        for (const auto &psearcher : searchers_)
        {
            assert(!!psearcher);
            const bool continue_search = handler.accept(*psearcher);
            presult_ = psearcher->result();
            if (!continue_search) return false; //stop
        }
        return true; //continue
    }

    std::vector<Ptr> searchers_;
};

}}} // namespace core::handler::search

ARGO_NS_END

#endif
