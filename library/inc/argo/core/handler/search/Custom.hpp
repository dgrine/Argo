#ifndef HEADER_argo_core_handler_search_Custom_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_search_Custom_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/search/ISearch.hpp>
#include <argo/core/mss.hpp>
#include <argo/handler/Interface.hpp>
#include <argo/handler/group/Interface.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace search {

class Custom : public ISearch
{
public:
    using ISearch::visit;

    explicit Custom(Context &context)
        : ISearch(context)
    {
    }

    virtual bool visit(const ARGO_NS(handler::Interface) & custom)
    {
        if (custom.recognizes_(context_))
        {
            presult_ = &custom;
            return false; //stop
        }
        return true; //continue
    }
    virtual bool visit(const ARGO_NS(handler::Flag) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Option) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Positional) &) { return true; }
    virtual bool visit(const ARGO_NS(handler::Toggle) &) { return true; }
};

}}} // namespace core::handler::search

ARGO_NS_END

#endif
