#ifndef HEADER_argo_core_handler_Info_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_Info_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <sstream>

ARGO_NS_BEGIN

namespace core { namespace handler {

struct Info
{
    unsigned nr_parses = 0;
};

}} // namespace core::handler

ARGO_NS_END

#endif
