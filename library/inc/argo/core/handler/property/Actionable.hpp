#ifndef HEADER_argo_core_handler_property_Actionable_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_property_Actionable_hpp_INCLUDE_GUARD

#include <argo/action/store.hpp>
#include <argo/core/Context_fwd.hpp>
#include <argo/core/action/IAction.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/log.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/type_traits.hpp>
#include <vector>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace property {

template <typename Handler>
class Actionable
{
public:
    Actionable() = default;
    Actionable(const Actionable &other)
    {
        assert(actions_().empty());
        for (const auto &paction : other.actions_())
        {
            assert(!!paction);
            actions_().push_back(paction->clone()); //Not calling action() as this results in a false positive with ASAN (invalid downcast)
        }
    }

    //!Adds an action to the handler and returns it.
    Handler &action(const action::IAction &action)
    {
        actions_().push_back(action.clone());
        return static_cast<Handler &>(*this);
    }
    //!Adds a requirement to the handler and returns it. Note that this is syntactic sugar since it has the same effect as calling ``action``.
    Handler &require(const action::IAction &requirement) { return action(requirement); }

protected:
    const std::vector<action::Ptr> &actions_() const { return actions__; }
    std::vector<action::Ptr> &actions_() { return actions__; }
    bool run_(core::Context &context)
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        for (const auto &paction : actions_())
        {
            assert(!!paction);
            L("Applying action on value");
            MSS(paction->apply(context));
            L("Finished applying action on value");
        }
        MSS_END();
    }
    template<typename Variable>
    void add_store_action_(Variable &variable)
    {
        action(ARGO_NS(action::store(variable)));
        if (core::is_collection(variable)) static_cast<Handler &>(*this).nargs("*");
        else static_cast<Handler &>(*this).nargs(1);
    }

private:
    std::vector<action::Ptr> actions__;
};

}}} // namespace core::handler::property

ARGO_NS_END

#endif
