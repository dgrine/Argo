#ifndef HEADER_argo_core_handler_property_Help_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_property_Help_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace property {

template <typename Handler>
class Help
{
public:
    Help() = default;
    Help(const Help &other) = default;

    //!Returns the help description.
    std::string help() const { return description_; }
    //!Sets the help description and returns the handler.
    Handler &help(const std::string description)
    {
        description_ = description;
        return static_cast<Handler &>(*this);
    }

protected:
    std::string description_;
};

}}} // namespace core::handler::property

ARGO_NS_END

#endif
