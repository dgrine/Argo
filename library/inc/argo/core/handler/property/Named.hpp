#ifndef HEADER_argo_core_handler_property_Named_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_property_Named_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/optional.hpp>
#include <argo/core/utility.hpp>
#include <cassert>
#include <ostream>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace property {

class Named
{
public:
    explicit Named(std::string name)
    {
        dash(name);
        prefix(name);
        if (name::is_longhand(name))
            longhand_ = name;
        else if (name::is_shorthand(name))
            shorthand_ = name;
        validate_();
    }
    explicit Named(std::string first, std::string second)
    {
        dash(first);
        dash(second);
        prefix(first, second);
        if (name::is_longhand(first))
            longhand_ = first;
        else if (name::is_shorthand(first))
            shorthand_ = first;
        if (name::is_longhand(second))
            longhand_ = second;
        else if (name::is_shorthand(second))
            shorthand_ = second;
        validate_();
    }
    Named(const Named &other) = default;

    //!Indicates if a longhand is set.
    bool has_longhand() const { return !!longhand_; }
    //!Returns the longhand. Note that the longhand must be set.
    std::string longhand() const
    {
        assert(has_longhand());
        return *longhand_;
    }
    //!Indicates if a shorthand is set.
    bool has_shorthand() const { return !!shorthand_; }
    //!Returns the shorthand. Note that the shorthand must be set.
    std::string shorthand() const
    {
        assert(has_shorthand());
        return *shorthand_;
    }

protected:
    void validate_()
    {
        is_valid__ = has_longhand() || has_shorthand();
        if (has_longhand()) is_valid__ = name::is_longhand(*longhand_);
        if (has_shorthand()) is_valid__ = is_valid__ && name::is_shorthand(*shorthand_);
    }
    std::string name_() const
    {
        std::ostringstream os;
        if (has_longhand())
        {
            os << longhand();
            if (has_shorthand()) os << " (" << shorthand() << ")";
        }
        else if (has_shorthand())
        {
            os << shorthand();
        }
        return os.str();
    }
    bool is_valid_() const { return is_valid__; }

private:
    core::optional<std::string> longhand_;
    core::optional<std::string> shorthand_;
    bool is_valid__ = true;
};

}}} // namespace core::handler::property

ARGO_NS_END

#endif
