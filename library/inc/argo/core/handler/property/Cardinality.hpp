#ifndef HEADER_argo_core_handler_property_Cardinality_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_property_Cardinality_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/mss.hpp>
#include <argo/nargs/FixedNumber.hpp>
#include <argo/nargs/OneOrMore.hpp>
#include <argo/nargs/Optional.hpp>
#include <argo/nargs/ZeroOrMore.hpp>
#include <string>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace property {

template <typename Handler>
class Cardinality
{
public:
    Cardinality()
        : pnargs_(make_unique<ARGO_NS(nargs::FixedNumber)>(1))
    {
    }
    Cardinality(const Cardinality &other)
        : pnargs_(!!other.pnargs_ ? other.pnargs_->clone() : nullptr)
    {
    }

    //!Sets the expected number of arguments and returns the handler.
    Handler &nargs(const std::string &type)
    {
        if ("+" == type)
            pnargs_ = make_unique<ARGO_NS(nargs::OneOrMore)>();
        else if ("*" == type)
            pnargs_ = make_unique<ARGO_NS(nargs::ZeroOrMore)>();
        else if ("?" == type)
            pnargs_ = make_unique<ARGO_NS(nargs::Optional)>();
        else
            is_valid__ = false;
        return static_cast<Handler &>(*this);
    }
    //!Sets the expected fixed number of arguments and returns the handler.
    Handler &nargs(const unsigned nr)
    {
        if (0 == nr)
            is_valid__ = false;
        else
            pnargs_ = make_unique<ARGO_NS(nargs::FixedNumber)>(nr);
        return static_cast<Handler &>(*this);
    }
    //!Returns the cardinality description.
    std::string nargs_type() const { return nargs_().description(); }
    //!Returns the cardinality symbol.
    std::string nargs_symbol() const { return nargs_().symbol(); }

protected:
    const core::nargs::INArgs &nargs_() const
    {
        assert(!!pnargs_);
        return *pnargs_;
    }
    core::nargs::INArgs &nargs_()
    {
        assert(!!pnargs_);
        return *pnargs_;
    }
    bool is_valid_() const { return is_valid__; }
    bool is_satisfied_() const
    {
        MSS_BEGIN(bool);
        MSS(!nargs_().must_have_next());
        MSS_END();
    }

private:
    core::nargs::Ptr pnargs_;
    bool is_valid__ = true;
};

}}} // namespace core::handler::property

ARGO_NS_END

#endif
