#ifndef HEADER_argo_core_handler_property_Required_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_property_Required_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/optional.hpp>
#include <functional>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace property {

template <typename Handler>
class Required
{
public:
    using Condition = std::function<bool()>;

    Required() = default;
    Required(const Required &other) = default;

    //!Indicates if the handler is required.
    bool is_required() const { return !condition_ ? false : (*condition_)(); }
    //!Marks the handler as required and returns it.
    Handler &required(const Condition &condition = []() { return true; })
    {
        condition_ = condition;
        return static_cast<Handler &>(*this);
    }
    //!Indicates if the handler is optional. This is the default state.
    bool is_optional() const { return !is_required(); }
    //!Marks the handler as optional and returns it.
    Handler &optional(const Condition &condition = []() { return false; })
    {
        condition_ = condition;
        return static_cast<Handler &>(*this);
    }

    bool has_user_defined_required_or_optional() const { return !!condition_; }

protected:
    bool is_satisfied_(core::Context &context) const { return is_required() ? context.info(static_cast<const Handler &>(*this)).nr_parses : true; }

private:
    core::optional<Condition> condition_;
};

}}} // namespace core::handler::property

ARGO_NS_END

#endif
