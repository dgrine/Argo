#ifndef HEADER_argo_core_handler_visitor_Collector_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_visitor_Collector_hpp_INCLUDE_GUARD

#include <argo/core/AlwaysTrueOrFalse.hpp>
#include <argo/core/Handlers.hpp>
#include <argo/core/assert.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <argo/handler/all.hpp>

ARGO_NS_BEGIN

namespace core {
namespace handler {
namespace visitor {

namespace collector {

template <typename Handler, bool is_const>
class NonRecursive : public IVisitor<is_const> {
    template <typename Type>
    using type_of = visitor::type_of<is_const, Type>;
    using pointer_type =
        typename std::conditional<is_const, const handler::IHandler *,
                                  handler::IHandler *>::type;

   public:
    using handler_type = type_of<Handler>;
    using value_type = pointer_type;
    using data_type = std::vector<value_type>;
    using function_type = std::function<bool(handler_type)>;

    data_type result() const { return result_; }
    bool foreach (const function_type &fnc,
                  const function_type &predicate = AlwaysTrue{}) {
        MSS_BEGIN(bool);
        for (const auto phandler : result()) {
            assert(!!phandler);
            auto &handler = static_cast<handler_type>(*phandler);
            if (!predicate(handler)) continue;
            MSS(fnc(handler));
        }
        MSS_END();
    }

   private:
    virtual bool visit(type_of<ARGO_NS(handler::Flag)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Interface)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Option)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Positional)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(
        type_of<ARGO_NS(handler::group::Interface)> group) override {
        return visit_(group);
    }

    template <typename Type>
    bool visit_(const Type &handler) {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    template <typename Type>
    bool visit_(Type &handler) {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    void collect_(pointer_type phandler) { result_.push_back(phandler); }

    data_type result_;
};

template <typename Handler>
using ConstNonRecursive = NonRecursive<Handler, true>;
template <typename Handler>
using NonConstNonRecursive = NonRecursive<Handler, false>;

template <typename Handler, bool is_const>
class Recursive : public IVisitor<is_const> {
    template <typename Type>
    using type_of = visitor::type_of<is_const, Type>;
    using pointer_type =
        typename std::conditional<is_const, const handler::IHandler *,
                                  handler::IHandler *>::type;

   public:
    using handler_type = type_of<Handler>;
    struct value_type {
        explicit value_type(pointer_type phandler, unsigned depth)
            : phandler(phandler), depth(depth) {}

        pointer_type phandler = nullptr;
        unsigned depth = 0u;
    };
    using data_type = std::vector<value_type>;
    using function_type = std::function<bool(handler_type, unsigned)>;

    data_type result() const { return result_; }
    bool foreach (const function_type &fnc,
                  const function_type &predicate = AlwaysTrue{}) {
        MSS_BEGIN(bool);
        for (const auto &item : result()) {
            assert(!!item.phandler);
            auto &handler = static_cast<handler_type>(*item.phandler);
            if (!predicate(handler, item.depth)) continue;
            MSS(fnc(handler, item.depth));
        }
        MSS_END();
    }

   private:
    virtual bool visit(type_of<ARGO_NS(handler::Flag)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Interface)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Option)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Positional)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)> handler) override {
        return visit_(handler);
    }
    virtual bool visit(
        type_of<ARGO_NS(handler::group::Interface)> group) override {
        MSS_BEGIN(bool);
        MSS(visit_(group));
        ++depth_;
        MSS(group.accept_on_elements(*this));
        --depth_;
        MSS_END();
    }
    template <typename Type>
    bool visit_(const Type &handler) {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    template <typename Type>
    bool visit_(Type &handler) {
        if (std::is_same<Handler, Type>::value) collect_(&handler);
        return true;
    }
    void collect_(pointer_type phandler) {
        result_.emplace_back(phandler, depth_);
    }

    data_type result_;
    unsigned depth_ = 0u;
};

template <typename Handler>
using ConstRecursive = Recursive<Handler, true>;
template <typename Handler>
using NonConstRecursive = Recursive<Handler, false>;

}  // namespace collector

template <typename Collector, typename Handler>
bool foreach (const Handler &handler, typename Collector::function_type fnc,
              typename Collector::function_type predicate = AlwaysTrue{}) {
    MSS_BEGIN(bool);
    Collector collector;
    ARGO_ASSERT_RELEASE(handler.accept_on_elements(collector));
    MSS(collector.foreach (fnc, predicate));
    MSS_END();
}

}  // namespace visitor
}  // namespace handler
}  // namespace core

ARGO_NS_END

#endif
