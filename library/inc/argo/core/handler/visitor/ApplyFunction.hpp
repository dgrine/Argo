#ifndef HEADER_argo_core_handler_visitor_ApplyFunction_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_visitor_ApplyFunction_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <argo/handler/all.hpp>
#include <functional>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace visitor {

template <bool is_const>
class ApplyFunction : public IVisitor<is_const>
{
    template <typename Type>
    using type_of = visitor::type_of<is_const, Type>;

public:
    using function_type = std::function<bool(type_of<IHandler>)>;

    explicit ApplyFunction(const function_type &fnc)
        : fnc_(fnc)
    {
    }

    virtual bool visit(type_of<ARGO_NS(handler::Flag)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Interface)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Option)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Positional)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)> handler) override { return fnc_(handler); }
    virtual bool visit(type_of<ARGO_NS(handler::group::Interface)> handler) override { return fnc_(handler); }

private:
    function_type fnc_;
};

using ApplyConstFunction = ApplyFunction<true>;
using ApplyMutatingFunction = ApplyFunction<false>;

}}} // namespace core::handler::visitor

ARGO_NS_END

#endif
