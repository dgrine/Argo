#ifndef HEADER_argo_core_handler_visitor_IVisitor_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_visitor_IVisitor_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <type_traits>

ARGO_NS_BEGIN

namespace handler {

class Flag;
class Interface;
class Option;
class Positional;
class Toggle;

namespace group {

class Interface;
}

} // namespace handler

namespace core { namespace handler { namespace visitor {

template <bool is_const, typename Type>
using type_of = typename std::conditional<is_const, const Type &, Type &>::type;

template <bool is_const>
class IVisitor
{
    template <typename Type>
    using type_of = type_of<is_const, Type>;

public:
    virtual ~IVisitor() = default;

    using is_const_type = typename std::conditional<is_const, std::true_type, std::false_type>::type;

    virtual bool visit(type_of<ARGO_NS(handler::Flag)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Interface)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Option)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Positional)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::Toggle)>) = 0;
    virtual bool visit(type_of<ARGO_NS(handler::group::Interface)>) = 0;
};

using IConstVisitor = IVisitor<true>;
using IMutatingVisitor = IVisitor<false>;

}}} // namespace core::handler::visitor

ARGO_NS_END

#endif
