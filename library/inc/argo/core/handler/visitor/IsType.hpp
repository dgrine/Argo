#ifndef HEADER_argo_core_handler_visitor_IsType_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_visitor_IsType_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <argo/handler/all.hpp>

ARGO_NS_BEGIN

namespace core { namespace handler {

namespace visitor {

template <typename RefType>
class IsType : public IVisitor<true>
{
public:
    virtual bool visit(const ARGO_NS(handler::Flag) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Interface) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Option) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Positional) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::Toggle) & handler) override { return visit_(handler); }
    virtual bool visit(const ARGO_NS(handler::group::Interface) & handler) override { return visit_(handler); }

private:
    template <typename Type>
    bool visit_(const Type &handler) const
    {
        return std::is_same<Type, RefType>::value;
    }
};

} // namespace visitor

template <typename RefType, typename Type>
bool is_type(const Type &type)
{
    visitor::IsType<RefType> visitor;
    return type.accept(visitor);
}

}} // namespace core::handler

ARGO_NS_END

#endif
