#ifndef HEADER_argo_core_assert_hpp_ALREADY_INCLUDED
#define HEADER_argo_core_assert_hpp_ALREADY_INCLUDED

#include <argo/core/config/build.hpp>
#include <cassert>

#if ARGO_TOOLSET_DEBUG
#define ARGO_ASSERT_RELEASE(expr) const auto is_ok = expr; assert(!!is_ok)
#else
#define ARGO_ASSERT_RELEASE(expr) expr
#endif

#endif

