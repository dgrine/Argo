#ifndef HEADER_argo_core_convert_hpp_ALREADY_INCLUDED
#define HEADER_argo_core_convert_hpp_ALREADY_INCLUDED

#include <argo/core/config/build.hpp>
#include <string>
#include <cstdlib>

ARGO_NS_BEGIN

namespace core {

//!Return the number of characters that could be used during the conversion
inline std::size_t convert(double &dst, const std::string &src)
{
    const char *c_str = src.c_str();
    char *end = nullptr;
    dst = std::strtod(c_str, &end);
    return end-c_str;
}
//!Return the number of characters that could be used during the conversion
inline std::size_t convert(float &dst, const std::string &src)
{
    const char *c_str = src.c_str();
    char *end = nullptr;
    dst = std::strtof(c_str, &end);
    return end-c_str;
}

}

ARGO_NS_END

#endif

