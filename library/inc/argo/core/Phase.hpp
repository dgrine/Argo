#ifndef HEADER_argo_core_Phase_hpp_INCLUDE_GUARD
#define HEADER_argo_core_Phase_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <ostream>

ARGO_NS_BEGIN

namespace core {

enum class Phase
{
    OptionsFirstPass,
    Positionals,
    OptionsLastPass,
    Nr_
};

inline std::ostream &operator<<(std::ostream &os, const Phase phase)
{
    switch (phase)
    {
    case Phase::OptionsFirstPass: os << "OptionsFirstPass"; break;
    case Phase::Positionals: os << "Positionals"; break;
    case Phase::OptionsLastPass: os << "OptionsLastPass"; break;
    default: assert(false); os << "?";
    }
    return os;
}

} // namespace core

ARGO_NS_END

#endif

