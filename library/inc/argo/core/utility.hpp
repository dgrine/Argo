#ifndef HEADER_argo_core_utility_hpp_INCLUDE_GUARD
#define HEADER_argo_core_utility_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/config/toolset.hpp>
#include <argo/core/convert.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/string/utility.hpp>
#include <algorithm>
#include <cassert>
#include <exception>
#include <string>
#include <vector>

ARGO_NS_BEGIN

namespace core {

inline std::string basename_of(const std::string &path)
{
    return std::string(std::find_if(path.rbegin(), path.rend(), [](const char ch) {
#if ARGO_TOOLSET_PLATFORM_WINDOWS
                           return '/' == ch || '\\' == ch;
#else
                           return '/' == ch;
#endif
                       })
                           .base(),
                       path.end());
}
inline bool is_number(const std::string &value)
{
    float dummy;
    const auto chars_used = convert(dummy, value);
    return chars_used == value.size();
}

namespace name {

inline bool is_shorthand(const std::string &name)
{
    MSS_BEGIN(bool);
    MSS(!name.empty());
    MSS(!is_number(name));
    MSS_Q(string::starts_with(name, "-"));
    MSS_END();
}
inline bool is_longhand(const std::string &name)
{
    MSS_BEGIN(bool);
    MSS(!name.empty());
    MSS_Q(2 < name.size());
    MSS_Q(string::starts_with(name, "--"));
    MSS_END();
}
inline bool is_option(const std::string &name)
{
    MSS_BEGIN(bool);
    MSS_Q(is_shorthand(name) || is_longhand(name));
    MSS_END();
}
inline bool is_positional(const std::string &name)
{
    MSS_BEGIN(bool);
    MSS(!name.empty());
    MSS(!is_shorthand(name));
    MSS(!is_longhand(name));
    MSS(!is_number(name));
    MSS_END();
}
inline std::string guess_shorthand(const std::string &longhand)
{
    assert(is_longhand(longhand));
    return std::string("-") + longhand[2];
}
} // namespace name

inline std::vector<std::string> normalize_arguments(const std::vector<std::string> &arguments)
{
    S(false);
    std::vector<std::string> args;
    args.reserve(arguments.size());
    for (const auto &argument : arguments)
    {
        L(C(argument));
        const auto &kv = string::split(argument, "=");
        if (1 < kv.size())
        {
            const auto &name = kv[0];
            if (name::is_option(name))
            {
                L(C(name));
                args.push_back(name);
                const std::string value(std::begin(argument) + name.size() + 1, std::end(argument));
                if (string::is_delimited(value, "'") || string::is_delimited(value, "\""))
                {
                    L(C(value));
                    args.push_back(value);
                    continue;
                }
                else
                {
                    const auto &values = string::split(value, ",");
                    for (const auto &value : values)
                    {
                        L(C(value));
                        args.push_back(value);
                    }
                    continue;
                }
            }
        }
        args.push_back(argument);
    }
    return args;
}

} // namespace core

ARGO_NS_END

#endif

