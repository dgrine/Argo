#ifndef HEADER_argo_core_log_hpp_INCLUDE_GUARD
#define HEADER_argo_core_log_hpp_INCLUDE_GUARD

#include <iostream>

#define ARGO_DEBUG false
#define S(state) static bool log_enabled_ = state
#define L(expr) if (log_enabled_) { std::cout << "[TRACE] " << expr << std::endl; }
#define C(expr) "(" << #expr << ":" << (expr) << ")"

#endif
