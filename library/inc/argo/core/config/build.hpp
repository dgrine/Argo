#ifndef HEADER_argo_core_config_build_hpp_INCLUDE_GUARD
#define HEADER_argo_core_config_build_hpp_INCLUDE_GUARD

#define ARGO_NS_BEGIN namespace argo {
#define ARGO_NS_END }
#define ARGO_NS(x) argo::x
#ifdef ARGO_BUILD_STATIC_LIBRARY
#define ARGO_INLINE 
#else
#define ARGO_INLINE inline
#endif

#endif
