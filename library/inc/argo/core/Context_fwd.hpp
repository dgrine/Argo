#ifndef HEADER_argo_core_Context_fwd_hpp_INCLUDE_GUARD
#define HEADER_argo_core_Context_fwd_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>

ARGO_NS_BEGIN

namespace core {

class Context;

}

ARGO_NS_END

#endif

