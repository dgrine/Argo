#ifndef HEADER_argo_core_string_utility_hpp_INCLUDE_GUARD
#define HEADER_argo_core_string_utility_hpp_INCLUDE_GUARD

#include <argo/core/NotOnFirstTime.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/log.hpp>
#include <argo/core/optional.hpp>
#include <argo/core/range.hpp>
#include <algorithm>
#include <cassert>
#include <cctype>
#include <initializer_list>
#include <sstream>
#include <string>
#include <vector>

ARGO_NS_BEGIN

namespace core { namespace string {

template <typename T>
struct from
{
    static std::string run(const T &t) { return std::to_string(t); }
};
template <>
struct from<std::string>
{
    static std::string run(const std::string &t) { return t; }
};
template <>
struct from<const char *>
{
    static std::string run(const char *cstr) { return cstr; }
};

template <typename It, typename value_type = typename std::iterator_traits<It>::value_type>
std::string join(const std::string &separator, It begin, It end)
{
    if (begin == end) return {};

    auto it = begin;
    std::string result = from<value_type>::run(*it++);
    for (; it != end; ++it) result += separator + from<value_type>::run(*it);
    return result;
}
template <typename T>
std::string join(const std::string &separator, const std::initializer_list<T> &list)
{
    return join(RANGE(list), separator);
}
inline std::vector<std::string> split(std::string str, std::string delimiter = " ")
{
    std::size_t pos = 0;
    std::vector<std::string> result;
    while ((pos = str.find(delimiter)) != std::string::npos)
    {
        const auto token = str.substr(0, pos);
        result.push_back(std::move(token));
        str.erase(0, pos + delimiter.length());
    }
    if (!str.empty()) result.push_back(str);
    return result;
}
inline void replace_all(std::string &str, const std::string &from, const std::string &to)
{
    std::size_t start_pos = 0u;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.size(), to);
        start_pos += to.size();
    }
}
inline bool starts_with(const std::string &input, const std::string &test) { return test.size() <= input.size() && input.compare(0, test.size(), test) == 0; }
inline bool ends_with(const std::string &input, const std::string &test) { return input.size() >= test.size() && input.compare(input.size() - test.size(), test.size(), test) == 0; }
inline void ltrim(std::string &input)
{
    input.erase(input.begin(), std::find_if(RANGE(input), [](int ch) { return !std::isspace(ch); }));
}
inline void rtrim(std::string &input)
{
    input.erase(std::find_if(input.rbegin(), input.rend(), [](int ch) { return !std::isspace(ch); }).base(), input.end());
}
inline void trim(std::string &input)
{
    ltrim(input);
    rtrim(input);
}
inline std::string surround(const std::string &input, const std::string &left, core::optional<std::string> right = core::nullopt)
{
    if (!right)
    {
        if ("(" == left)
            right = ")";
        else if ("[" == left)
            right = "]";
        else if ("{" == left)
            right = "}";
        else if ("<" == left)
            right = ">";
        else
            right = left;
    }
    assert(!!right);
    return left + input + *right;
}
template <typename Out>
void split(const std::string &input, const char delimiter, Out out)
{
    std::istringstream ss{ input };
    std::string part;
    while (std::getline(ss, part, delimiter)) *(out++) = part;
}
inline std::vector<std::string> split(const std::string &input, const char delimiter)
{
    std::vector<std::string> result;
    split(input, delimiter, std::back_inserter(result));
    return result;
}
inline std::string tolower(const std::string &input)
{
    std::string result;
    result.reserve(input.size());
    for (const auto ch : input) result.push_back(std::tolower(ch));
    return result;
}
inline void tolower(std::string::iterator first, std::string::iterator last)
{
    for (auto current = first; current != last; ++current) *current = std::tolower(*current);
}
inline std::string toupper(const std::string &input)
{
    std::string result;
    result.reserve(input.size());
    for (const auto ch : input) result.push_back(std::toupper(ch));
    return result;
}
inline void toupper(std::string::iterator first, std::string::iterator last)
{
    for (auto current = first; current != last; ++current) *current = std::toupper(*current);
}
inline bool is_delimited(const std::string &input, const std::string &delimiter) { return starts_with(input, delimiter) && ends_with(input, delimiter); }
inline std::string align(const std::string &input, const unsigned page_size, const unsigned indentation, bool ascii_color_code = true)
{
    S(false);
    assert(page_size > indentation);
    std::string result;
    const std::string prefix(indentation, ' ');
    struct
    {
        unsigned line_idx = 0;
        std::string::const_iterator previous;
        std::string::const_iterator breaker;

        void reset(std::string::const_iterator current, std::string::const_iterator end)
        {
            line_idx = 0;
            previous = current;
            breaker = end;
        }
    } state;
    L(C(input.size()) C(page_size) C(indentation));
    const std::string::const_iterator begin = std::begin(input);
    const std::string::const_iterator end = std::end(input);
    std::string::const_iterator current = begin;
    state.reset(current, end);
    auto skip_whitespace = [&current, &end]() {
        while (current != end && std::isspace(*current)) ++current;
    };
    auto skip_colorcodes = [&current, &end]() {
        S(false);
        auto it = current;
        std::string look_ahead;
        while (it != end && look_ahead.size() < 2)
        {
            look_ahead.push_back(*it);
            ++it;
        }
        if ("\033[" == look_ahead)
        {
            L("Skipping color code");
            while (current != end && *current != 'm')
            {
                L(" > skip");
                ++current;
            }
            if (current != end) ++current;
        }
    };
    auto complete = [&]() {
        std::copy(state.previous, current, std::back_inserter(result));
    };
    while (current != end)
    {
        skip_colorcodes();
        if (current == end) break;
        const char ch = *current;
        L(C(ch) C(state.line_idx));
        if (state.line_idx == 0)
        {
            L("New line");
            result += prefix;
            L(C(result));
            state.line_idx += prefix.size() + 1;
        }
        else if (state.line_idx > page_size)
        {
            L("Line wrap");
            if (end != state.breaker) current = state.breaker;
            complete();
            result += "\n";
            skip_whitespace();
            state.reset(current, end);
        }
        else if (*current == '\n')
        {
            L("Line break");
            complete();
            result += "\n";
            ++current;
            state.reset(current, end);
        }
        else
        {
            if (std::isspace(ch))
            {
                L("Breaker");
                state.breaker = current;
            }
            ++state.line_idx;
            ++current;
        }
    }
    L("Left over");
    std::copy(state.previous, end, std::back_inserter(result));
    L(C(result));
    return result;
}

template<typename Type>
std::string quote(Type value)
{
    std::ostringstream os;
    os << value;
    return surround(os.str(), "'");
}

template<typename It>
std::string quote(It first, It last, const std::string &last_word)
{
    const auto nr_values = std::distance(first, last);
    if (0 == nr_values) return {};
    else if (1 == nr_values) return quote(*first);
    std::ostringstream os;
    auto current = first;
    NotOnFirstTime noft{};
    for (auto i = 0u; i < nr_values - 1; ++i, ++current)
    {
        noft([&os](){ os << ", "; });
        os << quote(*current);
    }
    os << surround(last_word, " ") << quote(*current);
    return os.str();
}

template<typename It>
std::string quote_or(It first, It last) { return quote(first, last, "or"); }

template<typename It>
std::string quote_and(It first, It last) { return quote(first, last, "and"); }

}} // namespace core::string

ARGO_NS_END

#endif

