#ifndef HEADER_argo_core_range_hpp_INCLUDE_GUARD
#define HEADER_argo_core_range_hpp_INCLUDE_GUARD

#define RANGE(container) (std::begin(container)), (std::end(container))

#endif
