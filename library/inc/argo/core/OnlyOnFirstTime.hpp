#ifndef HEADER_argo_core_OnlyOnFirstTime_hpp_INCLUDE_GUARD
#define HEADER_argo_core_OnlyOnFirstTime_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>

ARGO_NS_BEGIN

namespace core {

class OnlyOnFirstTime
{
public:
    template <typename Functor>
    void operator()(Functor functor) const
    {
        if (first_time_)
        {
            functor();
            first_time_ = false;
        }
    }

private:
    mutable bool first_time_ = true;
};

} // namespace core

ARGO_NS_END

#endif

