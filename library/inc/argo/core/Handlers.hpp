#ifndef HEADER_argo_core_Handlers_hpp_INCLUDE_GUARD
#define HEADER_argo_core_Handlers_hpp_INCLUDE_GUARD

#include <argo/core/assert.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <argo/core/log.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/range.hpp>
#include <argo/core/transform_iterator.hpp>
#include <map>
#include <string>
#include <vector>

ARGO_NS_BEGIN

namespace handler {

class Positional;
}

namespace core {

class Handlers
{
    using data_type = std::vector<handler::Ptr>;

public:
    using value_type = handler::IHandler &;
    using reference = value_type;
    using const_iterator = transform_iterator<reference, typename data_type::const_iterator>;
    using iterator = transform_iterator<reference, typename data_type::iterator>;

    Handlers() = default;
    Handlers(const handler::IHandler &parent)
        : pparent_(&parent)
    {
    }
    Handlers(const Handlers &other)
        : pparent_(other.pparent_)
    {
        for (const auto &handler : other)
        {
            ARGO_ASSERT_RELEASE(add(handler));
        }
    }
    Handlers &operator=(const Handlers &other)
    {
        if (this != &other)
        {
            handlers_.clear();
            hashes_.clear();
            pparent_ = nullptr;
            for (const auto &handler : other)
            {
                ARGO_ASSERT_RELEASE(add(handler));
            }
        }
        return *this;
    }

    const_iterator begin() const { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::begin(handlers_)); }
    iterator begin() { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::begin(handlers_)); }
    const_iterator end() const { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::end(handlers_)); }
    iterator end() { return make_transform_iterator<reference>(transformation::dereference<reference>{}, std::end(handlers_)); }
    bool has(const handler::IHandler &handler) { return hashes_.count(handler.name()); }
    bool add(const handler::IHandler &handler)
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        MSS(handler.is_valid());
        const auto hash = handler.name();
        L("Adding handler with hash " << C(hash));
        MSS(!hashes_.count(hash));
        hashes_[hash] = handlers_.size();
        handlers_.push_back(handler.clone());
        if (!!pparent_) back().set_parent(*pparent_);
        MSS_END();
    }
    bool remove(const handler::IHandler &handler)
    {
        MSS_BEGIN(bool);
        MSS(has(handler));
        const auto hash = handler.name();
        const auto idx = hashes_[hash];
        assert(idx < handlers_.size());
        assert(handlers_[idx]->name() == hash);
        hashes_.erase(hash);
        const auto it = std::begin(handlers_) + idx;
        handlers_.erase(it);
        reindex_(idx);
        MSS_END();
    }
    bool empty() const { return handlers_.empty(); }
    unsigned size() const { return handlers_.size(); }
    const handler::IHandler &back() const
    {
        assert(!handlers_.empty());
        const auto &ptr = handlers_.back();
        assert(!!ptr);
        return *ptr;
    }

    bool accept_on_elements(handler::visitor::IConstVisitor &visitor) const
    {
        MSS_BEGIN(bool);
        for (const auto &phandler : handlers_)
        {
            assert(!!phandler);
            MSS(phandler->accept(visitor));
        }
        MSS_END();
    }
    bool accept_on_elements(handler::visitor::IMutatingVisitor &visitor) const
    {
        MSS_BEGIN(bool);
        for (const auto &phandler : handlers_)
        {
            assert(!!phandler);
            MSS(phandler->accept(visitor));
        }
        MSS_END();
    }
    void set_parent(const handler::IHandler &handler)
    {
        pparent_ = &handler;
        for (const auto &hdlr: *this) hdlr.set_parent(handler);
    }

private:
    void reindex_(unsigned idx)
    {
        for (auto &item : hashes_)
            if (item.second > idx) --item.second;
    }

    const handler::IHandler *pparent_ = nullptr;
    data_type handlers_;
    std::map<std::string, unsigned> hashes_;
};

} // namespace core

ARGO_NS_END

#endif
