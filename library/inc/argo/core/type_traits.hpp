#ifndef HEADER_argo_core_type_traits_hpp_INCLUDE_GUARD
#define HEADER_argo_core_type_traits_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <list>
#include <set>
#include <vector>

ARGO_NS_BEGIN

namespace core {

template <typename Variable>
bool is_collection(const Variable &variable)
{
    return false;
}
template <typename Type>
bool is_collection(const std::vector<Type> &variable)
{
    return true;
}
template <typename Type>
bool is_collection(const std::list<Type> &variable)
{
    return true;
}
template <typename Type>
bool is_collection(const std::set<Type> &variable)
{
    return true;
}

} // namespace core

ARGO_NS_END

#endif

