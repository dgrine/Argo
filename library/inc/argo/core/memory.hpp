#ifndef HEADER_argo_core_memory_hpp_INCLUDE_GUARD
#define HEADER_argo_core_memory_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <memory>

ARGO_NS_BEGIN

namespace core {

template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args &&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

} // namespace core

ARGO_NS_END

#endif
