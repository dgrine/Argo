#ifndef HEADER_argo_handler_ResponseFile_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_ResponseFile_hpp_INCLUDE_GUARD

#include <argo/ResponseFile.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/string/utility.hpp>
#include <argo/handler/Interface.hpp>
#include <cassert>
#include <sstream>
#include <string>

ARGO_NS_BEGIN

namespace handler {

//!A handler that processes response files.
class ResponseFile : public Interface
{
public:
    //!Constructs the response file handler with the given identifier. By default, the commonly used `@` character is chosen. As such, invocations such as `./app @/path/to/response/file.rsp` are processed by this handler. Note that the path to the response file must immediately follow the identifier string. In the case of the `@character` there is therefore no white space between the `@` the response file.
    explicit ResponseFile(const std::string &identifier = "@")
        : identifier_(identifier)
    {
    }

    virtual bool is_valid() const override { return true; }
    virtual std::string name() const override { return identifier_; }
    virtual std::string type() const override { return "Response file handler"; }
    virtual std::string help() const override
    {
        std::ostringstream os;
        os << "As an alternative to placing all the options on the command line, response files are also supported. Response files are plain text files that list command line arguments. This may be useful as a convenience or necessary due to limitations of your environment. Arguments in a response file are interpreted as if they were present at that place in the invocation. Each argument in a response file must begin and end on the same line. You cannot use the backslash character to concatenate lines. Comments are supported by prefixing lines with the // characters.";
        return os.str();
    }

private:
    virtual core::handler::Ptr clone() const override { return core::make_unique<ResponseFile>(*this); }
    virtual bool is_satisfied(core::Context &context) const override { return true; }
    virtual bool parse(core::Context &context) override
    {
        MSS_BEGIN(bool);
        assert(!context.args().reached_end());
        const auto arg = *context.args().current();
        MSS(core::string::starts_with(arg, identifier_));
        const std::string fn(std::begin(arg) + identifier_.size(), std::end(arg));
        ARGO_NS(ResponseFile)
        rsp;
        MSS(rsp.parse(fn),
            {
                context.error() << "Could not parse file " << core::string::surround(fn, "'");
            });
        context.args().replace(rsp.argv());
        MSS_END();
    }
    virtual bool recognizes_(core::Context &context) const override
    {
        assert(!context.args().reached_end());
        const auto arg = *context.args().current();
        return core::string::starts_with(arg, identifier_);
    }

    std::string identifier_;
};

} // namespace handler

ARGO_NS_END

#endif

