#ifndef HEADER_argo_handler_group_Exclusive_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_group_Exclusive_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/optional.hpp>
#include <argo/handler/all.hpp>
#include <string>

ARGO_NS_BEGIN

namespace handler { namespace group {

class Exclusive;

namespace details { namespace exclusive {

class Validator : public core::handler::visitor::IVisitor<true>
{
public:
    explicit Validator(core::Context &context, const core::handler::IHandler &group)
        : context_(context)
        , group_(group)
    {
    }

    virtual bool visit(const handler::Flag &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Interface &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Option &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Positional &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Toggle &handler) override { return visit_(handler); }
    virtual bool visit(const handler::group::Interface &group) override
    {
        MSS_BEGIN(bool);
        MSS(visit_(group));
        const auto &info = context_.info(group);
        if (!!info.nr_parses) MSS(group.is_satisfied(context_));
        MSS_END();
    }

    unsigned nr_parsed() const { return nr_parsed_; }

private:
    template <typename Handler>
    bool visit_(const Handler &handler)
    {
        MSS_BEGIN(bool);
        S(ARGO_DEBUG);
        const auto &info = context_.info(handler);
        nr_parsed_ += static_cast<unsigned>(!!info.nr_parses);
        L(C(nr_parsed_));
        L("Validating " << C(handler));
        MSS(nr_parsed_ < 2,
            {
                assert(!!pprev_handler_);
                context_.error() << core::handler::name_of(handler, true) << " belongs to " << core::handler::name_of(group_) << " and cannot be used in combination with options from the same group";
            });
        pprev_handler_ = &handler;
        MSS_END();
    }
    core::Context &context_;
    const core::handler::IHandler &group_;
    std::string group_name_;
    unsigned nr_parsed_ = 0;
    const core::handler::IHandler *pprev_handler_ = nullptr;
};

}} // namespace details::exclusive

//!Group in which the presence of any one option/group within it, forces the exclusion of the others.
class Exclusive : public Interface
{
public:
    explicit Exclusive(const std::string &name)
        : Interface(name, "Exclusive group")
    {
    }

    virtual core::handler::Ptr clone() const override { return core::make_unique<Exclusive>(*this); }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        S(ARGO_DEBUG);
        L("Checking if " << core::handler::name_of(*this) << " is satisfied");
        details::exclusive::Validator validator{ context, *this };
        MSS(handlers_().accept_on_elements(validator));
        MSS(is_optional() || validator.nr_parsed() == 1, context.error() << "Missing argument for " << core::handler::name_of(*this););
        MSS_END();
    }
};

}} // namespace handler::group

ARGO_NS_END

#endif

