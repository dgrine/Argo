#ifndef HEADER_argo_handler_group_Interface_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_group_Interface_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/Handlers.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/config/toolset.hpp>
#include <argo/core/handler/property/Help.hpp>
#include <argo/core/handler/property/Required.hpp>
#include <argo/core/handler/visitor/IsType.hpp>
#include <sstream>

ARGO_NS_BEGIN

namespace handler { namespace group {

class Interface : public core::handler::IHandler,
                  public core::handler::property::Help<Interface>,
                  public core::handler::property::Required<Interface>
{
public:
    using value_type = const core::handler::IHandler &;
    using reference_type = value_type;
    using iterator = core::Handlers::const_iterator;
    using const_iterator = iterator;

    //!Constructs the group with the given name and type description.
    explicit Interface(const std::string &name, const std::string &type)
        : name_(name)
        , type_(type)
        , handlers__(*this)
    {
    }
    //!Copy constructs the group.
    Interface(const Interface &other)
        : core::handler::IHandler(other)
        , core::handler::property::Help<Interface>(other)
        , core::handler::property::Required<Interface>(other)
        , name_(other.name_)
        , type_(other.type_)
        , handlers__(other.handlers__)
    {
        handlers__.set_parent(*this);
    }
    //!Copy assigns the group.
    Interface &operator=(const Interface &other)
    {
        if (this != &other)
        {
            core::handler::IHandler::operator=(other);
            core::handler::property::Help<Interface>::operator=(other);
            core::handler::property::Required<Interface>::operator=(other);
            name_ = other.name_;
            type_ = other.type_;
            handlers__ = other.handlers__;
            handlers__.set_parent(*this);
        }
        return *this;
    }

    virtual bool is_valid() const override { return true; }
    virtual std::string name() const override { return name_; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    bool accept_on_elements(core::handler::visitor::IConstVisitor &visitor) const
    {
        MSS_BEGIN(bool);
        for (const auto &element : *this) MSS(element.accept(visitor));
        MSS_END();
    }
    bool accept_on_elements(core::handler::visitor::IMutatingVisitor &visitor)
    {
        MSS_BEGIN(bool);
        for (auto &element : *this) MSS(element.accept(visitor));
        MSS_END();
    }
    virtual std::string type() const override { return type_; }
    bool add(const core::handler::IHandler &handler) { return handlers__.add(handler); }
    bool empty() const { return handlers__.empty(); }
    std::size_t size() const { return handlers__.size(); }

    const_iterator begin() const { return std::begin(handlers__); }
    const_iterator end() const { return std::end(handlers__); }

protected:
    const core::Handlers &handlers_() const { return handlers__; }

private:
    virtual bool parse(core::Context &context) override
    {
        assert(false);
        return false;
    }

    std::string name_;
    std::string type_;
    core::Handlers handlers__;
};

}} // namespace handler::group

ARGO_NS_END

#endif

