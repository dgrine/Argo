#ifndef HEADER_argo_handler_group_Inclusive_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_group_Inclusive_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/config/toolset.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/optional.hpp>
#include <argo/handler/all.hpp>
#include <string>

ARGO_NS_BEGIN

namespace handler { namespace group {

namespace details { namespace inclusive {

class Validator : public core::handler::visitor::IVisitor<true>
{
public:
    explicit Validator(core::Context &context, const std::string &group_name)
        : context_(context)
        , group_name_(group_name)
    {
    }

    virtual bool visit(const handler::Flag &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Interface &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Option &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Positional &handler) override { return visit_(handler); }
    virtual bool visit(const handler::Toggle &handler) override { return visit_(handler); }
    virtual bool visit(const handler::group::Interface &group) override
    {
        MSS_BEGIN(bool);
        MSS(visit_(group));
        const auto &info = context_.info(group);
        if (!!info.nr_parses) MSS(group.is_satisfied(context_));
        MSS_END();
    }
    bool parsed() const { return !!must_have_parsed_ && *must_have_parsed_; }

private:
    template <typename Handler>
    bool visit_(const Handler &handler)
    {
        MSS_BEGIN(bool);
        S(ARGO_DEBUG);
        const auto &info = context_.info(handler);
        L("Validating " << C(handler));
        if (!must_have_parsed_)
        {
            must_have_parsed_ = info.nr_parses;
            L("Looking for options that must have been parsed: " << C(*must_have_parsed_));
        }
        else
        {
            L(C(info.nr_parses));
            L(C(is_optional_(handler)));
            MSS(is_optional_(handler) || (*must_have_parsed_ == static_cast<bool>(info.nr_parses)),
                {
                    auto error = context_.error();
                    if (*must_have_parsed_)
                    {
                        error << "Missing argument '" << handler << "' for inclusive group '" << group_name_ << "'";
                    }
                    else
                    {
                        error << "Argument '" << handler << "' belongs to inclusive group '" << group_name_ << "', which expects other arguments";
                    }
                });
        }
        MSS_END();
    }
    template <typename Interface>
    bool is_optional_(const Interface &handler)
    {
        const auto ptr = dynamic_cast<const core::handler::property::Required<Interface> *>(&handler);
        return (!!ptr) && ptr->has_user_defined_required_or_optional() && ptr->is_optional();
    }

    core::Context &context_;
    std::string group_name_;
    core::optional<bool> must_have_parsed_;
};

}} // namespace details::inclusive

//!Group in which the presence of any one option/group within it, forces the presence of the others.
class Inclusive : public Interface
{
public:
    explicit Inclusive(const std::string &name)
        : Interface(name, "Inclusive group")
    {
    }

    virtual core::handler::Ptr clone() const override { return core::make_unique<Inclusive>(*this); }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        S(ARGO_DEBUG);
        L("Checking if " << core::handler::name_of(*this) << " is satisfied");
        const auto &info = context.info(*this);
        L(C(info.nr_parses));
        if (!!info.nr_parses)
        {
            details::inclusive::Validator validator{ context, name() };
            MSS(handlers_().accept_on_elements(validator));
        }
        else
        {
            MSS(is_optional(), context.error() << "Missing arguments for required " << core::handler::name_of(*this););
        }
        MSS_END();
    }
};

}} // namespace handler::group

ARGO_NS_END

#endif

