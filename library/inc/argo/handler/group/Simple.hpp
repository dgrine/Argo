#ifndef HEADER_argo_handler_group_Simple_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_group_Simple_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/handler/group/Interface.hpp>

ARGO_NS_BEGIN

namespace handler { namespace group {

//!Group that holds a simple collection of handlers.
class Simple : public Interface
{
public:
    explicit Simple(const std::string &name)
        : Interface(name, "Group")
    {
    }

private:
    virtual core::handler::Ptr clone() const override { return core::make_unique<Simple>(*this); }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        bool parsed = false;
        for (const auto &handler : *this)
        {
            MSS(handler.is_satisfied(context));
            if (!parsed) parsed = !!context.info(handler).nr_parses;
        }
        MSS(is_optional() || parsed, context.error() << "At least one argument is required for " << core::handler::name_of(*this););
        MSS_END();
    }
};

}} // namespace handler::group

ARGO_NS_END

#endif

