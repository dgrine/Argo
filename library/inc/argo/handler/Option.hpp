#ifndef HEADER_argo_handler_Option_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_Option_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/property/Actionable.hpp>
#include <argo/core/handler/property/Cardinality.hpp>
#include <argo/core/handler/property/Help.hpp>
#include <argo/core/handler/property/Named.hpp>
#include <argo/core/handler/property/Required.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/log.hpp>
#include <argo/core/utility.hpp>
#include <cassert>
#include <sstream>

ARGO_NS_BEGIN

namespace handler {

//!Handler that processes arguments with values.
class Option : public core::handler::IHandler,
               public core::handler::property::Named,
               public core::handler::property::Help<Option>,
               public core::handler::property::Required<Option>,
               public core::handler::property::Cardinality<Option>,
               public core::handler::property::Actionable<Option>
{
public:
    //!Constructs an option with a longhand and shorthand. They may be provided in any order. This constructor is useful if no direct variable binding can be done. Using this constructor implies adding a custom action later in order to create a valid Option.
    explicit Option(const std::string &first, const std::string &second)
        : core::handler::property::Named(first, second)
    {
    }
    //!Constructs an option with a longhand or shorthand. This constructor is useful if no direct variable binding can be done. Using this constructor implies adding a custom action later in order to create a valid Option.
    explicit Option(const std::string &name)
        : core::handler::property::Named(name)
    {
    }
    //!Constructs an option with a longhand and shorthand. They may be provided in any order. This constructor is useful if no direct variable binding can be done. Using this constructor implies adding a custom action later in order to create a valid Option.
    explicit Option(const char *first, const char *second)
        : Option(std::string(first), std::string(second))
    {
    }
    //!Constructs an option with a longhand or shorthand.
    explicit Option(const char *name)
        : Option(std::string(name))
    {
    }
    //!Constructs an option with a longhand and shorthand. They may be provided in any order. Any values will be stored in the given variable.
    template <typename Variable, typename = typename std::enable_if<!std::is_const<Variable>::value>::type>
    explicit Option(const std::string &first, const std::string &second, Variable &variable)
        : Option(first, second)
    {
        add_store_action_(variable);
    }
    //!Constructs an option with a longhand or shorthand. Any values will be stored in the given variable.
    template <typename Variable, typename = typename std::enable_if<!std::is_const<Variable>::value>::type>
    explicit Option(const std::string &name, Variable &variable)
        : Option(name)
    {
        add_store_action_(variable);
    }

    virtual bool is_valid() const override
    {
        MSS_BEGIN(bool);
        MSS(!actions_().empty());
        MSS(core::handler::property::Named::is_valid_());
        MSS(core::handler::property::Cardinality<Option>::is_valid_());
        MSS_END();
    }
    virtual std::string name() const override { return core::handler::property::Named::name_(); }
    virtual std::string type() const override { return "Option"; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Option>(*this); }
    virtual bool parse(core::Context &context) override
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Option parsing...");

        //First process the name
        MSS(!context.args().reached_end());
        L("Consuming name '" << *context.args().current() << "'");
        MSS(context.args().next());

        if (context.config().parser.implicit_values)
        {
            //Next, process any values step by step
            L("Processing implicit value(s)");
            while (!context.args().reached_end())
            {
                bool stop_processing = false;
                MSS(process_value_(context, stop_processing));
                if (stop_processing) MSS_RETURN_OK();
            }
        }
        else
        {
            // One value
            L("Processing explicit value");
            bool stop_processing = false;
            if (!context.args().reached_end()) MSS(process_value_(context, stop_processing));
            if (stop_processing) MSS_RETURN_OK();
        }

        L("Finished option parsing");
        MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Required<Option>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required");
        MSS(is_optional() || core::handler::property::Cardinality<Option>::is_satisfied_(), context.error() << core::handler::name_of(*this, true) << " expects " << nargs_().description());
        MSS_END();
    }

private:
    virtual void reset_() override { nargs_().reset(); }
    bool process_value_(core::Context &context, bool &stop_processing)
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        const auto value = *context.args().current();
        L("Considering consuming " << C(value));
        if (!context.config().parser.empty_values && value.empty())
        {
            L("Argument is empty, not configured to accept this");
            context.error() << core::handler::name_of(*this, true) << " expects non-empty arguments";
            stop_processing = true;
            MSS_RETURN_ERROR(); // We explicitly reject this
        }
        if (context.parser_could_continue(context))
        {
            L("Yielding back, the toplevel parser definitely knows what to do");
            stop_processing = true;
            MSS_RETURN_OK(); // Let someone else handle this
        }
        else if (!context.config().parser.argument_values && core::name::is_option(value))
        {
            L("Argument like value, not configured to accept this");
            stop_processing = true;
            MSS_RETURN_OK(); // Let someone else handle this
        }

        //Check if we're allowed another value
        if (!nargs_().may_have_next())
        {
            const auto last_arg = std::distance(context.args().current(), context.args().end()) == 1;
            switch (context.phase())
            {
                case core::Phase::OptionsFirstPass:
                    {
                        if (core::name::is_option(value))
                        {
                            L("The value is an option which is unknown so let the toplevel parser handle the failure");
                            stop_processing = true;
                            MSS_RETURN_OK();
                        }
                        else if (last_arg)
                        {
                            L("This is the last value, let's see if the positional phase will allow the toplevel parser to continue");
                            context.next_phase();
                            if (!context.parser_could_continue(context))
                            {
                                L("We can't have another value: the positional phase did not resolve anything");
                                context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(*this) << ": expected " << nargs_().description();
                                MSS_RETURN_ERROR();
                            }
                            L("Yielding back, the toplevel parser knows how to handle this value as a positional");
                            stop_processing = true;
                            MSS_RETURN_OK();
                        }
                    }
                    break;
                case core::Phase::Positionals: break;
                case core::Phase::OptionsLastPass:
                                               {
                                                   L("We can't have another value because we have already had the positionals phase");
                                                   context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(*this) << ": expected " << nargs_().description();
                                                   MSS_RETURN_ERROR();
                                               }
                                               break;
                case core::Phase::Nr_: break;
            }
            L("Let the toplevel parser handle the failure");
            stop_processing = true;
            MSS_RETURN_OK();
        }

        //Running all actions
        assert(!actions_().empty()); //If this assertion triggers, it most likely means you have used a wrong constructor for Option.
        //The most common case is when you have dynamically created the option names and passed them as non-const references.
        //For example: Option{name_one, name_two} where name_one, name_two are non-const references to std::string.
        //In this case, the compiler might choose the templated constructor: Option{const std::string &, Variable &}
        //To work around this, use the correct types. If this is a big nuisance, consider filing a bug to request a change in the API.
        L("Running actions: " << C(actions_().size()));
        MSS(core::handler::property::Actionable<Option>::run_(context));
        L("All actions were run");

        //Consume value
        L("Consuming " << C(value));
        MSS(context.args().next());

        //Update properties
        nargs_().next();
        MSS_END();
    }
};

} // namespace handler

ARGO_NS_END

#endif

