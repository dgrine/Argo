#ifndef HEADER_argo_handler_Interface_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_Interface_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/property/Help.hpp>
#include <argo/core/handler/visitor/IVisitor.hpp>
#include <cassert>
#include <sstream>

ARGO_NS_BEGIN

namespace core { namespace handler { namespace search {

class Custom;

} } }

namespace handler {

//!A handler that can serve as a base class for custom handlers.
class Interface : public core::handler::IHandler,
                  public core::handler::property::Help<Interface>
{
public:
    //!Returns the help description.
    virtual std::string help() const = 0;

protected:
    friend class core::handler::search::Custom;

    virtual bool recognizes_(core::Context &context) const = 0;

    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) { return visitor.visit(*this); }
};

} // namespace handler

ARGO_NS_END

#endif

