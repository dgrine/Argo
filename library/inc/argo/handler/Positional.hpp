#ifndef HEADER_argo_handler_Positional_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_Positional_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/property/Actionable.hpp>
#include <argo/core/handler/property/Cardinality.hpp>
#include <argo/core/handler/property/Help.hpp>
#include <argo/core/handler/property/Required.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/log.hpp>
#include <argo/core/traits/conversion.hpp>
#include <argo/core/utility.hpp>
#include <cassert>
#include <sstream>

ARGO_NS_BEGIN

namespace handler {

//!A handler that processes positional arguments: unnamed arguments that signal values.
class Positional : public core::handler::IHandler,
                   public core::handler::property::Help<Positional>,
                   public core::handler::property::Required<Positional>,
                   public core::handler::property::Cardinality<Positional>,
                   public core::handler::property::Actionable<Positional>
{
public:
    //!Constructs the positional handler with the given name. Note that _the name may not be a shorthand or longhand_. It is merely used in the generation of help output and error messages.
    explicit Positional(const std::string &name)
        : name_(name)
    {
    }
    //!Constructs the positional handler with the given name. Note that _the name may not be a shorthand or longhand_. It is merely used in the generation of help output and error messages. Any values are stored in the given variable.
    template <typename Variable>
    explicit Positional(const std::string &name, Variable &variable)
        : Positional(name)
    {
        add_store_action_(variable);
    }

    virtual bool is_valid() const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Cardinality<Positional>::is_valid_());
        MSS(!core::name::is_shorthand(name_) && !core::name::is_longhand(name_));
        MSS_END();
    }
    virtual std::string name() const override { return name_; }
    virtual std::string type() const override { return "Positional argument"; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Positional>(*this); }
    virtual bool parse(core::Context &context) override
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Positional parsing...");
        MSS(context.info(*this).nr_parses, L("Skipping this handler, as it has already run..."));

        //First process the name
        MSS(!context.args().reached_end());
        const auto name = *context.args().current();

        //Next, process any values
        L("Processing value(s)");
        while (!context.args().reached_end())
        {
            const auto value = *context.args().current();
            L("Considering consuming " << C(value));
            if (context.parser_could_continue(context))
            {
                L("Yielding back, the toplevel parser knows what to do");
                MSS_RETURN_OK();
            }

            //We process all values that we are allowed to process
            if (!nargs_().may_have_next())
            {
                if (this == last_positional_(context))
                {
                    if (!core::name::is_option(value))
                    {
                        L("We can't have another value");
                        context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(*this) << ": expected " << nargs_().description();
                        MSS_RETURN_ERROR();
                    }
                    else
                    {
                        L("We can't have another value. The value is an option which we know the parser can't continue with, but we yield anyway");
                        MSS_RETURN_OK();
                    }
                } else
                {
                    L("Yielding back");
                    MSS_RETURN_OK();
                }
            }

            //Running all actions
            L("Running actions: " << C(actions_().size()));
            MSS(core::handler::property::Actionable<Positional>::run_(context));
            L("All actions were run");

            //Consume value
            L("Consuming " << C(value));
            MSS(context.args().next());

            //Update properties
            nargs_().next();
        }
        MSS(!nargs_().must_have_next(), context.error() << "Missing value(s) for " << core::handler::name_of(*this) << ": expected " << nargs_().description(););

        L("Finished option parsing");
        MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Required<Positional>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required";);
        MSS(is_optional() || core::handler::property::Cardinality<Positional>::is_satisfied_(), context.error() << core::handler::name_of(*this, true) << " expects " << nargs_().description(););
        MSS_END();
    }

private:
    virtual void reset_() override { nargs_().reset(); }
    Positional *last_positional_(core::Context &context) const
    {
        //#TODO: implement this to get better error reporting
        return nullptr;
    }

    std::string name_;
};

} // namespace handler

ARGO_NS_END

#endif

