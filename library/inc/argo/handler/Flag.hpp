#ifndef HEADER_argo_handler_Flag_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_Flag_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/property/Actionable.hpp>
#include <argo/core/handler/property/Help.hpp>
#include <argo/core/handler/property/Named.hpp>
#include <argo/core/handler/property/Required.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/log.hpp>
#include <argo/core/traits/conversion.hpp>
#include <argo/core/utility.hpp>
#include <cassert>
#include <sstream>

ARGO_NS_BEGIN

namespace handler {

//!A handler that processes flags: shorthand or longhand arguments without values.
class Flag : public core::handler::IHandler,
             public core::handler::property::Named,
             public core::handler::property::Help<Flag>,
             public core::handler::property::Required<Flag>,
             public core::handler::property::Actionable<Flag>
{
public:
    //!Constructs a flag with a longhand and shorthand. They may be provided in any order.
    explicit Flag(const std::string &first, const std::string &second)
        : core::handler::property::Named(first, second)
    {
    }
    //!Constructs an option with a longhand or shorthand.
    explicit Flag(const std::string &name)
        : core::handler::property::Named(name)
    {
    }

    virtual bool is_valid() const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Named::is_valid_());
        MSS_END();
    }
    virtual std::string name() const override { return core::handler::property::Named::name_(); }
    virtual std::string type() const override { return "Flag"; }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Flag>(*this); }
    virtual bool parse(core::Context &context) override
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Flag parsing...");

        //First process the name
        MSS(context.args().current() != context.args().end());
        const auto name = *context.args().current();

        //Running actions
        L("Running actions: " << C(actions_().size()));
        MSS(core::handler::property::Actionable<Flag>::run_(context));
        L("All actions were run");

        //Consume the name _after_ applying the actions, so that they get the flag name as value
        L("Consuming " << C(name));
        MSS(context.args().next());

        L("Finished flag parsing");
        MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Required<Flag>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required");
        MSS_END();
    }
};

} // namespace handler

ARGO_NS_END

#endif

