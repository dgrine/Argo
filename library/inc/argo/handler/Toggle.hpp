#ifndef HEADER_argo_handler_Toggle_hpp_INCLUDE_GUARD
#define HEADER_argo_handler_Toggle_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/property/Actionable.hpp>
#include <argo/core/handler/property/Help.hpp>
#include <argo/core/handler/property/Named.hpp>
#include <argo/core/handler/property/Required.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/log.hpp>
#include <argo/core/optional.hpp>
#include <argo/core/traits/conversion.hpp>
#include <argo/core/utility.hpp>
#include <cassert>
#include <functional>
#include <sstream>

ARGO_NS_BEGIN

namespace handler {

//!A handler that processes toggles: arguments that toggle boolean values.
class Toggle : public core::handler::IHandler,
               public core::handler::property::Named,
               public core::handler::property::Help<Toggle>,
               public core::handler::property::Required<Toggle>,
               public core::handler::property::Actionable<Toggle>
{
public:
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function can alter the parsing process.
    explicit Toggle(const std::string &first, const std::string &second, const std::function<bool(core::Context &, const bool)> &setter)
        : core::handler::property::Named(first, second)
        , setter_(setter)
        , negation_longhand_(default_negation_longhand_())
    {
    }
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cannot alter the parsing process.
    explicit Toggle(const std::string &first, const std::string &second, const std::function<void(const bool)> &setter)
        : core::handler::property::Named(first, second)
        , setter_([setter](core::Context &, const bool value){ setter(value); return true; })
        , negation_longhand_(default_negation_longhand_())
    {
    }
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function can alter the parsing process. The passed getter is useful for formatters to display helpful output.
    explicit Toggle(const std::string &first, const std::string &second, const std::function<bool(core::Context &, const bool)> &setter, const std::function<bool()> &getter)
        : Toggle(first, second, setter)
    {
        getter_ = getter;
    }
    //!Constructs a toggle with a longhand and shorthand. They may be provided in any order. The passed variable will be toggled when the handler is triggered.
    explicit Toggle(const std::string &first, const std::string &second, bool &variable)
        : Toggle(first, second, [&variable](core::Context &, const bool value) { variable = value; return true; }, [&variable](){ return variable; })
    {
    }
    //!Constructs a toggle with a longhand or shorthand. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cal alter the parsing process.
    explicit Toggle(const std::string &name, const std::function<bool(core::Context &, const bool)> &setter)
        : core::handler::property::Named(name)
        , setter_(setter)
        , negation_longhand_(default_negation_longhand_())
    {}
    //!Constructs a toggle with a longhand or shorthand. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cannot alter the parsing process.
    explicit Toggle(const std::string &name, const std::function<void(const bool)> &setter)
        : core::handler::property::Named(name)
        , setter_([setter](core::Context &, const bool value){ setter(value); return true; })
        , negation_longhand_(default_negation_longhand_())
    {}
    //!Constructs a toggle with a longhand or shorthand. The passed setter function will be called with the truthy value when the handler is triggered. This version of the setter function cannot alter the parsing process. The passed getter is useful for formatters to display helpful output.
    explicit Toggle(const std::string &name, const std::function<bool(core::Context &, const bool)> &setter, const std::function<bool()> &getter)
        : Toggle(name, setter)
    {
        getter_ = getter;
    }
    //!Constructs a toggle with a longhand or shorthand. The passed variable will be toggled when the handler is triggered.
    explicit Toggle(const std::string &name, bool &variable)
        : Toggle(name, [&variable](core::Context &, const bool value) { variable = value; return true; }, [&variable](){ return variable; })
    {
    }

    virtual bool is_valid() const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Named::is_valid_());
        MSS(is_valid_);
        MSS_END();
    }
    virtual std::string name() const override { return core::handler::property::Named::name_(); }
    virtual std::string type() const override { return "Toggle"; }
    //!Sets the negation longhand. For example, constructing a "--debug" toggle will automatically respond to a "--no-debug" argument. If the latter name is to be different, it can be set using this method.
    Toggle &with_negation(const std::string &longhand)
    {
        const auto is_longhand = core::name::is_longhand(longhand);
        is_valid_ = is_valid_ && is_longhand;
        if (is_longhand) negation_longhand_ = longhand;
        return *this;
    }
    //!Returns the negation longhand.
    std::string negation_longhand() const { return negation_longhand_; }
    //!Indicates if the default value is available.
    bool has_default_value() const { return !!getter_; }
    //!Returns the default value. This assumes `has_default_value()` returns true.
    bool default_value() const { return (*getter_)(); }
    virtual bool accept(core::handler::visitor::IConstVisitor &visitor) const override { return visitor.visit(*this); }
    virtual bool accept(core::handler::visitor::IMutatingVisitor &visitor) override { return visitor.visit(*this); }
    virtual core::handler::Ptr clone() const override { return core::make_unique<Toggle>(*this); }
    virtual bool parse(core::Context &context) override
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Toggle parsing...");

        //First process the name
        MSS(!context.args().reached_end());
        const auto name = *context.args().current();
        L("Consuming name '" << name << "'");
        MSS(context.args().next());

        //Generation of negation
        const bool negating = name == negation_longhand_;
        L(C(negating));

        //Next, process a potential truthy value
        if (!context.args().reached_end())
        {
            const auto value = *context.args().current();
            L("Considering consuming potential truthy value " << C(value));
            const auto truthy = core::traits::conversion<bool>::run(value);
            if (!!truthy)
            {
                const bool value = negating ? !*truthy : *truthy;
                MSS(setter_(context, value));

                //Consume value
                L("Consuming " << C(value));
                MSS(context.args().next());
            }
            else
            {
                L("Not consuming, leaving to toplevel parser");
                const bool value = !negating;
                MSS(setter_(context, value));
            }
        } else MSS(setter_(context, !negating));

        //Running all actions
        L("Running actions: " << C(actions_().size()));
        MSS(core::handler::property::Actionable<Toggle>::run_(context));
        L("All actions were run");

        L("Finished toggle parsing");
        MSS_END();
    }
    virtual bool is_satisfied(core::Context &context) const override
    {
        MSS_BEGIN(bool);
        MSS(core::handler::property::Required<Toggle>::is_satisfied_(context), context.error() << core::handler::name_of(*this, true) << " is required");
        MSS_END();
    }

private:
    std::string default_negation_longhand_() const
    {
        if (!core::handler::property::Named::has_longhand()) return "";
        const auto lhs = core::handler::property::Named::longhand();
        std::ostringstream os;
        os << "--no-" << std::string(std::begin(lhs) + 2, std::end(lhs));
        return os.str();
    }

    std::function<bool(core::Context &, const bool)> setter_;
    core::optional<std::function<bool()>> getter_;
    bool is_valid_ = true;
    std::string negation_longhand_;
};

} // namespace handler

ARGO_NS_END

#endif

