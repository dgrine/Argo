#ifndef HEADER_argo_core_handler_all_hpp_INCLUDE_GUARD
#define HEADER_argo_core_handler_all_hpp_INCLUDE_GUARD

#include <argo/handler/Flag.hpp>
#include <argo/handler/Interface.hpp>
#include <argo/handler/Option.hpp>
#include <argo/handler/Positional.hpp>
#include <argo/handler/Toggle.hpp>
#include <argo/handler/group/Interface.hpp>

#endif
