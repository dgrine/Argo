#ifndef HEADER_argo_Result_hpp_INCLUDE_GUARD
#define HEADER_argo_Result_hpp_INCLUDE_GUARD

#include <argo/ReturnCode.hpp>
#include <argo/core/NotOnFirstTime.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/string/utility.hpp>
#include <ostream>
#include <string>
#include <vector>

ARGO_NS_BEGIN

//!Aggregate holding the end result of the parsing process.
struct Result
{
    ReturnCode status = ReturnCode::SuccessAndContinue; //!<Return code status.
    std::string message;                                //!<Empty unless ReturnCode::Error, in which case it contains a descriptive error message which is intended for the end user.
    std::vector<std::string> arguments;                 //!<The normalized and expanded arguments that were processed. This includes normalizing invocations such as `--foo 1 --foo=2,3` to `--foo 1 2 3` and expanding all response files.

    operator bool() const { return ReturnCode::Error != status; }
};

inline std::ostream &operator<<(std::ostream &os, const Result &result)
{
    os << "{\n";
    os << "  status: \"" << result.status << "\",\n";
    os << "  message: \"" << result.message << "\",\n";
    os << "  arguments: [";
    core::NotOnFirstTime delim;
    for (const auto &arg : result.arguments)
    {
        delim([&os]() { os << ", "; });
        os << core::string::surround(arg, "\"");
    }
    os << "]\n";
    os << "}";
    return os;
}

ARGO_NS_END

#endif

