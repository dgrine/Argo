#ifndef HEADER_argo_ResponseFile_hpp_INCLUDE_GUARD
#define HEADER_argo_ResponseFile_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/string/utility.hpp>
#include <fstream>
#include <vector>

ARGO_NS_BEGIN

//!Response file parser.
class ResponseFile
{
public:
    //!Parses the given reponse file and returns a boolean indicating success.
    bool parse(const std::string &filename)
    {
        MSS_BEGIN(bool);
        filename_ = filename;
        argv_.clear();
        std::ifstream stream(filename, std::ios_base::in);
        MSS(!stream.fail());
        std::string line;
        while (std::getline(stream, line))
        {
            core::string::ltrim(line);
            if (core::string::starts_with(line, "#")) continue;
            const auto args = core::string::split(line);
            std::copy(RANGE(args), std::back_inserter(argv_));
        }
        MSS_END();
    }
    //!Returns the filename of the last parsed response file.
    std::string filename() const { return filename_; }
    //!Returns the number of parsed arguments.
    int argc() const { return argv_.size(); }
    //!Returns the parsed arguments.
    std::vector<std::string> argv() const { return argv_; }

private:
    std::string filename_;
    std::vector<std::string> argv_;
};

ARGO_NS_END

#endif

