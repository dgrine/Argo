#ifndef HEADER_argo_program_Version_hpp_INCLUDE_GUARD
#define HEADER_argo_program_Version_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <string>

ARGO_NS_BEGIN

namespace program {

//!Holds the program's version information.
struct Version
{
    unsigned major;      //!<Major version number.
    unsigned minor;      //!<Minor version number.
    unsigned patch;      //!<Patch version number.
    std::string githash; //!<Git revision hash.

    //!Indicates if the version is set.
    bool is_set() const { return major + minor + patch; }
};

} // namespace program

ARGO_NS_END

#endif

