#ifndef HEADER_argo_program_Name_hpp_INCLUDE_GUARD
#define HEADER_argo_program_Name_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <string>

ARGO_NS_BEGIN

namespace program {

//!Holds the program's different name aliases.
struct Name
{
    std::string brief;    //!<Short name of the program. For example: gcc.
    std::string extended; //!<Long name of the program. For example: GNU Compiler Collection.
};

} // namespace program

ARGO_NS_END

#endif

