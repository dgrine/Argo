#ifndef HEADER_argo_program_Description_hpp_INCLUDE_GUARD
#define HEADER_argo_program_Description_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <string>

ARGO_NS_BEGIN

namespace program {

//!Holds program descriptions: goal and intended usage.
struct Description
{
    std::string brief;    //!<Short description: a one-liner describing the program's goal.
    std::string extended; //!<Long description which may span multiple lines.
    std::string usage;    //!<Describes the program's usage, i.e. its argument invocation. When empty, the output formatters are responsible for generating this information based on the available handlers. However, when the string is not empty, output formatters should use this instead.
};

} // namespace program

ARGO_NS_END

#endif

