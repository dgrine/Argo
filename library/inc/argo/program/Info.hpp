#ifndef HEADER_argo_program_Info_hpp_INCLUDE_GUARD
#define HEADER_argo_program_Info_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/program/Description.hpp>
#include <argo/program/Name.hpp>
#include <argo/program/Version.hpp>
#include <string>

ARGO_NS_BEGIN

namespace program {

//!Aggregate of program-related information.
struct Info
{
    Name name;               //!<Program name.
    Description description; //!<Program description.
    Version version;         //!<Program version information.
    std::string copyright;   //!<Copyright information.
};

} // namespace program

ARGO_NS_END

#endif

