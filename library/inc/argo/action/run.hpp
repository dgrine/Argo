#ifndef HEADER_argo_action_run_hpp_INCLUDE_GUARD
#define HEADER_argo_action_run_hpp_INCLUDE_GUARD

#include <argo/action/Interface.hpp>
#include <argo/core/Context.hpp>
#include <argo/core/memory.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/optional.hpp>
#include <argo/core/string/utility.hpp>
#include <functional>
#include <sstream>
#include <string>

ARGO_NS_BEGIN

namespace action {

//!Action that performs all required conversions and then invokes the associated callback function.
template <typename Type>
class Run : public Interface<Type>
{
public:
    using function_type = std::function<bool(const Type)>;
    using function_with_context_type = std::function<bool(core::Context &, const Type)>;

    explicit Run(const function_type &fnc)
        : simple_(fnc)
    {
    }
    explicit Run(const function_with_context_type &fnc)
        : extended_(fnc)
    {
    }

private:
    virtual core::action::Ptr clone() const override
    {
        if (!!simple_) return core::make_unique<Run>(*simple_);
        assert(!!extended_);
        return core::make_unique<Run>(*extended_);
    }
    virtual bool run(core::Context &context, const Type value) override
    {
        MSS_BEGIN(bool);
        MSS(!!simple_ ? (*simple_)(value) : (*extended_)(context, value),
            {
                if (context.result().message.empty())
                {
                    context.error() << "Could not process " << core::string::surround(*context.args().current(), "'");
                }
            });
        MSS_END();
    }

    core::optional<function_type> simple_;
    core::optional<function_with_context_type> extended_;
};

//Reference implementations
//!Creates an action that accepts a callback with prototype `bool(const Type)`. By default, `Type = std::string`.
template <typename Type = std::string>
Run<Type> run(const typename Run<Type>::function_type &fnc)
{
    return Run<Type>{ fnc };
}
//!\overload Creates an action that accepts a callback with prototype `bool(core::Context &, const Type)`. By default, `Type = std::string`.
template <typename Type = std::string>
Run<Type> run(const typename Run<Type>::function_with_context_type &fnc)
{
    return Run<Type>{ fnc };
}
//Wrappers
//!\overload Creates an action that accepts a callback with prototype `bool(core::Context &)`.
inline Run<std::string> run(const std::function<bool(core::Context &)> &fnc)
{
    auto wrapper = [fnc](core::Context &context, const std::string &value) { fnc(context); return true; };
    return run(wrapper);
}
//!\overload Creates an action that accepts a callback with prototype `void()`. Note that this is the most trivial way of attaching a stateless, non-failing callback to a handler.
inline Run<std::string> run(const std::function<void()> &fnc)
{
    auto wrapper = [fnc](const std::string &value) { fnc(); return true; };
    return run(wrapper);
}

} // namespace action

ARGO_NS_END

#endif

