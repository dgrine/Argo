#ifndef HEADER_argo_action_store_hpp_INCLUDE_GUARD
#define HEADER_argo_action_store_hpp_INCLUDE_GUARD

#include <argo/action/run.hpp>
#include <list>
#include <set>
#include <vector>

ARGO_NS_BEGIN

namespace action {

//!Stores the parsed value in the given variable.
template <typename Type>
Run<Type> store(Type &variable)
{
    auto set = [&variable](Type value) { variable = value; return true; };
    return Run<Type>{ set };
}
//!Stores the `const_value` in the given variable when the associated handler triggers. `ConstType` must be copy-assignable to `Type`.
template <typename Type, typename ConstType>
Run<Type> store(Type &variable, ConstType const_value)
{
    auto set = [&variable, &const_value](Type value) { variable = const_value; return true; };
    return Run<Type>{ set };
}
//!Appends the parsed values in-order to the given vector.
template <typename Type>
Run<Type> store(std::vector<Type> &variable)
{
    auto set = [&variable](Type value) { variable.push_back(value); return true; };
    return Run<Type>{ set };
}
//!Appends the parsed values in-order to the given list.
template <typename Type>
Run<Type> store(std::list<Type> &variable)
{
    auto set = [&variable](Type value) { variable.push_back(value); return true; };
    return Run<Type>{ set };
}
//!Inserts the parsed values in the given set.
template <typename Type>
Run<Type> store(std::set<Type> &variable)
{
    auto set = [&variable](Type value) { variable.insert(value); return true; };
    return Run<Type>{ set };
}

} // namespace action

ARGO_NS_END

#endif

