#ifndef HEADER_argo_action_Interface_hpp_INCLUDE_GUARD
#define HEADER_argo_action_Interface_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/action/IAction.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/string/utility.hpp>
#include <argo/core/traits/conversion.hpp>
#include <cassert>
#include <string>

ARGO_NS_BEGIN

namespace action {

template <typename Type>
class Interface : public core::action::IAction
{
protected:
    virtual bool run(core::Context &, const Type value) = 0;

private:
    virtual bool apply(core::Context &context) override
    {
        using converter = core::traits::conversion<Type>;

        MSS_BEGIN(bool);
        assert(context.args().current() != context.args().end());
        const auto str = *context.args().current();
        const auto value = converter::run(str);
        MSS(!!value,
            {
                constexpr auto description = converter::description;
                context.error() << core::handler::name_of(context.handler(), true) << " expects "
                                << description
                                << " (not " << core::string::surround(str, "'") << ")";
            });
        MSS(run(context, *value));
        MSS_END();
    }
};

} // namespace action

ARGO_NS_END

#endif

