#ifndef HEADER_argo_ReturnCode_hpp_INCLUDE_GUARD
#define HEADER_argo_ReturnCode_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <cassert>
#include <ostream>

ARGO_NS_BEGIN

//!Enumeration class that holds the three end states of the parsing process.
enum class ReturnCode
{
    Error = 0,             //!<Indicates an error occurred during parsing. The intent is to signal the application to handle the error and exit.
    SuccessAndAbort = 1,   //!<Indicates parsing was successful. The intent is to signal the application that although no error occurred, it should still abort. Usually used by flags like `--help`.
    SuccessAndContinue = 2 //!<Indicates parsing was successful. The intent is to signal the application that it may proceed with normal execution.
};

inline std::ostream &operator<<(std::ostream &os, const ReturnCode &rc)
{
    switch (rc)
    {
    case ReturnCode::Error:
        os << "Error";
        break;
    case ReturnCode::SuccessAndAbort:
        os << "SuccessAndAbort";
        break;
    case ReturnCode::SuccessAndContinue:
        os << "SuccessAndContinue";
        break;
    default:
        assert(false);
        break;
    }
    return os;
}

ARGO_NS_END

#endif

