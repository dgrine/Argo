#ifndef HEADER_argo_Configuration_hpp_INCLUDE_GUARD
#define HEADER_argo_Configuration_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/program/Info.hpp>

ARGO_NS_BEGIN

//! Configuration is a structure containing parser behavior and program
//! information.
struct Configuration {
    program::Info program{};  //!< Program information.
    struct Parser {
        bool help = true;             //!< Addition of the `--help` flag.
        bool version = true;          //!< Addition of the `--version` flag.
        bool responsefile = true;     //!< Addition of the default response file handler.
        bool dash_names = true;       //!< Named arguments on the command line have underscores turned to dashes.
        bool implicit_values = true;  //!< When false, values must explicitly be set, e.g. `--option a --option b`, instead of `--option a b`
        bool argument_values = false; //!< When true, values can be of the form `--value` or `-v`
        bool empty_values = false;    //!< When true, values can be empty
    } parser;
};

ARGO_NS_END

#endif

