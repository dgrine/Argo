#ifndef HEADER_argo_require_range_open_open_hpp_INCLUDE_GUARD
#define HEADER_argo_require_range_open_open_hpp_INCLUDE_GUARD

#include <argo/action/run.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/type_traits.hpp>
#include <cassert>

ARGO_NS_BEGIN

namespace require { namespace range {

//!Verifies that the parsed value lies within the range ``]first, last[``.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> open_open(Type first, Type last)
{
    assert(first < last);
    auto check = [first, last](core::Context &context, Type value) {
        MSS_BEGIN(bool);
        const auto is_within_range = (value > first) && (value < last);
        MSS(is_within_range,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value within range ]" << first << ", " << last << "[");
        MSS_END();
    };
    return action::Run<Type>{ check };
}

}} // namespace require::range

ARGO_NS_END

#endif

