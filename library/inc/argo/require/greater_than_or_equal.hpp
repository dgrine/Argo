#ifndef HEADER_argo_require_greater_than_or_equal_hpp_INCLUDE_GUARD
#define HEADER_argo_require_greater_than_or_equal_hpp_INCLUDE_GUARD

#include <argo/action/run.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/type_traits.hpp>
#include <algorithm>

ARGO_NS_BEGIN

namespace require {

//!Verifies that the parsed value is greater than or equal to the given value.
template<typename Type, typename = typename std::is_arithmetic<Type>::type>
action::Run<Type> greater_than_or_equal(Type cutoff)
{
    auto check = [cutoff](core::Context &context, Type value)
    {
        MSS_BEGIN(bool);
        MSS(value >= cutoff,
            context.error() << "Unexpected value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected value greater than or equal to " << cutoff);
        MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

ARGO_NS_END

#endif

