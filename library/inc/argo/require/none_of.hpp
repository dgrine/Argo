#ifndef HEADER_argo_require_none_of_hpp_INCLUDE_GUARD
#define HEADER_argo_require_none_of_hpp_INCLUDE_GUARD

#include <argo/action/run.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/range.hpp>
#include <argo/core/traits/conversion.hpp>
#include <argo/core/type_traits.hpp>
#include <algorithm>

ARGO_NS_BEGIN

namespace require {

//!Verifies that the parsed value is none of the given values.
template<typename Collection, typename Type = typename core::traits::conversion<typename std::remove_cv<typename Collection::value_type>::type>::result_type>
action::Run<Type> none_of(const Collection &values)
{
    auto check = [values](core::Context &context, Type value)
    {
        MSS_BEGIN(bool);
        const auto it = std::find(RANGE(values), value);
        MSS(std::end(values) == it,
            context.error() << "Disallowed value '" << value << "' for " << core::handler::name_of(context.handler()) << ". "
                            << "Expected values other than " << core::string::quote_or(RANGE(values)));
        MSS_END();
    };
    return action::Run<Type>{ check };
}

} // namespace require

ARGO_NS_END

#endif

