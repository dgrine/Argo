#ifndef HEADER_argo_formatter_Interface_hpp_INCLUDE_GUARD
#define HEADER_argo_formatter_Interface_hpp_INCLUDE_GUARD

#include <argo/core/Context_fwd.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/memory.hpp>
#include <argo/core/string/Text.hpp>
#include <argo/core/system/terminal.hpp>
#include <string>

ARGO_NS_BEGIN

class Arguments;

namespace formatter {

class Interface;

using Ptr = std::unique_ptr<Interface>;

//!Interface serving as a base class for concrete formatters.
class Interface
{
public:
    virtual ~Interface() = default;

    //!Sets the output width. Note that a width of 0 corresponds to full width, i.e. the entire width of the terminal console.
    virtual void set_width(unsigned width) { width_ = width; }
    //!Returns the output width.
    virtual unsigned width() const { return 0 == width_ ? core::system::terminal::width() : width_; }
    //!Returns the formatted error message.
    virtual std::string format_error(const core::Context &context, const std::string &message) const = 0;
    //!Returns the formatted usage section.
    virtual std::string format_usage(const core::Context &context) const = 0;
    //!Returns the formatted help section.
    virtual std::string format_help(const core::Context &context) const = 0;
    //!Returns the formatted version section.
    virtual std::string format_version(const core::Context &context) const = 0;

protected:
    static constexpr auto DEFAULT_WIDTH = 80u;

    using Txt = core::string::Text;

    unsigned width_ = 0; //! 0 means dynamic width

private:
    friend class ARGO_NS(Arguments);

    virtual Ptr clone() const = 0;
};

} // namespace formatter

ARGO_NS_END

#endif

