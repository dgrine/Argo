#ifndef HEADER_argo_formatter_Default_hpp_INCLUDE_GUARD
#define HEADER_argo_formatter_Default_hpp_INCLUDE_GUARD

#include <argo/core/Context.hpp>
#include <argo/core/OnlyOnFirstTime.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/visitor/Collector.hpp>
#include <argo/core/string/utility.hpp>
#include <argo/core/utility.hpp>
#include <argo/formatter/Interface.hpp>
#include <argo/handler/all.hpp>
#include <cassert>
#include <iomanip>
#include <stack>

ARGO_NS_BEGIN

namespace formatter {

//!Default formatter.
class Default : public Interface
{
public:
    Default() = default;
    Default(const Default &other)
        : indentations_(other.indentations_)
    {
        out_ << other.out_.str();
    }

    virtual unsigned width() const override
    {
        const auto w = Interface::width() * 0.66;
        return w <= DEFAULT_WIDTH ? DEFAULT_WIDTH : static_cast<unsigned>(w);
    }
    virtual std::string format_error(const core::Context &context, const std::string &message) const override { return message; }
    virtual std::string format_usage(const core::Context &context) const override
    {
        out_.clear();
        section_usage_(context);
        return out_.str();
    }
    virtual std::string format_help(const core::Context &context) const override
    {
        out_.clear();
        section_heading_(context);
        section_usage_(context);
        section_description_(context);
        section_extra_(context);
        section_copyright_(context);
        out_ << Txt::endl();
        return out_.str();
    }
    virtual std::string format_version(const core::Context &context) const override
    {
        out_.clear();
        const auto &program = context.config().program;
        if (!program.name.extended.empty()) out_ << program.name.extended << Txt::endl();
        out_ << version_info_(program.version);
        return out_.str();
    }

protected:
    virtual Ptr clone() const override { return core::make_unique<Default>(); }

    virtual void section_heading_(const core::Context &context) const
    {
        const auto &program = context.config().program;
        {
            std::string title = program.name.extended.empty() ? program.name.brief : program.name.extended;
            if (title.empty()) return;
            const auto nr = title.size() < width() ? ((width() - title.size()) / 2) : 0;
            if (nr)
            {
                std::string shift(nr, ' ');
                out_ << shift << title << Txt::endl();
            }
        }
        if (!program.name.brief.empty())
        {
            out_ << format_title_("NAME");
            push_indent_(2);
            out_ << format_content_(program.name.brief);
            if (!program.description.brief.empty())
            {
                out_ << format_content_(program.description.brief);
            }
            pop_indent_();
        }
        if (program.version.is_set())
        {
            out_ << format_title_("VERSION");
            push_indent_(2);
            out_ << format_content_(version_info_(program.version));
            pop_indent_();
        }
    }
    virtual void section_usage_(const core::Context &context) const
    {
        const auto &program = context.config().program;
        out_ << format_title_("USAGE");
        push_indent_(2);
        if (!program.description.usage.empty())
            out_ << format_content_(program.description.usage);
        else
        {
            const auto &text = generate_usage_(context);
            out_ << format_content_(text);
        }
        out_ << Txt::endl();
        //Legend
        {
            std::ostringstream legend;
            Txt note;
            {
                note << Txt::color_set({ Txt::Color::Underline }) << "Note:" << Txt::color_reset();
            }
            legend << note.str() << std::endl;
            legend << "Groups are indicated with {}" << std::endl;
            legend << "Optional groups and arguments are indicated with ()" << std::endl;
            legend << "Values are indicated with <x> where x is a cardinality marker" << std::endl;
            legend << "Positional arguments are labeled in the corresponding cardinality marker";
            out_ << format_content_(legend.str());
        }
        pop_indent_();
    }
    virtual void section_description_(const core::Context &context) const
    {
        using namespace core::handler::visitor::collector;

        out_ << format_title_("DESCRIPTION");
        const auto &program = context.config().program;
        push_indent_(2);
        if (!program.description.extended.empty())
            out_ << format_content_(program.description.extended) << Txt::endl();
        else if (!program.description.brief.empty())
            out_ << format_content_(program.description.brief) << Txt::endl();

        //Groups
        core::OnlyOnFirstTime line_groups;
        auto cb_group = [&](const handler::group::Interface &group, const unsigned depth) {
            line_groups([&]() { out_ << format_content_("The groups are as follows:") << Txt::endl(); });
            std::vector<std::string> properties = { group.type() };
            if (group.is_required()) properties.push_back("Required");
            out_ << format_item_(group.name(), properties);
            if (!group.help().empty()) out_ << format_content_(group.help());
            for (const auto &handler : group)
            {
                push_indent_(2);
                out_ << format_content_(handler.name());
                pop_indent_();
            }
            return true;
        };
        //Flags
        core::OnlyOnFirstTime line_options;
        auto cb_line_named = [&]() { line_options([&]() { out_ << Txt::endl() << format_content_("The named arguments are as follows:") << Txt::endl(); }); };
        auto cb_flag = [&](const handler::Flag &flag, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(flag.type());
            if (flag.is_required()) properties.push_back("Required");
            out_ << format_item_(flag.name(), properties);
            push_indent_(2);
            if (!flag.help().empty()) out_ << format_content_(flag.help());
            pop_indent_();
            return true;
        };
        //Options
        auto cb_option = [&](const handler::Option &option, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(option.type());
            if (option.is_required()) properties.push_back("Required");
            properties.push_back(option.nargs_type());
            out_ << format_item_(option.name(), properties);
            push_indent_(2);
            if (!option.help().empty()) out_ << format_content_(option.help());
            pop_indent_();
            return true;
        };
        //Toggles
        auto cb_toggle_negation = [&](const handler::Toggle &toggle, const unsigned depth) {
            if (toggle.negation_longhand().empty()) return true;
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(toggle.type());
            out_ << format_item_(toggle.negation_longhand(), properties);
            push_indent_(2);
            {
                Txt help;
                help << "Negation of " << core::handler::name_of(toggle) << ".";
                out_ << format_content_(help.str());
            }
            pop_indent_();
            return true;
        };
        auto cb_toggle = [&](const handler::Toggle &toggle, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(toggle.type());
            if (toggle.is_required()) properties.push_back("Required");
            if (toggle.has_default_value())
            {
                std::ostringstream os;
                os << "Default: " << std::boolalpha << toggle.default_value();
                properties.push_back(os.str());
            }
            out_ << format_item_(toggle.name(), properties);
            push_indent_(2);
            if (!toggle.help().empty()) out_ << format_content_(toggle.help());
            pop_indent_();
            cb_toggle_negation(toggle, depth);
            return true;
        };
        //Custom handlers
        auto cb_custom = [&](const handler::Interface &custom, const unsigned depth) {
            cb_line_named();
            std::vector<std::string> properties;
            properties.push_back(custom.type());
            out_ << format_item_(custom.name(), properties);
            push_indent_(2);
            if (!custom.help().empty()) out_ << format_content_(custom.help());
            pop_indent_();
            return true;
        };
        //Positional
        core::OnlyOnFirstTime line_positionals;
        auto cb_positional = [&](const handler::Positional &positional, const unsigned depth) {
            line_positionals([&]() { out_ << Txt::endl() << format_content_("The positional arguments are as follows:") << Txt::endl(); });
            std::vector<std::string> properties;
            properties.push_back(positional.type());
            if (positional.is_required()) properties.push_back("Required");
            properties.push_back(positional.nargs_type());
            out_ << format_item_(positional.name(), properties);
            push_indent_(2);
            if (!positional.help().empty()) out_ << format_content_(positional.help());
            pop_indent_();
            return true;
        };
        core::handler::visitor::foreach<ConstRecursive<handler::group::Interface>>(context.handlers(), cb_group);
        core::handler::visitor::foreach<ConstRecursive<handler::Flag>>(context.handlers(), cb_flag);
        core::handler::visitor::foreach<ConstRecursive<handler::Toggle>>(context.handlers(), cb_toggle);
        core::handler::visitor::foreach<ConstRecursive<handler::Option>>(context.handlers(), cb_option);
        core::handler::visitor::foreach<ConstRecursive<handler::Interface>>(context.handlers(), cb_custom);
        core::handler::visitor::foreach<ConstRecursive<handler::Positional>>(context.handlers(), cb_positional);
        pop_indent_();
    }
    virtual void section_extra_(const core::Context &context) const {}
    virtual void section_copyright_(const core::Context &context) const
    {
        const auto &program = context.config().program;
        if (program.copyright.empty()) return;

        out_ << format_title_("COPYRIGHT");
        push_indent_(2);
        auto copyright = program.copyright;
        core::string::trim(copyright);
        out_ << format_content_(copyright);
        pop_indent_();
    }

    std::string format_title_(const std::string &title) const
    {
        Txt txt;
        txt << Txt::endl() << Txt::color_set({ Txt::Color::BoldBrightOn }) << title << Txt::color_reset() << Txt::endl() << Txt::endl();
        return txt.str();
    }
    std::string format_content_(const std::string &text) const
    {
        Txt txt;
        txt << core::string::align(text, width(), indent_());
        if (!core::string::ends_with(text, "\n")) txt << Txt::endl();
        return txt.str();
    }
    std::string format_item_(const std::string &name, const std::vector<std::string> &properties) const
    {
        std::ostringstream os;
        std::string item_left;
        {
            Txt txt;
            txt << std::string(indent_(), ' ');
            txt << Txt::color_set({ Txt::Color::BoldBrightOn }) << name << Txt::color_reset();
            item_left = txt.str();
        }
        std::string item_right;
        {
            std::ostringstream os;
            for (const auto &property : properties) os << core::string::surround(property, "[");
            item_right = os.str();
        }
        os << std::left << item_left << std::right << std::setfill(' ') << std::setw(width() - name.size() - indent_()) << item_right << std::endl;
        return os.str();
    }

    template <typename Version>
    std::string version_info_(const Version &version) const
    {
        std::ostringstream info;
        std::vector<unsigned> numbers = { version.major, version.minor, version.patch };
        info << "Release:  " + core::string::join(".", RANGE(numbers));
        const auto githash = std::string(version.githash);
        if (!githash.empty())
        {
            info << std::endl;
            info << "Revision: " + githash;
        }
        return info.str();
    }
    std::string generate_usage_(const core::Context &context) const
    {
        using namespace core::handler::visitor::collector;

        const auto &program = context.config().program;

        std::vector<std::string> items;
        items.push_back(program.name.brief.empty() ? "<app>" : program.name.brief);

        //Groups
        auto cb_group = [&](const handler::group::Interface &group) {
            std::string handler = core::string::surround(group.name(), "{");
            handler = group.is_required() ? handler : core::string::surround(handler, "(");
            items.push_back(handler);
            return true;
        };
        //Flags
        auto cb_flag = [&](const handler::Flag &flag) {
            std::string handler = flag.is_required() ? flag.name() : core::string::surround(flag.name(), "(");
            items.push_back(handler);
            return true;
        };
        //Options
        auto cb_option = [&](const handler::Option &option) {
            std::string handler = option.is_required() ? option.name() : core::string::surround(option.name(), "(");
            items.push_back(handler);
            std::string value = core::string::surround(option.nargs_symbol(), "<");
            items.push_back(value);
            return true;
        };
        //Toggles
        auto cb_toggle = [&](const handler::Toggle &toggle) {
            std::string handler = toggle.is_required() ? toggle.name() : core::string::surround(toggle.name(), "(");
            items.push_back(handler);
            return true;
        };
        //Positional
        auto cb_positional = [&](const handler::Positional &positional) {
            std::string handler = positional.name() + ":" + positional.nargs_symbol();
            handler = core::string::surround(handler, "<");
            handler = positional.is_required() ? handler : core::string::surround(handler, "(");
            items.push_back(handler);
            return true;
        };
        core::handler::visitor::foreach<ConstNonRecursive<handler::Flag>>(context.handlers(), cb_flag);
        core::handler::visitor::foreach<ConstNonRecursive<handler::Toggle>>(context.handlers(), cb_toggle);
        core::handler::visitor::foreach<ConstNonRecursive<handler::Option>>(context.handlers(), cb_option);
        core::handler::visitor::foreach<ConstNonRecursive<handler::group::Interface>>(context.handlers(), cb_group);
        core::handler::visitor::foreach<ConstNonRecursive<handler::Positional>>(context.handlers(), cb_positional);
        return core::string::join(" ", RANGE(items));
    }

    void push_indent_(unsigned indentation) const { indentations_.push(indent_() + indentation); }
    void pop_indent_() const
    {
        if (!indentations_.empty()) indentations_.pop();
    }
    unsigned indent_() const { return indentations_.empty() ? 0 : indentations_.top(); }

    mutable std::stack<unsigned> indentations_;
    mutable Txt out_;
};

} // namespace formatter

ARGO_NS_END

#endif

