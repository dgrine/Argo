#ifndef HEADER_argo_nargs_OneOrMore_hpp_INCLUDE_GUARD
#define HEADER_argo_nargs_OneOrMore_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/nargs/INArgs.hpp>

ARGO_NS_BEGIN

namespace nargs {

class OneOrMore : public core::nargs::INArgs
{
public:
    OneOrMore() = default;
    virtual std::string description() const override { return "one or more arguments"; }
    virtual std::string symbol() const override { return "+"; }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<OneOrMore>(new OneOrMore{ *this }); }
    virtual bool must_have_next() const override { return count_ == 0; };
    virtual bool may_have_next() const override { return true; }
    virtual void next() override { ++count_; }
    virtual void reset() override { count_ = 0; }

private:
    OneOrMore(const OneOrMore &other) = default;

    unsigned count_ = 0;
};

} // namespace nargs

ARGO_NS_END

#endif

