#ifndef HEADER_argo_nargs_FixedNumber_hpp_INCLUDE_GUARD
#define HEADER_argo_nargs_FixedNumber_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/nargs/INArgs.hpp>
#include <sstream>

ARGO_NS_BEGIN

namespace nargs {

class FixedNumber : public core::nargs::INArgs
{
public:
    explicit FixedNumber(const unsigned value)
        : value_(value)
    {
    }

    virtual std::string description() const override
    {
        std::ostringstream os;
        if (0 == value_)
            os << "no arguments";
        else
            os << value_ << " argument" << (value_ > 1 ? "s" : "");
        return os.str();
    }
    virtual std::string symbol() const override { return std::to_string(value_); }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<FixedNumber>(new FixedNumber{ *this }); }
    virtual bool must_have_next() const override { return count_ < value_; };
    virtual bool may_have_next() const override { return must_have_next(); }
    virtual void next() override { ++count_; }
    virtual void reset() override { count_ = 0; }

    unsigned value() const { return value_; }

private:
    FixedNumber(const FixedNumber &other) = default;

    unsigned value_;
    unsigned count_ = 0;
};

} // namespace nargs

ARGO_NS_END

#endif

