#ifndef HEADER_argo_nargs_Optional_hpp_INCLUDE_GUARD
#define HEADER_argo_nargs_Optional_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/nargs/INArgs.hpp>

ARGO_NS_BEGIN

namespace nargs {

class Optional : public core::nargs::INArgs
{
public:
    Optional() = default;
    virtual std::string description() const override { return "an optional argument"; }
    virtual std::string symbol() const override { return "?"; }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<Optional>(new Optional{ *this }); }
    virtual bool must_have_next() const override { return false; };
    virtual bool may_have_next() const override { return count_ == 0; }
    virtual void next() override { ++count_; }
    virtual void reset() override { count_ = 0; }

private:
    Optional(const Optional &other) = default;

    unsigned count_ = 0;
};

} // namespace nargs

ARGO_NS_END

#endif

