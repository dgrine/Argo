#ifndef HEADER_argo_nargs_ZeroOrMore_hpp_INCLUDE_GUARD
#define HEADER_argo_nargs_ZeroOrMore_hpp_INCLUDE_GUARD

#include <argo/core/config/build.hpp>
#include <argo/core/nargs/INArgs.hpp>

ARGO_NS_BEGIN

namespace nargs {

class ZeroOrMore : public core::nargs::INArgs
{
public:
    ZeroOrMore() = default;
    virtual std::string description() const override { return "zero or more arguments"; }
    virtual std::string symbol() const override { return "*"; }

    virtual core::nargs::Ptr clone() const override { return std::unique_ptr<ZeroOrMore>(new ZeroOrMore{ *this }); }
    virtual bool must_have_next() const override { return false; };
    virtual bool may_have_next() const override { return true; }
    virtual void next() override {}
    virtual void reset() override {}

private:
    ZeroOrMore(const ZeroOrMore &other) = default;
};

} // namespace nargs

ARGO_NS_END

#endif

