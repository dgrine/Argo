#ifndef HEADER_argo_Arguments_hpp_INCLUDE_GUARD
#define HEADER_argo_Arguments_hpp_INCLUDE_GUARD

#include <argo//handler/ResponseFile.hpp>
#include <argo/Configuration.hpp>
#include <argo/action/run.hpp>
#include <argo/core/Context.hpp>
#include <argo/core/Handlers.hpp>
#include <argo/core/Phase.hpp>
#include <argo/core/assert.hpp>
#include <argo/core/config/build.hpp>
#include <argo/core/handler/IHandler.hpp>
#include <argo/core/handler/search/Custom.hpp>
#include <argo/core/handler/search/FirstMatch.hpp>
#include <argo/core/handler/search/Option.hpp>
#include <argo/core/handler/search/Positional.hpp>
#include <argo/core/handler/utility.hpp>
#include <argo/core/handler/visitor/ApplyFunction.hpp>
#include <argo/core/handler/visitor/IsType.hpp>
#include <argo/core/log.hpp>
#include <argo/core/memory.hpp>
#include <argo/core/mss.hpp>
#include <argo/core/raii.hpp>
#include <argo/core/range.hpp>
#include <argo/core/utility.hpp>
#include <argo/formatter/Default.hpp>
#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

ARGO_NS_BEGIN

//!Arguments is the toplevel parser of arguments. It is configured step-by-step by iteratively adding handlers to it.
class Arguments
{
public:
    //!Default constructor.
    Arguments()
    {
        process_configuration_();
    }
    //!Constructs the parser with the given configuration.
    explicit Arguments(const Configuration &config)
        : config_(config)
    {
        process_configuration_();
    }
    //!Copy constructs the parser.
    Arguments(const Arguments &other)
        : config_(other.config_)
        , handlers_(other.handlers_)
        , pformatter_(other.formatter().clone())
    {
    }
    //!Move constructs the parser.
    Arguments(Arguments &&other)
        : config_(std::move(other.config_))
        , handlers_(other.handlers_)
        , pformatter_(other.formatter().clone())
    {
    }
    //!Copy assigns the parser.
    Arguments &operator=(const Arguments &other)
    {
        if (this != &other)
        {
            config_ = other.config_;
            handlers_ = other.handlers_;
            pformatter_ = other.formatter().clone();
            pcontext_ = nullptr;
            phase_ = core::Phase::OptionsFirstPass;
        }
        return *this;
    }
    //!Move assigns the parser.
    Arguments &operator=(Arguments &&other)
    {
        if (this != &other)
        {
            config_ = std::move(other.config_);
            handlers_ = std::move(other.handlers_);
            pformatter_ = std::move(other.pformatter_);
            pcontext_ = nullptr;
            phase_ = core::Phase::OptionsFirstPass;
        }
        return *this;
    }

    //!Returns the configuration.
    const Configuration &configuration() const { return config_; }
    //!Adds the given handler to the parser. Returns a boolean indicating success.
    bool add(const core::handler::IHandler &handler)
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Adding handler " << handler);
        MSS(handler.is_valid(), context_().error() << "Programmer failure: " << core::handler::name_of(handler) << " is wrongly configured";);
        MSS(handlers_.add(handler), context_().error() << "Programmer failure: attempted to add multiple handlers for " << core::handler::name_of(handler););
        MSS_END();
    }
    //!Merges the given parser by adding all its handlers. If a handler conflicts, the handler of the other parser is used when `replace = true` or skipped when `replace = false`.
    bool merge(const Arguments &other, bool replace = true)
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Merging parsers...");
        for (const auto &handler : other.handlers_)
        {
            if (!handlers_.has(handler))
            {
                L("Adding " << handler.name());
                MSS(add(handler));
            }
            else if (replace)
            {
                L("Replacing " << handler.name());
                MSS(handlers_.remove(handler));
                MSS(add(handler));
            }
            else
            {
                L("Skipping " << handler.name());
            }
        }
        L("Merging done");
        MSS_END();
    }
    //!Parses the given arguments. By default, the first argument is not skipped. Useful for unit-testing or when the arguments are not directly read from `argv`.
    Result parse(const std::vector<std::string> &arguments, const bool skip_first_argument = false)
    {
        S(ARGO_DEBUG);
        L("Starting parsing");
        reset_();
        auto data = core::normalize_arguments(arguments);
        auto args = core::Args{ data };
        pcontext_ = create_context_(*this, &args);
        if (skip_first_argument && context_().args().current() != context_().args().end()) context_().args().next();
        process_();
        auto result = context_().result();
        result.arguments.assign(RANGE(context_().args()));
        L("Finished parsing");
        return result;
    }
    //!Parses the given C-style arguments. By default, the first argument is skipped. Useful to directly pass the arguments of `main` where the first argument is the application's executable name.
    template <typename CharArrayType>
    Result parse(unsigned int argc, CharArrayType argv, const bool skip_first_argument = true)
    {
        std::vector<std::string> args;
        std::copy(argv, argv + argc, std::back_inserter(args));
        if (config_.program.name.brief.empty() && !!argc) config_.program.name.brief = core::basename_of(argv[0]);
        return parse(args, skip_first_argument);
    }
    //!Prints the usage section to the given output stream.
    bool print_usage(std::ostream &os) const
    {
        MSS_BEGIN(bool);
        MSS(!!os);
        os << formatter().format_usage(context_());
        MSS_END();
    }
    //!Prints the help section to the given output stream.
    bool print_help(std::ostream &os) const
    {
        MSS_BEGIN(bool);
        MSS(!!os);
        os << formatter().format_help(context_());
        MSS_END();
    }
    //!Prints the version section to the given output stream.
    bool print_version(std::ostream &os) const
    {
        MSS_BEGIN(bool);
        MSS(!!os);
        os << formatter().format_version(context_());
        MSS_END();
    }
    //!Sets the output formatter.
    void set_formatter(const formatter::Interface &formatter) { pformatter_ = formatter.clone(); }
    //!Returns the output formatter.
    const formatter::Interface &formatter() const
    {
        if (!pformatter_) pformatter_ = core::make_unique<formatter::Default>();
        assert(!!pformatter_);
        return *pformatter_;
    }
    //!Returns the output formatter.
    formatter::Interface &formatter()
    {
        const auto &self = *this;
        return const_cast<formatter::Interface &>(self.formatter());
    }

private:
    static std::unique_ptr<core::Context> create_context_(Arguments &parser, core::Args *args = nullptr)
    {
        core::Context::ParserCouldContinueFunction parser_could_continue = [&parser](core::Context &context) {
            S(ARGO_DEBUG);
            L("Checking if parser could continue with current argument in phase " << parser.phase_);
            core::handler::search::FirstMatch search{ context };
            search.add<core::handler::search::Custom>();
            if (core::Phase::Positionals == parser.phase_) search.add<core::handler::search::Positional>();
            search.add<core::handler::search::Option>();
            parser.handlers_.accept_on_elements(search);
            const bool could_continue = search.result();
            L(C(could_continue));
            return could_continue;
        };
        auto ptr = core::make_unique<core::Context>(parser, parser.config_, parser.handlers_, parser.phase_, parser_could_continue);
        if (!!args) ptr->set_args(*args);
        return ptr;
    }
    std::unique_ptr<core::Context> create_switch_context_(Arguments &parser) { return create_context_(parser, &context_().args()); }
    const core::Context &context_() const
    {
        if (!pcontext_) pcontext_ = create_context_(const_cast<Arguments &>(*this));
        assert(!!pcontext_);
        return *pcontext_;
    }
    core::Context &context_()
    {
        const auto &self = *this;
        return const_cast<core::Context &>(self.context_());
    }
    void process_configuration_()
    {
        if (config_.parser.help)
        {
            ARGO_ASSERT_RELEASE(add_help_());
        }
        if (config_.parser.version)
        {
            ARGO_ASSERT_RELEASE(add_version_());
        }
        if (config_.parser.responsefile)
        {
            ARGO_ASSERT_RELEASE(add_responsefile_());
        }
    }
    bool add_help_()
    {
        MSS_BEGIN(bool);
        handler::Flag flag{ "--help", "-h" };
        flag.action(action::run([](core::Context &context, const std::string &) {
            context.parser().print_help(std::cout);
            context.abort();
            return true;
        }));
        flag.help("Prints out this help.");
        MSS(add(flag));
        MSS_END();
    }
    bool add_version_()
    {
        MSS_BEGIN(bool);
        handler::Flag flag{ "--version" };
        flag.action(action::run([](core::Context &context, const std::string &) {
            context.parser().print_version(std::cout);
            context.abort();
            return true;
        }));
        flag.help("Prints out the version information.");
        MSS(add(flag));
        MSS_END();
    }
    bool add_responsefile_()
    {
        MSS_BEGIN(bool);
        handler::ResponseFile rsp;
        MSS(add(rsp));
        MSS_END();
    }
    core::handler::IHandler *search_()
    {
        S(ARGO_DEBUG);
        const auto &arg = *context_().args().current();
        core::handler::search::FirstMatch search{ context_() };
        auto lookup = [&]() {
            L("Searching handler for " << C(arg) << C(phase_));
            search.clear();
            search.add<core::handler::search::Custom>();
            switch (phase_)
            {
            case core::Phase::OptionsFirstPass:
                search.add<core::handler::search::Option>();
                break;
            case core::Phase::Positionals:
                search.add<core::handler::search::Positional>();
                break;
            case core::Phase::OptionsLastPass:
                search.add<core::handler::search::Option>();
                break;
            default:
                assert(false);
                break;
            }
            handlers_.accept_on_elements(search);
        };
        while (core::Phase::Nr_ != phase_ && !search.result())
        {
            lookup();
            if (!search.result())
            {
                //#TODO: optimization: if the current argument is an option
                // we don't need to go through all the phases. If we passed
                // the first options phase, anything else is superfluous
                L("No result found, moving to next phase");
                phase_ = core::Phase(static_cast<unsigned>(phase_) + 1);
            }
        }
        return search.result();
    }
    bool process_()
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        if (context_().aborted()) MSS_RETURN_OK();
        L("Processing arguments using parser " << this << "...");
        while (context_().args().current() != context_().args().end())
        {
            if (context_().aborted())
            {
                L("Action requested aborting processing...");
                MSS_RETURN_OK();
            }
            const auto &arg = *context_().args().current();
            auto phandler = search_();
            MSS(!!phandler,
                {
                    auto error = context_().error();
                    if (core::name::is_option(arg))
                        error << "Unknown option";
                    else
                    {
                        error << "Unexpected positional argument";
                    }
                    error << " " << core::string::surround(arg, "'");
                });
            auto &handler = *phandler;
            L("Handler found: " << core::handler::name_of(handler));
            L("Verifying delegate parser " << core::handler::name_of(handler) << " is valid");
            MSS(handler.is_valid(),
                {
                    context_().error() << "Handler for argument " << core::string::surround(arg, "'") << " is wrongly configured";
                });
            MSS(update_info_(handler));
            L("Yielding to delegate parser");
            context_().set_handler(handler);
            MSS(handler.parse(context_()));
            L("Continuing with toplevel parsing");
            L("Checking if a parser switch occurred");
            if (context_().is_current_parser(*this))
            {
                L("No, so let's continue");
            }
            else
            {
                auto &parser = context_().parser();
                L("Yes, a parser switch occurred. Delegating to parser " << &parser << "...");
                parser.pcontext_ = create_switch_context_(parser);
                parser.process_();
                L("Parser switch finished");
                context_().result() = parser.context_().result();
                MSS(ReturnCode::SuccessAndContinue == context_().result().status);
                L("Continuing where we left off with parser " << this << "...");
                context_().switch_parser(*this);
            }
        }
        L("Finished processing all arguments");
        if (!context_().aborted()) MSS(is_satisfied_());
        MSS_END();
    }
    bool update_info_(const core::handler::IHandler &handler)
    {
        S(ARGO_DEBUG);
        auto pcurrent = &handler;
        while (!!pcurrent)
        {
            L("Updating handler info of '" << *pcurrent << "'");
            auto &info = context_().info(*pcurrent);
            ++info.nr_parses;
            L("-> " << C(info.nr_parses));
            pcurrent = pcurrent->parent();
            if (!!pcurrent)
            {
                L("Handler has parent: " << *pcurrent);
            }
        }
        return true;
    }
    bool is_satisfied_()
    {
        S(ARGO_DEBUG);
        MSS_BEGIN(bool);
        L("Checking all handlers are satisified...");
        core::handler::visitor::ApplyConstFunction function{
            [&](const core::handler::IHandler &handler) {
                S(ARGO_DEBUG);
                L("Checking handler " << core::handler::name_of(handler) << " is satisfied");
                const auto res = handler.is_satisfied(context_());
                L("-> satisfied: " << C(res));
                return res;
            }
        };
        MSS(handlers_.accept_on_elements(function));
        L("All handlers are satisfied");
        MSS_END();
    }
    void reset_()
    {
        S(ARGO_DEBUG);
        for (auto &handler: handlers_)
        {
            L("Resetting handler " << handler.name());
            handler.reset_();
        }
    }

    Configuration config_{};
    core::Handlers handlers_;
    mutable formatter::Ptr pformatter_ = nullptr;
    mutable std::unique_ptr<core::Context> pcontext_ = nullptr;
    core::Phase phase_ = core::Phase::OptionsFirstPass;
};

ARGO_NS_END

#endif

