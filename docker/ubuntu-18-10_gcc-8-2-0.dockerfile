FROM ubuntu:18.10
MAINTAINER D. Grine
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    build-essential cmake git python3 python3-pip \
    && apt-get autoremove \
    && pip3 install invoke
RUN mkdir -p /workspace
COPY . /workspace/
WORKDIR /workspace/
CMD inv qc --no-docker

