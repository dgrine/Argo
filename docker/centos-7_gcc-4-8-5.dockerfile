FROM centos:latest
MAINTAINER D. Grine
RUN yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
    && yum update -y \
    && yum install -y wget python36u python36u-libs python36u-devel python36u-pip \
    && yum groupinstall -y "Development Tools" \
    && pip3.6 install invoke
RUN mkdir -p /workspace \
    && cd /workspace \
    && wget http://www.cmake.org/files/v3.1/cmake-3.1.3.tar.gz \
    && tar xzf cmake-3.1.3.tar.gz \
    && cd cmake-3.1.3 \
    && ./bootstrap && make && make install \
    && cd .. ; rm -rf cmake*
COPY . /workspace/
WORKDIR /workspace/
CMD inv qc --no-docker

