from invoke import task
import pathlib

THIS_DIR = pathlib.Path(__file__).parent.resolve()
fn_dockers = sorted([fn for fn in THIS_DIR.glob('*.dockerfile')])

def fmt(name):
    parts = name.split('-')
    title = parts[0].title()
    version = '.'.join(parts[1:])
    return "{}-{}".format(title, version)

@task
def list(context):
    """
    Lists the platforms on which the library is verified.
    """
    with context.cd(str(THIS_DIR)):
        for fn in fn_dockers:
            name = fn.stem
            parts = name.split('_')
            system = fmt(parts[0])
            compiler = fmt(parts[1])
            print("* {} / {} ({})".format(system, compiler, name))

@task
def test(context, platform = ""):
    """
    Tests the library on different platforms.
    """
    with context.cd(str(THIS_DIR)):
        for fn in fn_dockers:
            name = fn.stem
            if platform and name != platform:
                "Skipping {}".format(name)
                continue
            tag = 'argo/{}'.format(name)
            print("Verifying {}".format(name))
            try:
                print("Building image tag {}".format(tag))
                cmd = 'docker build -t {} -f {} ..'.format(tag, fn)
                context.run(cmd, pty = True)
            except Exception as err:
                print("Could not build image tag {}".format(tag))
                exit(1)
            try:
                print("Runnning container with image tag {}...".format(tag))
                cmd = 'docker run -t {}'.format(tag)
                context.run(cmd, pty = True)
            except Exception:
                print("Integration failed failed on platform {} using image tag {}".format(fmt(name), tag))
                exit(1)
            print("Library successfully passed integration tests for platform {} using image tag {}".format(fmt(name), tag))
        print("Library successfully passed all integration tests")
