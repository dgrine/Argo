FROM ubuntu:16.04
MAINTAINER D. Grine
RUN apt-get update \
    && apt-get install -y build-essential \
       checkinstall \
       libreadline-gplv2-dev \
       libncursesw5-dev \
       libssl-dev \
       libsqlite3-dev \
       tk-dev \
       libgdbm-dev \
       libc6-dev \
       libbz2-dev \
       zlib1g-dev \
       openssl \
       libffi-dev \
       python3-dev \
       python3-setuptools \
       wget \
    && mkdir -p /workspace/Python37 \
    && cd /workspace/Python37 \
    && wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tar.xz \
    && tar xvf Python-3.7.0.tar.xz \
    && cd /workspace/Python37/Python-3.7.0 \
    && ./configure \
    && make altinstall \
    && cd /workspace \
    && rm -rf Python37
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    build-essential cmake git \
    && apt-get autoremove \
    && pip3.7 install invoke
RUN mkdir -p /workspace
COPY . /workspace/
WORKDIR /workspace/
CMD inv qc --no-docker

