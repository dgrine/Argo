from invoke import task, Collection
import pathlib

THIS_DIR = pathlib.Path(__file__).parent.resolve()
BUILD_DIR = pathlib.Path(THIS_DIR, 'build')

ns = Collection()

@task
def build(context):
    """
    Builds the examples.
    """
    with context.cd(str(THIS_DIR)):
        context.run('mkdir -p {dir} && cd {dir} && cmake .. && make -j4'.format(dir = BUILD_DIR), pty = True)
ns.add_task(build)

@task
def clean(context):
    """
    Removes the build directory.
    """
    if BUILD_DIR.exists():
        context.run('rm -rf {dir}'.format(dir = BUILD_DIR))
ns.add_task(clean)

