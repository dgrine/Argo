#include <argo/Argo.hpp>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using namespace argo;

int main(int argc, char **argv)
{
    std::string output_file;
    std::vector<std::string> input_files;
    Arguments args;
    args.add(handler::Option{"--output", output_file}.required());
    args.add(handler::Positional{"input-files", input_files}.required().nargs("+"));
    const auto result = args.parse(argc, argv);
    switch (result.status)
    {
        case ReturnCode::Error: std::cerr << "Error: " << result.message << std::endl; return 1;
        case ReturnCode::SuccessAndAbort: return 0;
        default: break;
    }
    std::cout << "Output file: " << output_file << std::endl;
    std::cout << "Input files: ";
    for (const auto &input_file: input_files) std::cout << input_file << " ";
    std::cout << std::endl;
    return 0;
}
