#include <argo/Argo.hpp>
#include <iostream>
#include <list>
#include <set>
#include <string>
#include <vector>

using namespace argo;

template<typename Collection>
void pp(const Collection &collection, const std::string &name)
{
    std::cout << name << ": ";
    for (const auto item: collection) std::cout << item << " ";
    std::cout << std::endl;
}

int main(int argc, char **argv)
{
    std::vector<int> v;
    std::set<int> s;
    std::list<int> l;
    
    Arguments args;
    args.add(handler::Option{"--vector", v});
    args.add(handler::Option{"--set", s});
    args.add(handler::Option{"--list", l}.nargs("*"));
    const auto result = args.parse(argc, argv);
    switch (result.status)
    {
        case ReturnCode::Error: std::cerr << "Error: " << result.message << std::endl; return 1;
        case ReturnCode::SuccessAndAbort: return 0;
        default: break;
    }
    pp(v, "Vector");
    pp(s, "Set");
    pp(l, "List");
    return 0;
}
