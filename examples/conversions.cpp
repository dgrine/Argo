#include <argo/Argo.hpp>
#include <iostream>
#include <string>

using namespace argo;

namespace {

    class MyFile
    {
    public:
        MyFile() = default;
        explicit MyFile(const std::string &path) {}
    };

}

namespace argo { namespace core { namespace traits {

    template<>
    struct conversion<MyFile>
    {
        static constexpr const char *description = "a MyFile";
        static optional<MyFile> run(const std::string &value)
        {
            try
            {
                return MyFile(value);
            } catch (const std::exception &)
            {
                return nullopt;
            }
        }
    };

} } }

int main(int argc, char **argv)
{
    MyFile fn;
    Arguments args;
    args.add(handler::Option{"--input", fn});
    const auto result = args.parse(argc, argv);
    switch (result.status)
    {
        case ReturnCode::Error: std::cerr << "Error: " << result.message << std::endl; return 1;
        case ReturnCode::SuccessAndAbort: return 0;
        default: break;
    }
    return 0;
}
