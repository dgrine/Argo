#include <argo/Argo.hpp>
#include <cassert>
#include <iostream>

using namespace argo;

// supports --debug, --debug true, --debug false, --no-debug, --no-debug true, --no-debug false
int main(int argc, char **argv)
{
    bool debug = false;
    
    Arguments args;
    handler::Toggle toggle{"--debug", debug};
    args.add(toggle);
    const auto result = args.parse(argc, argv);
    switch (result.status)
    {
        case ReturnCode::Error: std::cerr << "Error: " << result.message << std::endl; return 1;
        case ReturnCode::SuccessAndAbort: return 0;
        default: break;
    }
    std::cout << "debug: " << std::boolalpha << debug << std::endl;
    return 0;
}
