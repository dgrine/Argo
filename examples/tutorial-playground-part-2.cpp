#include <argo/Argo.hpp>
#include <cassert>
#include <iostream>
#include <string>

using namespace argo;

struct Options
{
    std::vector<int> numbers;
    bool reverse = false;
    std::vector<int> squared;
};

void pp(const std::string &name, const std::vector<int> &numbers)
{
    std::cout << name << ": ";
    for (const auto number : numbers)
        std::cout << number << " ";
    std::cout << "\n";
}

int main(int argc, char **argv)
{
    Options options{};

    Arguments args{};
    {
        handler::Option numbers{ "--numbers", options.numbers };
        numbers.nargs("+");
        numbers.help("Numbers to display");
        numbers.required();
        auto square = action::run<int>([&options](const int number) {
            options.squared.push_back(number * number);
            return true;
        });
        numbers.action(square);
        auto validate = action::run<int>([&options](core::Context &context,
                                                    const int number) {
            if (number % 2 == 0)
                return true;
            context.error() << "Odd numbers such as " << number << " are not allowed";
            return false;
        });
        numbers.action(validate);
        args.add(numbers);
    }
    {
        args.add(handler::Toggle{ "--reverse", options.reverse }.help(
            "Reverses the order of the numbers"));
    }
    const auto result = args.parse(argc, argv);
    switch (result.status)
    {
    case ReturnCode::Error:
        std::cerr << "Error: " << result.message << std::endl;
        return 1;
    case ReturnCode::SuccessAndAbort:
        return 0;
    case ReturnCode::SuccessAndContinue:
        break;
    }
    if (options.reverse)
    {
        std::reverse(std::begin(options.numbers), std::end(options.numbers));
        std::reverse(std::begin(options.squared), std::end(options.squared));
    }
    pp("numbers", options.numbers);
    pp("squared", options.squared);
    return 0;
}
