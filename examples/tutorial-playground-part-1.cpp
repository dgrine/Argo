#include <argo/Argo.hpp>
#include <cassert>
#include <iostream>
#include <string>

using namespace argo;

struct Options
{
    std::vector<int> numbers;
};

void pp(const std::string &name, const std::vector<int> &numbers)
{
    std::cout << name << ": ";
    for (const auto number : numbers) std::cout << number << " ";
    std::cout << "\n";
}

int main(int argc, char **argv)
{
    Options options{};

    Arguments args{};
    {
        handler::Option numbers{ "--numbers", options.numbers };
        numbers.nargs("+");
        numbers.help("Numbers to display");
        numbers.required();
        args.add(numbers);
    }
    const auto result = args.parse(argc, argv);
    switch (result.status)
    {
    case ReturnCode::Error:
        std::cerr << "Error: " << result.message << std::endl;
        return 1;
    case ReturnCode::SuccessAndAbort:
        return 0;
    case ReturnCode::SuccessAndContinue:
        break;
    }
    pp("number", options.numbers);
    return 0;
}
