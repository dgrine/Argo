Change log
==========

This file documents high-level changes made to the library.

1.5.3
~~~~~

- Date 2021, July 4
- Description:

  - Missing include
  - Fixed warnings produced by Clang 

1.4.7
~~~~~

- Date: 2019, May 29
- Description:

  - Empty values are configurable via ``Configuration::empty_values`` (``false`` by default)

1.4.6
~~~~~

- Date: 2019, May 29
- Description:

  - Minor clean-up for code consistency

1.4.5
~~~~~

- Date: 2019, May 23
- Description:

  - Argument-like values are configurable via ``Configuration::argument_values`` (``false`` by default)

1.4.4
~~~~~

- Date: 2019, May 14
- Description:

  - Fixed warning (``-Wdefaulted-function-deleted``) on Linux with Clang
  - Documentation update

1.4.3
~~~~~

- Date: 2019, May 14
- Description:

  - Unit-tests are compiled with ``-Wall`` and no warnings are produced under Clang
  - Minor improvements
  - Changelog added

1.4.2
~~~~~

- Date: 2019, Apr 8
- Description:

  - Documentation update for include changes (``<>`` instead of ``""``)
  - Float conversions are exception-free

1.4.1
~~~~~

- Date: 2019, Mar 19
- Description:

  - Fixed bug where logging would remain active

1.4.0
~~~~~

- Date: 2019, Mar 19
- Description:

  - Added ability to change behavior of positionals parsing using ``Configuration::implicit_values``

1.3.4
~~~~~

- Date: 2019, Feb 24
- Description:

  - Changed includes from ``""`` to ``<>``

1.3.3
~~~~~

- Date: 2019, Jan 21
- Description:

  - Documentation update
  - Move operator improvements
  - Docker build context reduced

1.3.2
~~~~~

- Date: 2018, Dec 12
- Description:

  - Minor improvements

1.3.1
~~~~~

- Date: 2018, Dec 10
- Description:

  - Code style consistency

1.3.0
~~~~~

- Date: 2018, Dec 6
- Description:

  - Parser robustness improved

1.2.11
~~~~~~

- Date: 2018, Nov 30
- Description:

  - Minor improvements

1.2.10
~~~~~~

- Date: 2018, Nov 28
- Description:

  - ``std::optional`` conversion improved

1.2.9
~~~~~

- Date: 2018, Nov 28
- Description:

  - ``std::optional`` support improved

1.2.8
~~~~~

- Date: 2018, Nov 23
- Description:

  - ``formatter::Default`` improved

1.2.7
~~~~~

- Date: 2018, Nov 21
- Description:

  - Fixed namespace issue for embedded libraries

1.2.6
~~~~~

- Date: 2018, Nov 21
- Description:

  - ``formatter::Default`` improved for toggles

1.2.5
~~~~~

- Date: 2018, Nov 14
- Description:

  - Added ``require`` collection
  - Unit-test cases for toggles extended
  - Documentation update
  - Code style consistency
  - Minor improvements

1.2.4
~~~~~

- Date: 2018, Nov 13
- Description:

  - Fixed issue where ``nargs`` property would not always reset correctly
  - Unit-test cases extended

1.2.3
~~~~~

- Date: 2018, Nov 8
- Description:

  - Response file support improved
  - Documentation update
  - Unit-test cases extended

1.2.2
~~~~~

- Date: 2018, Nov 7
- Description:

  - Minor improvements

1.2.1
~~~~~

- Date: 2018, Oct 26
- Description:

  - Automatic prefixing support added
  - Unit-test cases extended
  - Documentation update

1.1.2
~~~~~

- Date: 2018, Oct 22
- Description:

  - Fixed issue where wrong ``Option`` constructor would sometimes be chosen when compiling with GCC

1.1.1
~~~~~

- Date: 2018, Oct 22
- Description:

  - Fixed issue in parser where buffer overflow could occur on edge cases

1.1.0
~~~~~

- Date: 2018, Oct 19
- Description:

  - Fixed issue where wrong ``Option`` constructor would sometimes be chosen when compiling with GCC
  - Documentation update

1.0.25
~~~~~~

- Date: 2018, Oct 12
- Description:

  - Automatic dashing support added using ``Configuration::dash_names``
  - Documentation update

1.0.24
~~~~~~

- Date: 2018, Oct 12
- Description:

  - Fixed compiler warning
  - Documentation update: project moved to GitLab

1.0.23
~~~~~~

- Date: 2018, Sep 22
- Description:

  - Parsing of groups improved

1.0.22
~~~~~~

- Date: 2018, Sep 22
- Description:

  - Parsing of groups improved
  - Documentation update

1.0.21
~~~~~~

- Date: 2018, Sep 19
- Description:

  - Fixed bug where logging would remain active

1.0.20
~~~~~~

- Date: 2018, Sep 19
- Description:

  - Documentation update

1.0.19
~~~~~~

- Date: 2018, Sep 19
- Description:

  - Improved groups and toggles
  - Fixed issue with missing virtual destructor
  - Unit-test cases extended
  - Documentation update

1.0.18
~~~~~~

- Date: 2018, Sep 16
- Description:

  - Metric symbol conversion support added
  - Unit-test cases extended

1.0.17
~~~~~~

- Date: 2018, Sep 12
- Description:

  - ``formatter::Default`` improved
  - Documentation update

1.0.16
~~~~~~

- Date: 2018, Sep 9
- Description:

  - Parser improvements
  - Documentation update

1.0.14
~~~~~~

- Date: 2018, Sep 7
- Description:

  - Error handling improved
  - Code style consistency
  - Examples update
  - Unit-test cases extended

1.0.13
~~~~~~

- Date: 2018, Sep 7
- Description:

  - Toggles improved

1.0.11
~~~~~~

- Date: 2018, Sep 3
- Description:

  - Toggles and groups improved
  - Fixed bug where numeric conversion would not always be correct on 32-bit systems

1.0.10
~~~~~~

- Date: 2018, Sep 2
- Description:

  - Numeric conversions improved

1.0.9
~~~~~

- Date: 2018, Sep 2
- Description:

  - Single header release improved


1.0.8
~~~~~

- Date: 2018, Aug 31
- Description:

  - Numeric conversion improved

1.0.7
~~~~~

- Date: 2018, Aug 30
- Description:

  - ``formatter::Default`` improved
  - Minor improvements

1.0.6
~~~~~

- Date: 2018, Aug 28
- Description:

  - Minor improvements

1.0.5
~~~~~

- Date: 2018, Aug 28
- Description:

  - Minor improvements

1.0.3
~~~~~

- Date: 2018, Aug 28
- Description:

  - Fixed bug in ``formatter::Default`` where the version would not be printed

1.0.2
~~~~~

- Date: 2018, Aug 28
- Description:

  - Storing variables in STL collections (``std::vector``, ``std::set``, ``std::list``)

1.0.1
~~~~~

- Date: 2018, Aug 28
- Description:

  - Configuration struct improved
  - ``formatter::Default`` improved: terminal width is detected on POSIX systems
  - Documentation updated

1.0.0
~~~~~

- Date: 2018, Aug 28
- Description:

  - Parser algorithm redesigned
  - Internal codebase restructure
  - API rewrite
  - Documentation updated
  - Examples extended

