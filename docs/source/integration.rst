Integration
===========

Argo consists of just a single header file without any external dependencies, **no building or installation is required**. Simply make the header file available to your project.

Requirements
------------

A C++11 compliant compiler.

The library is tested on the following platforms:

* macOS 10.14 / Clang (Apple LLVM version 10.0.1)
* macOS 10.13 / Clang (Apple LLVM version 9.1.0)
* Ubuntu-18.10 / GCC-8.2.0
* Ubuntu-18.04 / GCC-7.3.0
* Ubuntu-16.04 / GCC-5.4.0
* CentOS-7 / GCC-4.8.5
* Microsoft Windows 10 / Visual Studio 2017

Basic integration
-----------------

Clone the repository:

.. code-block:: Text

   $ git clone https://gitlab.com/dgrine/Argo.git

Alternatively, you can `download the latest archive <https://gitlab.com/dgrine/Argo-release/raw/master/version/Argo-latest.tgz>`_ directly.

Next, add the directory (``Argo/single_include`` when cloning, or the target directory when extracting) to your compiler flags. For example, with Clang or GCC simply add the ``-I</path/to/Argo>/`` flag.

That's it! You can now include Argo in your C++ projects:

.. code-block:: C++

   #include <argo/Argo.hpp>

CMake integration
-----------------

Using Git submodules
~~~~~~~~~~~~~~~~~~~~

If your project makes use of Git submodules, you can simply add the Argo repository as a subdirectory in your ``CMakeLists.txt``:

.. code-block:: CMake

   add_subdirectory(argo)

This will create the target ``Argo`` as an ``interface`` library.

That's it! You can now link your targets against ``Argo``:

.. code-block:: CMake

   target_link_libraries(my-app PRIVATE Argo)

.. note::

   If required, you can also use Argo as an expanded library, i.e. with separate files and folders. This is done by setting ``ARGO_EXPANDED_LIBRARY:BOOL=TRUE`` in CMake. There's no compelling reason to go this route, so the header-only approach is recommended.

Using ExternalProject_Add
~~~~~~~~~~~~~~~~~~~~~~~~~

The following shows integration via CMake's ``ExternalProject_Add`` command:

.. code-block:: CMake

   cmake_minimum_required(VERSION 3.1)

   project(App)

   include(ExternalProject)
   find_package(Git REQUIRED)
   ExternalProject_Add(Argo
       PREFIX ${CMAKE_CURRENT_BINARY_DIR}/extern/argo
       GIT_REPOSITORY https://gitlab.com/dgrine/Argo
       TIMEOUT 10
       UPDATE_COMMAND ${GIT_EXECUTABLE} pull
       CONFIGURE_COMMAND ""
       BUILD_COMMAND ""
       INSTALL_COMMAND ""
       LOG_DOWNLOAD ON)
   ExternalProject_Get_Property(Argo SOURCE_DIR)
   set(ARGO_ROOT ${SOURCE_DIR})

   add_executable(app main.cpp)
   add_dependencies(app Argo)
   set_target_properties(app PROPERTIES CXX_STANDARD 11) #Your project should be C++ >= 11
   target_include_directories(app PRIVATE ${ARGO_ROOT}/single_include)

Deep integration
----------------

The Argo library can also be deeply embedded in your own code base: the single include header can be transformed so that the code resides in a namespace of your choosing. This requires the following dependencies:

* Python 3
* `Invoke <http://www.pyinvoke.org>`_

Assuming the repository is checked out, the following example call in the repository's root folder transforms the single include header into a new header in which the code resides in a custom namespace:

.. code-block:: Text
   
   $ inv library.transform -n my::cpp::project::args -o /path/to/my/cpp/project/inc/Argo.hpp

Including the resulting output header in your C++ project, will now allow access to Argo's functionality via the ``my::cpp::project::args`` namespace.
