API
===========

Argo's API consists of a number of related classes in the following namespaces:

* ``argo``: general objects
* ``argo::action``: action specific functionality
* ``argo::handler``: argument handlers
* ``argo::program``: program information
* ``argo::nargs``: cardinality behavior

The core functionality resides in the ``argo::core`` namespace and is not intended for direct usage by client code, with a minor exception for some of the methods of the :cpp:class:`core::Context` object.

.. note:: A good source of API examples is to `look at the unit-tests <https://gitlab.com/dgrine/Argo/blob/master/library/test/argo>`_ for the library.

Arguments
---------

.. doxygenclass:: Arguments

Configuration
-------------

.. doxygenstruct:: Configuration

Program
-------

The ``argo::program`` namespace contains objects holding program specific information. This is mostly useful for displaying help and output error messages.

.. doxygenstruct:: program::Info

.. doxygenstruct:: program::Description

.. doxygenstruct:: program::Name

.. doxygenstruct:: program::Version

Handlers
--------

Handlers are delegate parsers: the toplevel :cpp:class:`Arguments` parser searches for handlers that 'recognize' the current argument. If found, the parsing is delegated to that handler allowing it to process any required values before yielding back control.

Handlers have certain properties:

* Cardinality: controlled by the ``nargs`` methods, it determines the number of values a handler expects:
   * ``nargs(n)``: fixed number of `n` values
   * ``nargs("?")``: zero or one value
   * ``nargs("*")``: zero or more values
   * ``nargs("+")``: one or more values
* Help: controlled by the ``help`` method, it is a description of the handled argument, usually used by formatters in the help output
* Required/Optional: controlled by the ``required/optional`` methods, they indicate whether the argument is required, i.e. must be present in the processed arguments. By default, all handlers are optional.
* Actionable: controlled by the ``action`` method, it is a list of attached :ref:`actions` that are invoked on the processed values

Options
~~~~~~~

Options are named arguments that expect values, depending on the cardinality. By default, a single value is expected.

Examples of invocations for which options are suitable are:

.. code-block:: Text

   --bar 1
   --foo 1 2 3
   --foo 1 --foo 2
   --foo=1,2
   --foo=1,2 --foo 3

The :cpp:class:`handler::Option` class implements the required behavior.

.. doxygenclass:: handler::Option

Note that named arguments such as **options always have dashed names**. That is to say: adding an option ``--foo_bar``, will silently be replaced by ``--foo-bar``.

Toggles
~~~~~~~

Toggles are named arguments that expect *at most one value*. This value must be 'truthy': something that can be interpreted as either true or false. The following table indicates which values (case insensitive) are considered true or false.

+-----------+----------------+
| Value     | Interpretation |
+===========+================+
| ``true``  | true           |
+-----------+----------------+
| ``1``     | true           |
+-----------+----------------+
| ``yes``   | true           |
+-----------+----------------+
| ``y``     | true           |
+-----------+----------------+
| ``false`` | false          |
+-----------+----------------+
| ``0``     | false          |
+-----------+----------------+
| ``no``    | false          |
+-----------+----------------+
| ``n``     | false          |
+-----------+----------------+

Any other value results in an error. Note that this intepretation is not limited to toggles and holds for any type conversion to a `bool`.

A useful feature for toggles is that they automagically support negations. For example, adding a toggle ``--debug`` mapped to ``bool debug = false`` would not only set `debug=true` when ``--debug true`` is parsed, but would also set it to true if ``--no-debug false`` is parsed.

Consider the following toy example:

.. code-block:: C++

   struct {
      bool debug = default_value;
   } options;
   Arguments args;
   args.add(handler::Toggle{"--debug", options.debug});
   const auto result = args.parse(raw_args);
   std::cout << std::boolalpha << options.debug << std::endl;

This would result in the following input/output table:

+------------------+-------------------+--------+
| Command line     | ``default_value`` | Output |
+==================+===================+========+
| --debug          | true              | true   |
+------------------+-------------------+--------+
| --debug true     | true              | true   |
+------------------+-------------------+--------+
| --debug false    | true              | false  |
+------------------+-------------------+--------+
| --no-debug       | true              | false  |
+------------------+-------------------+--------+
| --no-debug true  | true              | false  |
+------------------+-------------------+--------+
| --no-debug false | true              | true   |
+------------------+-------------------+--------+
| --debug          | false             | true   |
+------------------+-------------------+--------+
| --debug true     | false             | true   |
+------------------+-------------------+--------+
| --debug false    | false             | false  |
+------------------+-------------------+--------+
| --no-debug       | false             | false  |
+------------------+-------------------+--------+
| --no-debug true  | false             | false  |
+------------------+-------------------+--------+
| --no-debug false | false             | true   |
+------------------+-------------------+--------+

The :cpp:class:`handler::Toggle` class implements the required behavior.

.. doxygenclass:: handler::Toggle

Flags
~~~~~

Flags are named arguments that do not expect any value. They serve as triggers, nothing more.

Examples of invocations for which flags are suitable are:

.. code-block:: Text

   --foo

The :cpp:class:`handler::Flag` class implements the required behavior.

.. doxygenclass:: handler::Flag

Positional arguments
~~~~~~~~~~~~~~~~~~~~

Positional arguments are unnamed arguments which are actually references to values passed in the command line, like a list of input or output files. Argo first looks for options, custom handlers, and then positional arguments. Care needs to be taken with positional arguments. For example, it generally doesn’t make much sense to have more than one positional argument with ``nargs = "*"``. This is in contrast with options, where it could make sense to multiple optional arguments with ``nargs="*"``.

Examples of invocations for which positional arguments are suitable are:

.. code-block:: Text

   a.cpp.obj b.cpp.obj c.a

The :cpp:class:`handler::Positional` class implements the required behavior.

.. doxygenclass:: handler::Positional

Interface
~~~~~~~~~

Custom handlers can be implemented by subclassing the :cpp:class:`handler::Interface` class. For example, the default response file handler class :cpp:class:`handler::ResponseFile` is actually does this.

.. doxygenclass:: handler::Interface

Response files
~~~~~~~~~~~~~~

Response files are files containing command line arguments. See :ref:`responsefiles`. The required behavior is implemented by the :cpp:class:`handler::ResponseFile` class.

.. doxygenclass:: handler::ResponseFile

Groups
~~~~~~

Groups are collection of handlers with limited properties:

* a ``name``, mostly useful for formatters
* and an ``optional``/ ``required`` marker

Note that the latter can override the ``optional``/``required`` behavior of
individual handlers in the group. This behavior depends on the group type.
There are three built-in group types:

* :cpp:class:`handler::group::Simple`
* :cpp:class:`handler::group::Inclusive`
* :cpp:class:`handler::group::Exclusive`

Custom groups
+++++++++++++

The ``handler::group`` hierarchy can be extended. It suffices to subclass the :cpp:class:`handler::group::Interface` and implement the ``clone`` and ``is_satisfied`` methods. In fact, all built-in group types differ only in the implementation of these methods where - obviously - the latter method determines the group behavior.

.. doxygenclass:: handler::group::Interface
   :protected-members:

Simple groups
+++++++++++++

All handlers in a simple group must be *satisfied*, i.e. their properties such as expected number of arguments, required, etc. must be met.
Additionally, optional simple groups do not require any of the handlers to have been triggered. On the other hand, required simple groups must have at least one handler that triggered during the parsing process.

For practical examples, `check out the unit-tests <https://gitlab.com/dgrine/Argo/blob/master/library/test/argo/handler/group/Simple_tests.cpp>`_.

.. doxygenclass:: handler::group::Simple

Inclusive groups
++++++++++++++++

The presence of handlers in an inclusive group is forced depending on whether *any* handler within the group triggered. In other words, if one handler is triggered then all other handlers must be triggered as well. Additionally, optional inclusive groups do not require any of the handlers to have been triggered (even those marked as required) whereas required inclusive groups enforce the presence of all handlers. Put simply: a required inclusive groups is equivalent to a simple group (be it optional or required) where all handlers of the simple group are required.

For practical examples, `check out the unit-tests <https://gitlab.com/dgrine/Argo/blob/master/library/test/argo/handler/group/Inclusive_tests.cpp>`_.

.. doxygenclass:: handler::group::Inclusive

Exclusive groups
++++++++++++++++

Similarly to inclusive groups, the presence of handlers in an exclusive group is forced depending on whether *any* handler within the group triggered. In other words, if one handler is triggered then none ofthe other handlers must be triggered as well. Additionally, optional exclusive groups do not require a single handler to have been triggered (even those marked as required).

For practical examples, `check out the unit-tests <https://gitlab.com/dgrine/Argo/blob/master/library/test/argo/handler/group/Exclusive_tests.cpp>`_.

.. doxygenclass:: handler::group::Exclusive

.. note:: It is left up to the user to make sure that handlers contained in exclusive groups play well. For instance, placing more than one required handler in an exclusive group will most likely not be the sought after behavior.

.. _actions:

Actions
-------

Actions are callbacks attached to handlers. They are a central part of Argo's architecture: when a handler consumes a value, all attached handlers are invoked with the value. They can then do any required computation or modify the parsing process using the :cpp:class:`core::Context` class. For instance, they can generate errors or abort the parsing.

The most useful action is the :cpp:func:`action::run` action with all its overloads:

.. doxygenfunction:: action::run(const typename Run<Type>::function_type&)

.. doxygenfunction:: action::run(const typename Run<Type>::function_with_context_type&)

.. doxygenfunction:: action::run(const std::function<boolcore::Context&>&)

.. doxygenfunction:: action::run(const std::function<void>&)

All :cpp:func:`action::run` actions are free functions that return an :cpp:class:`action::Run` which implements :cpp:class:`action::Interface`:

.. doxygenclass:: action::Run

When designing complex actions, it might be necessary to subclass :cpp:class:`action::Interface`. However, this is very unlikely and it will usually be sufficient to create a callback and pass it to :cpp:class:`action::Run`. In fact, this is how the other built-in action :cpp:func:`action::store` (with all the overloads) is implemented:

.. doxygenfunction:: action::store(Type&, ConstType)

.. doxygenfunction:: action::store(Type&) 

.. doxygenfunction:: action::store(std::list<Type>&) 

.. doxygenfunction:: action::store(std::set<Type>&) 

.. doxygenfunction:: action::store(std::vector<Type>&) 

.. warning::

   Client-code must take care not to pass in `lambda expressions <https://en.cppreference.com/w/cpp/language/lambda>`_ which hold references to variables that are out-of-scope at the time of execution of the lambda expressions. The latter happens during the parsing.

Requirements
------------

Requirements reflect restrictions that apply to the parsed values. A collection of built-in requirements is available in the ``require`` namespace:

.. doxygenfunction:: require::any_of
.. doxygenfunction:: require::none_of
.. doxygenfunction:: require::greater_than
.. doxygenfunction:: require::greater_than_or_equal
.. doxygenfunction:: require::lesser_than
.. doxygenfunction:: require::lesser_than_or_equal

There are also range-based requirements in the ``require::range`` namespace:

.. doxygenfunction:: require::range::closed_closed
.. doxygenfunction:: require::range::closed_open
.. doxygenfunction:: require::range::open_closed
.. doxygenfunction:: require::range::open_open

You can of course pass your own requirements as an action. In fact, requirements are syntactic sugar since calling ``require`` has the same effect as calling ``action``: requirements are of type :cpp:class:`action::IAction`.

.. _typeconversions:

Type conversions
----------------

By its very nature, all command line arguments enter the application as strings. Argo takes care of all required conversions from ``std::string`` to the requested type. This type can often be deduced automatically, for instance when handlers are constructed with binding to some target variable in which to store the values. However, when attaching actions, the value must be explicitly stated.  As previously mentioned, the :cpp:class:`action::Run` class invokes the associated callbacks with the converted values. Most relevant built-in types are supported:

* ``(unsigned) short``
* ``(unsigned) int``
* ``(unsigned) long``
* ``(unsigned) long long``
* ``float``
* ``double``
* ``bool``
* Enumeration types providing a ``Nr_`` member as final item. This is purely a convenience utility and usage is subject to client code discretion

Note that this list implies support for all ``std::(u)int_<n>_t`` types as the latter are not part of the C++ language but defined in terms of the types listed above which are often referred to as the *fundamental types*.

Automatic conversions can be extended by specializing the :cpp:class:`core::traits::conversion` structure. For example, the following built-in specialization provides the conversion to double precision floating point values:

.. code-block:: C++

  template <>
  struct conversion<double>
  {
      using result_type = double;
      static constexpr const char *description = "a double";
      static optional<double> run(std::string value)
      {
          REPLACE_METRIC_SYMBOLS;
          double dvalue;
          if (!convert(dvalue, value)) return nullopt;
          return dvalue;
      }
  };

Finally, `std::optional<T> <https://en.cppreference.com/w/cpp/utility/optional>`_ (since C++17) is optionally supported (pun intended). To enable support, define ``ARGO_ENABLE_STD_OPTIONAL`` prior to including the library:

.. code-block:: C++

   #define ARGO_ENABLE_STD_OPTIONAL 1
   #include <argo/Argo.hpp>

or in your build system, e.g. ``-DARGO_ENABLE_STD_OPTIONAL=1``

.. _metrics:

Scientific notation and metric prefixes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some syntactic sugar is available for conversions of numbers: scientific notation such as ``2e3`` and metric prefixes are recognized. For the latter, the following table indicates the supported symbols:

======  ==============  ===========
Prefix  Symbol          Power of 10
======  ==============  ===========
deka    ``D``           1e1
hecto   ``h``           1e2
kilo    ``k`` or ``K``  1e3
mega    ``M``           1e6
giga    ``G``           1e9
tera    ``T``           1e12
peta    ``P``           1e15
exa     ``E``           1e18
======  ==============  ===========

Result
------

The result of the parsing process is not much more than a status code and an optional error message. This is in contrast to other frameworks where the result is often a variant type object in which all arguments are collected. In Argo's design, this offers very few advantages and causes more issues in the 'success path' (sometimes referred to as the hot path) of the code than anything else. In Argo, actions are triggered during the parsing process allowing for more type safety and a more flexible approach in the success path. Consequently, the result of the parsing is limited and given by a small aggregate.

.. doxygenstruct:: Result

ReturnCode
----------

There are three possible outcome states for the parsing process, represented by the :cpp:enum:`ReturnCode` enum class.

.. doxygenenum:: ReturnCode

Formatter
---------

The format of all generated output is provided by a formatter object which implements the :cpp:class:`formatter::Interface`. Formatters can be set using :cpp:func:`Arguments::set_formatter`.

Interface
~~~~~~~~~

.. doxygenclass:: formatter::Interface

Default
~~~~~~~

The default formatter provides clear, helpful and beautiful output. It also supports colors via `ANSI escape codes <https://en.wikipedia.org/wiki/ANSI_escape_code>`_. It can in itself serve as a base class for custom formatters.

.. doxygenclass:: formatter::Default

.. _responsefiles:

ResponseFile
------------

As an alternative to placing all the options on the command line, some applications accept special files called *response files*: plain text files that list command line arguments. This may be useful due to limitations of your build environment or as a convenience for your build process. Arguments in a response file are interpreted as if they were present at that place in the invocation. Each argument in a response file must begin and end on the same line. You cannot use the backslash character to concatenate lines. Comments are supported by prefixing lines with the `//` characters, similar to C/C++ comments.

An example of an invocation with a response file could look like:

.. code-block:: Text

   $ ./app --foo @options.rsp

where ``options.rsp`` might look like:

.. code-block:: Bash

   //This is a response file listing some command line arguments
   --bar
   --foobar 1 2 3
   //--barfoo
   --barbar foofoo

In the above example, the invocation would be equivalent to:

.. code-block:: Text

   $ ./app --foo --bar --foobar 1 2 3 --barbar foofoo

On Windows, response files can be a necessary way of circumventing the limitations of the `CreateProcessA <https://docs.microsoft.com/en-us/windows/desktop/api/processthreadsapi/nf-processthreadsapi-createprocessa>`_ API function, which limits the command line string to 32,768 characters.

Argo has built-in support for response files via the :cpp:class:`handler::ResponseFile` argument handler. If a custom response file handler is required, the :cpp:class:`ResponseFile` parser class can be used for the implementation:

.. doxygenclass:: ResponseFile

Note that one or more response files may be provided in an invocation and that these may be combined with other command line arguments. Furthermore, response file parsing is enabled by default in the :cpp:class:`Configuration`.

Core Functionality
------------------

This section limits itself to the core functionality that is likely to be used by client code, i.e. low threshold usage. More intrusive development will require consulting the `actual code <https://gitlab.com/dgrine/Argo>`_.

The primary class with which interaction is likely is the :cpp:class:`core::Context`, a class which holds the parsing context: the active parser, its configuration, the current argument to be processed, etc.

.. doxygenclass:: core::Context

Another widely used class is the :cpp:class:`core::Error` class, which is a helper utility class for generating error messages.

.. doxygenclass:: core::Error

For example, the following code attaches an action to handler to validate some input. If the input doesn't satisfy some criterium, it ends the parsing process with an error message:

.. code-block:: C++

   handler.action(action::run<unsigned int>([](core::Context &context, unsigned int value) {
      if (value % 2 == 0)
      {
         context.error() << "Value must be even (not " << value << ")";
         return false;
      }
      else return true;
   }));
