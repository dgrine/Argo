Project
=======

About
-----

Argo is a free and open-source project `hosted on GitLab <https://gitlab.com/dgrine/Argo>`_ aimed at helping modern C++ developers create professional command line utilities by providing an extensive and easy-to-use header-only library for parsing of command line arguments.

The library is currently maintained by `me <https://gitlab.com/dgrine>`_. However, contributions in any form are most welcome!

The project is available under the :ref:`license`. The aim is to be as permissive as possible, but when in doubt freel free to get in touch.

Bug reports and feature requests
--------------------------------

To submit bug reports or feature requests, log an issue on the `project's issue tracker page <https://gitlab.com/dgrine/Argo/issues>`_. The project is actively maintained.

To track functional changes between library releases, please refer to the `change log <https://gitlab.com/dgrine/Argo/blob/master/changelog.rst>`.

Development
-----------

If you would like to contribute to the development of Argo, feel free to `fork the project <https://gitlab.com/dgrine/Argo>`_ and send a pull request.

Requirements
~~~~~~~~~~~~

The following development tools are required:

* C++11 compliant compiler
* CMake 3 (>= 3.1.3)
* Python 3 (>= 3.6) with Pip, see `requirements.txt <https://gitlab.com/dgrine/Argo/blob/master/requirements.txt>`_
* `Invoke <http://www.pyinvoke.org>`_
* `Doxygen <http://www.stack.nl/~dimitri/doxygen/>`_
* `Docker <https://docs.docker.com>`_

Argo itself does not have any external dependencies, however the unit-tests are written with the `Catch2 <https://github.com/catchorg/Catch2>`_ test framework. There's no need to install this as the build system takes care of fetching it locally, but it might be worth to `familiarize yourself with it <https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#top>`_.

As for the coding style, the following `clang-format style <https://gitlab.com/dgrine/setup/blob/master/clang-format/argo/.clang-format>`_ is used.

Test-driven development
~~~~~~~~~~~~~~~~~~~~~~~

My personal way of doing test-driven development of the library on macOS/Linux is:

.. code-block:: text

   $ inv library.config
   $ inv library.test/debug-rtc

The first command configures the library's build system files: CMake is invoked to generate Makefiles. By default, a debug version with `address sanitization <https://en.wikipedia.org/wiki/AddressSanitizer>`_ is configured.
After some development, I run the unit-tests to make sure everything works as expected and that there are no regressions. Obviously, this implies that any new development has corresponding unit-tests that cover the changes and/or new features.

At the end of the process, the QC task is invoked:

.. code-block:: text

   $ inv qc

If this goes well, you can go ahead and submit a pull request.

.. note:: By default, the QC task will run platform integration tests using Docker. If for some reason you don't want to install Docker, you may run the QC tests as ``inv qc --no-docker``

In case you're an authorized developer, you may use the **do-it-all** task:

.. code-block:: text

  $ inv dia

This will run the unit-tests, QC tests, platform integration tests, update the documentation, build a package and finally push and deploy your changes.

.. note:: When making changes, please adjust the version number accordingly. This can be done using the ``version`` task. E.g. ``inv version --set <major>.<minor>.<patch>`` or ``inv version --bump``. To find out the current version, invoke the task without arguments: ``inv version``

.. _license:

MIT License
-----------

Copyright (c) 2020 D. Grine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
