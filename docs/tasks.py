from invoke import task
import pathlib
import os

THIS_DIR = pathlib.Path(__file__).parent.resolve()

@task
def build(context):
    """
    Builds the documentation.
    """
    with context.cd(str(THIS_DIR)):
        context.run('cd ..; inv library.publish')
        if "nt" == os.name: context.run('make.bat')
        else: context.run('make clean html')

@task
def open(context):
    """
    Opens the documentation in a browser (macOS only).
    """
    fn = pathlib.Path(THIS_DIR, 'build', 'html', 'index.html')
    context.run('open {}'.format(fn));

